package edu.zjucst.spb.exception;

/**
 * service中碰到的业务异常
 *
 * @author MrLi
 * @date 2022-12-03 21:53
 **/
public class BizException extends RuntimeException {

    protected String retCode;

    protected String retMessage;

    public BizException() {
        super();
    }

    public BizException(String retCode, String retMessage) {
        this.retCode = retCode;
        this.retMessage = retMessage;
    }

    public String getRetCode() {
        return retCode;
    }

    public String getRetMessage() {
        return retMessage;
    }
}
