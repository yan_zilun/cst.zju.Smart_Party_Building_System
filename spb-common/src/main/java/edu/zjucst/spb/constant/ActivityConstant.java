package edu.zjucst.spb.constant;
/**
 * 关于Stage的所有常量统一存放类
 * @author Administrator
 *
 */
public class ActivityConstant {
	public static final String ACTIVITY_TYPE_TRAINING = "培训活动";
	public static final String ACTIVITY_TYPE_REPORT = "思想汇报";
    public static final String ACTIVITY_TYPE_OTHER = "其他活动";
    public static final String ACTIVITY_TYPE_PROMOTE = "推优活动";
}
