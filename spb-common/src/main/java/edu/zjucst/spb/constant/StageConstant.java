package edu.zjucst.spb.constant;
/**
 * 关于Stage的所有常量统一存放类
 * @author Administrator
 *
 */
public class StageConstant {
	public static final String APPLICANT = "入党申请人阶段";
	public static final String ACTIVIST = "入党积极分子阶段";
    public static final String DEVELOP = "发展对象阶段";
    public static final String PREPARE = "预备党员阶段";
    public static final String OFFICIAL = "正式党员阶段";
}
