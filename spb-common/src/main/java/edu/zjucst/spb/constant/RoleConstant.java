package edu.zjucst.spb.constant;

/**
 * @author zyy
 * @version 1.0
 * @date 2023/12/21 0:46
 */
public class RoleConstant {
    public static final String STUDENT = "学生";
    public static final String SECRETARY = "支部书记";
    public static final String COMMITTEE = "学院党委";
    public static final String ADMINISTRATOR = "系统管理员";

}
