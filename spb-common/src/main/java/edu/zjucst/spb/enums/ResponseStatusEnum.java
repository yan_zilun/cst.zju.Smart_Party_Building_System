package edu.zjucst.spb.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author MrLi
 * @date 2022-12-03 22:02
 **/
@Getter
@AllArgsConstructor
public enum ResponseStatusEnum {
    SUCCESS("200", "成功"),


    AUTHENTICATION_ERROR("401", "未认证, 需要用户登录"),

    AUTHORIZATION_FORBIDDEN("403", "未授权, 需要更高的权限"),

    NOT_FOUND("404", "未找到"),

    SERVER_ERROR("500", "服务器异常"),

    ;

    final String code;

    final String description;
}
