package edu.zjucst.spb.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 操作结果码
 *
 * @author MrLi
 * @date 2022-12-03 21:35
 **/
@Getter
@AllArgsConstructor
public enum OptStatusEnum {
    SUCCESS("0000", "操作成功"),

    UNKNOWN_ERROR("9999", "未知错误"),

    OPT_ERROR("0001", "操作错误"),

    PARAM_ERROR("0002", "参数错误"),

    UPLOAD_FORBIDDEN("1001", "文件上传失败! 上传被禁止"),

    DOWNLOAD_NOT_FOUND("1002", "文件下载失败! 文件未找到"),

    UPLOAD_OVER_MAX_SIZE("1003", "上传失败! 文件过大"),

    UPLOAD_ERROR("1004", "文件上传失败! 上传时出现错误"),

    UPLOAD_SIGN_ERROR("1005", "文件上传失败! 用户未注册"),

    UPDATE_SERVER_ERROR("1999", "上传失败! 服务器出错");

    private final String code;

    private final String description;

}
