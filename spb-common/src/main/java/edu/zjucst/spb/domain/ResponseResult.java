package edu.zjucst.spb.domain;

import edu.zjucst.spb.enums.OptStatusEnum;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
/**
 * http请求做外层对象
 *
 * @author MrLi
 * @date 2022-12-03 21:16
 **/
@Data
public class ResponseResult<T> {

    private boolean success;
    /**
     * 状态码
     */
    private String code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 返回数据
     */
    private T data;

    //get和set省略
    public static <T> ResponseResult<T> success(T object) {
        ResponseResult<T> result = new ResponseResult<T>();
        result.setCode(OptStatusEnum.SUCCESS.getCode());
        result.setMsg("OK");
        result.setData(object);
        result.setSuccess(true);
        return result;
    }

    public static ResponseResult<Object> success(String msg) {
        ResponseResult<Object> result = new ResponseResult<>();
        result.setCode(OptStatusEnum.SUCCESS.getCode());
        result.setData(null);
        result.setMsg(msg);
        result.setSuccess(true);
        return result;
    }

    public static <T> ResponseResult<T> success() {
        return (ResponseResult<T>) success("操作成功");
    }

    public static <T> ResponseResult<T> error(String code, String msg) {
        ResponseResult<T> result = new ResponseResult<T>();
        result.setCode(code);
        result.setMsg(msg);
        result.setSuccess(false);
        return result;
    }

    public static <T> ResponseResult<T> error(OptStatusEnum optStatusEnum) {
        ResponseResult<T> result = new ResponseResult<T>();
        result.setCode(optStatusEnum.getCode());
        result.setMsg(optStatusEnum.getDescription());
        result.setSuccess(false);
        return result;
    }

    public static ResponseEntity<byte[]> downloadTemplate(byte[] fileBytes, String filename) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", filename);
        return new ResponseEntity<>(fileBytes, headers, HttpStatus.OK);
    }
}
