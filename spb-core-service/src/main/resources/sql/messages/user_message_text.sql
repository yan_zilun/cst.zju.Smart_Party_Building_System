/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Schema         : smart_party_business

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 2023年4月5日
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- 站内信信息具体内容
-- Table structure for tb_user_message_text
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_message_text`;

CREATE TABLE `tb_user_message_text`
(
    `id`          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息id',
    `title`       TEXT            NOT NULL COMMENT '标题',
    `content`     TEXT            NOT NULL COMMENT '文本具体内容',
    `attachments` TEXT            NOT NULL COMMENT '附件链接, 多个用;分割',
    `create_time` DATETIME        NOT NULL COMMENT '创建时间' DEFAULT CURRENT_TIMESTAMP,
    `update_time` DATETIME        NOT NULL COMMENT '更新时间' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;

