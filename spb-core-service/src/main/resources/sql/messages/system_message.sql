/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Schema         : smart_party_business

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 2023年4月5日
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- 系统推送信息
-- Table structure for tb_system_message
-- ----------------------------
DROP TABLE IF EXISTS `tb_system_message`;

CREATE TABLE `tb_system_message`
(
    `id`           BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息id',
    `send_user_id` BIGINT UNSIGNED NOT NULL COMMENT '发送人id',
    `content`      TEXT     NOT NULL COMMENT '文本内容',
    `create_time`  DATETIME NOT NULL COMMENT '创建时间' DEFAULT CURRENT_TIMESTAMP,
    `update_time`  DATETIME NOT NULL COMMENT '更新时间' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

SET
FOREIGN_KEY_CHECKS = 1;
