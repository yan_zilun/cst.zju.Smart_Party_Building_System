/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Schema         : smart_party_business

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 2023年4月5日
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- 用户站内信记录表
-- Table structure for tb_user_message
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_message`;

CREATE TABLE `tb_user_message`
(
    `id`           BIGINT UNSIGNED AUTO_INCREMENT COMMENT '消息id',
    `send_user_id` BIGINT UNSIGNED NOT NULL COMMENT '发送用户id',
    `recv_user_id` BIGINT UNSIGNED NOT NULL COMMENT '接收用户id',
    -- 外键关联 user_message_text中的信息
    `message_id`   BIGINT   NOT NULL COMMENT '消息id',
    `is_read`      TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否已读',
    `create_time`  DATETIME NOT NULL COMMENT '创建时间' DEFAULT CURRENT_TIMESTAMP,
    `update_time`  DATETIME NOT NULL COMMENT '更新时间' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `need_push_time`  DATETIME NOT NULL COMMENT '创建时间' DEFAULT CURRENT_TIMESTAMP,
    `need_push`      TINYINT(1) NOT NULL DEFAULT 1 COMMENT '是否推送, 非定时消息默认为1',
    `message_param` TEXT  NULL COMMENT '动态参数, JSON TEXT',
    PRIMARY KEY (`id`),
    INDEX          send_index(`send_user_id`),
    INDEX          recv_index(`recv_user_id`, `is_read`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
