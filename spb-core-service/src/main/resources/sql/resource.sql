SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ACT_ID_USER
-- ----------------------------
DROP TABLE IF EXISTS `tb_resource`;

CREATE TABLE `tb_resource` (
  `ID` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `PARENT_ID` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '父资源',
  `RESOURCE_NAME` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '资源名称',
  `REQUEST_PATH` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '资源路径',
  `RESOURCE_TYPE` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '资源类型',
  `ENABLE_FLAG` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '是否有效',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ROW_FORMAT = Dynamic COMMENT='资源表';


SET FOREIGN_KEY_CHECKS = 1;

