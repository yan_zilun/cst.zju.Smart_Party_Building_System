SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `tb_branch`;

CREATE TABLE `tb_branch` (
    `id`        BIGINT UNSIGNED NOT NULL COMMENT '党支部编号id',
    `branch_name`      VARCHAR(300)    NULL COMMENT '党支部名称',
    `parent_branch_id` BIGINT unsigned NULL COMMENT '上级党支部编号',
    `branch_info`      VARCHAR (300)    NULL COMMENT '党支部信息',
    PRIMARY KEY (`id`),
    UNIQUE KEY(`branch_name`)
)ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ROW_FORMAT = Dynamic COMMENT='党支部信息表';

INSERT INTO `tb_branch` VALUES (0, '第一党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (1, '修改后的第一党支部', 2, '');
INSERT INTO `tb_branch` VALUES (2, '修改后的支部信息', 2, '');
INSERT INTO `tb_branch` VALUES (3, '第四党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (4, '第五党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (5, '第六党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (8, '第八党支部', NULL, '');

SET FOREIGN_KEY_CHECKS = 1;



