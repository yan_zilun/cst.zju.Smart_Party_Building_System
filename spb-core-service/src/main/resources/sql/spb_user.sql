/*
 Navicat Premium Data Transfer

 Source Server         : ali3307
 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Host           : 81.68.143.166:3307
 Source Schema         : spb

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 05/04/2023 19:37:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_spb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_spb_user`;
CREATE TABLE `tb_spb_user`
(
    `id`               BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键Id',
    `username`         VARCHAR(256)    NOT NULL COMMENT '用户名',
    `pwd`              VARCHAR(64)     NULL COMMENT '密码',
    `user_number`       VARCHAR(32)     NULL COMMENT '学号',
    `role_name`        VARCHAR(256)    default '未分配' COMMENT '权限，如非党员学生/党员学生/支部书记/学院党委/系统管理员',
    `register_time`    DATETIME        NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
    `sex`              tinyint(2)         NOT NULL DEFAULT 0 COMMENT '性别, 默认为, 0未知1男2女',
    `birthday`         DATE            NULL COMMENT '生日',
    `nationality`      VARCHAR(128)    NULL     DEFAULT NULL COMMENT '民族',
    `politics_status`  VARCHAR(64)     NULL     DEFAULT NULL COMMENT '政治面貌',
    `address`          VARCHAR(1024)   NULL     DEFAULT NULL COMMENT '家庭住址',
    `identity_id`      VARCHAR(32)     NULL     DEFAULT NULL COMMENT '身份证号',
    `qualification`    VARCHAR(128)    NULL     DEFAULT NULL COMMENT '学历',
    `reading_status`   VARCHAR(64)     NULL     DEFAULT NULL COMMENT '在读状态',
    `league_num`       VARCHAR(64)     NULL     DEFAULT NULL COMMENT '团员编号',
    `league_join_time` DATETIME        NULL     DEFAULT NULL COMMENT '入团时间',
    `if_apply`         VARCHAR(300)    NULL     DEFAULT NULL COMMENT '是否提交入党申请书',
    `development_phase`     VARCHAR(32)         NULL     DEFAULT 0 COMMENT '入党状态，如入党申请阶段/积极分子阶段/发展对象阶段/预备党员阶段',
    `party_join_time`  DATETIME        NULL     DEFAULT NULL COMMENT '入党时间',
    `party_branch`     VARCHAR(128)    NULL     DEFAULT NULL COMMENT '所在党支部',
    `phone`            CHAR(11)        NULL     DEFAULT NULL COMMENT '电话号码',
    `email`            VARCHAR(32)     NULL     DEFAULT NULL COMMENT '电子邮箱',
    PRIMARY KEY (`id`),
    UNIQUE INDEX `person_person_id_uindex` (`user_number`),
    INDEX `role_index` (`role_name`)
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci
  -- 与下面的insert保持一致
  AUTO_INCREMENT 1
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------

-- 保持与 NoticeMessageSenderEnum 一致
INSERT INTO `tb_spb_user` VALUES
       (10001, '软件学院支委会', 'aaa', '10001', '未分配', CURRENT_TIMESTAMP, 0, NULL, 'UNKNOWN', 'NLL', '宁波', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-01', 'NULL', 'NO_PHONE', 'NO_EMAIL'),
       (10002, '软件学院党委', 'aaa', '10002', '系统身份', CURRENT_TIMESTAMP, 0, NULL, 'UNKNOWN', 'NLL', '宁波', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-01', 'NULL', 'NO_PHONE', 'NO_EMAIL'),
       (10003, '软件学院支委会、党委', '10003', 'NULL', '系统身份', CURRENT_TIMESTAMP, 0, NULL, 'UNKNOWN', 'NLL', '宁波', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-01', 'NULL', 'NO_PHONE', 'NO_EMAIL')
;

SET FOREIGN_KEY_CHECKS = 1;
