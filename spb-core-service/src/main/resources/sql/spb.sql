/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : spb

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 28/10/2023 11:01:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_activist
-- ----------------------------
DROP TABLE IF EXISTS `tb_activist`;
CREATE TABLE `tb_activist`  (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '阶段ID',
  `user_number` int(50) NOT NULL COMMENT '用户学工号',
  `user_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户姓名',
  `development_phase` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发展阶段',
  `branch_prepare_time` date NULL DEFAULT NULL COMMENT '支部大会(接受为预备)时间',
  `talk_time` date NULL DEFAULT NULL COMMENT '上级党委派人谈话时间',
  `examine_time` date NULL DEFAULT NULL COMMENT '党委审批时间',
  `apply_full_time` date NULL DEFAULT NULL COMMENT '提出转正申请时间',
  `public_time` date NULL DEFAULT NULL COMMENT '群众意见调查表提交时间',
  `branch_full_time` date NULL DEFAULT NULL COMMENT '支部大会(转正)时间',
  `create_time` date NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` date NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_activist
-- ----------------------------

-- ----------------------------
-- Table structure for tb_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_activity`;
CREATE TABLE `tb_activity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `activity_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动编号',
  `activity_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动类型',
  `activity_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动名称',
  `user_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学工号',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `development_phase` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发展阶段',
  `activity_sponsor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主办单位',
  `activity_date` date NOT NULL COMMENT '活动时间',
  `addition_file` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '附加文件链接或者文件名',
  `applied_study_hour` int(4) NULL DEFAULT NULL COMMENT '申请学时',
  `state` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态',
  `remark` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` date NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` date NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_activity
-- ----------------------------

-- ----------------------------
-- Table structure for tb_branch
-- ----------------------------
DROP TABLE IF EXISTS `tb_branch`;
CREATE TABLE `tb_branch`  (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '党支部编号id',
  `branch_name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '党支部名称',
  `parent_branch_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '上级党支部编号',
  `branch_info` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '党支部信息',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `branch_name`(`branch_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '党支部信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_branch
-- ----------------------------
INSERT INTO `tb_branch` VALUES (0, '第一党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (1, '修改后的第一党支部', 2, '');
INSERT INTO `tb_branch` VALUES (2, '修改后的支部信息', 2, '');
INSERT INTO `tb_branch` VALUES (3, '第四党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (4, '第五党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (5, '第六党支部', NULL, '');
INSERT INTO `tb_branch` VALUES (8, '第八党支部', NULL, '');

-- ----------------------------
-- Table structure for tb_file
-- ----------------------------
DROP TABLE IF EXISTS `tb_file`;
CREATE TABLE `tb_file`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键Id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `type` int(11) NOT NULL COMMENT '文件类型',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `creator_id` int(11) NULL DEFAULT NULL COMMENT '创建者的用户id',
  `privilege` int(11) NOT NULL COMMENT '文件权限 公有or私有',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_file
-- ----------------------------

-- ----------------------------
-- Table structure for tb_party_applicant
-- ----------------------------
DROP TABLE IF EXISTS `tb_party_applicant`;
CREATE TABLE `tb_party_applicant`  (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '阶段ID',
  `user_number` int(50) NOT NULL COMMENT '用户学工号',
  `user_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户姓名',
  `development_phase` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发展阶段',
  `is_league` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否共青团员',
  `delivery_time` date NULL DEFAULT NULL COMMENT '入党申请书递交时间',
  `talk_time` date NULL DEFAULT NULL COMMENT '谈话时间',
  `create_time` date NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` date NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_party_applicant
-- ----------------------------

-- ----------------------------
-- Table structure for tb_resource
-- ----------------------------
DROP TABLE IF EXISTS `tb_resource`;
CREATE TABLE `tb_resource`  (
  `ID` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `PARENT_ID` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父资源',
  `RESOURCE_NAME` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `REQUEST_PATH` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源路径',
  `RESOURCE_TYPE` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源类型',
  `ENABLE_FLAG` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否有效',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '资源表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_resource
-- ----------------------------

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键Id',
  `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `role_name`(`role_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (4, '党员学生');
INSERT INTO `tb_role` VALUES (6, '学院党委');
INSERT INTO `tb_role` VALUES (1, '未分配');
INSERT INTO `tb_role` VALUES (2, '系统管理员');
INSERT INTO `tb_role` VALUES (5, '老师支部书记');
INSERT INTO `tb_role` VALUES (3, '非党员学生');

-- ----------------------------
-- Table structure for tb_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_resource`;
CREATE TABLE `tb_role_resource`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `enable_flag` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `resource_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色资源表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_role_resource
-- ----------------------------

-- ----------------------------
-- Table structure for tb_self_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_self_activity`;
CREATE TABLE `tb_self_activity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `activity_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动名称',
  `activity_level` int(4) NOT NULL COMMENT '活动级别 0院级 1校级',
  `user_number` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学工号',
  `activity_sponsor` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主办单位',
  `activity_date` date NOT NULL COMMENT '活动时间',
  `activity_graph` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '活动图谱文件',
  `applied_study_hour` int(4) NULL DEFAULT NULL COMMENT '申请学时',
  `addition_file` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '附加文件链接或者文件名',
  `audit_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '审核状态',
  `audit_time` date NULL DEFAULT NULL COMMENT '审核时间',
  `create_time` date NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` date NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_self_activity
-- ----------------------------

-- ----------------------------
-- Table structure for tb_spb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_spb_user`;
CREATE TABLE `tb_spb_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键Id',
  `username` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `pwd` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `user_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学号',
  `role_name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '未分配' COMMENT '权限，如非党员学生/党员学生/支部书记/学院党委/系统管理员',
  `register_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '注册时间',
  `sex` tinyint(2) NOT NULL DEFAULT 0 COMMENT '性别, 默认为, 0未知1男2女',
  `birthday` date NULL DEFAULT NULL COMMENT '生日',
  `nationality` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '民族',
  `politics_status` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '政治面貌',
  `address` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '家庭住址',
  `identity_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `qualification` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学历',
  `reading_status` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '在读状态',
  `league_num` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '团员编号',
  `league_join_time` datetime(0) NULL DEFAULT NULL COMMENT '入团时间',
  `if_apply` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否提交入党申请书',
  `development_phase` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '入党状态，如入党申请阶段/积极分子阶段/发展对象阶段/预备党员阶段',
  `party_join_time` datetime(0) NULL DEFAULT NULL COMMENT '入党时间',
  `party_branch` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所在党支部',
  `phone` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `person_person_id_uindex`(`user_number`) USING BTREE,
  INDEX `role_index`(`role_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10004 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_spb_user
-- ----------------------------
INSERT INTO `tb_spb_user` VALUES (10001, '软件学院支委会', 'aaa', '10001', '未分配', '2023-10-28 11:00:07', 0, NULL, 'UNKNOWN', 'NLL', '宁波', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-01 00:00:00', 'NULL', 'NO_PHONE', 'NO_EMAIL');
INSERT INTO `tb_spb_user` VALUES (10002, '软件学院党委', 'aaa', '10002', '系统身份', '2023-10-28 11:00:07', 0, NULL, 'UNKNOWN', 'NLL', '宁波', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-01 00:00:00', 'NULL', 'NO_PHONE', 'NO_EMAIL');
INSERT INTO `tb_spb_user` VALUES (10003, '软件学院支委会、党委', '10003', 'NULL', '系统身份', '2023-10-28 11:00:07', 0, NULL, 'UNKNOWN', 'NLL', '宁波', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-01-01 00:00:00', 'NULL', 'NO_PHONE', 'NO_EMAIL');

-- ----------------------------
-- Table structure for tb_stage
-- ----------------------------
DROP TABLE IF EXISTS `tb_stage`;
CREATE TABLE `tb_stage`  (
  `id` int(50) NOT NULL AUTO_INCREMENT COMMENT '阶段ID',
  `user_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学工号',
  `user_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户姓名',
  `development_phase` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发展阶段',
  `is_league` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否共青团员',
  `delivery_time` date NULL DEFAULT NULL COMMENT '入党申请书递交时间',
  `talk_registration_time` date NULL DEFAULT NULL COMMENT '入党申请人谈话登记表提交时间',
  `talker` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '谈话人',
  `qualification_interval` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资格审查时间间隔',
  `is_promote` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '否' COMMENT '是否满足推优条件',
  `promote_time` date NULL DEFAULT NULL COMMENT '推优时间',
  `activist_time` date NULL DEFAULT NULL COMMENT '确定为积极分子时间',
  `thought_report` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '思想汇报提交时间',
  `educational_visit` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '入党积极分子和发展对象培养教育考察登记表提交时间',
  `activist_party_training` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '积极分子阶段党校培训班参与时间',
  `cultivate_contacts` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '培养联系人',
  `development_public_time` date NULL DEFAULT NULL COMMENT '群众意见调查表(确定为发展对象)提交时间',
  `teacher_time` date NULL DEFAULT NULL COMMENT '班主任导师意见征求表提交时间',
  `confirm_time` date NULL DEFAULT NULL COMMENT '确定为发展对象时间',
  `record_time` date NULL DEFAULT NULL COMMENT '发展对象备案时间',
  `party_sponsor` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '入党介绍人',
  `political_review_time` date NULL DEFAULT NULL COMMENT '入党对象政治审查表提交时间',
  `develop_party_training` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发展对象阶段党校培训班参与时间',
  `investigate_time` date NULL DEFAULT NULL COMMENT '审查时间',
  `preliminary_investigate_time` date NULL DEFAULT NULL COMMENT '预审时间',
  `party_application_time` date NULL DEFAULT NULL COMMENT '入党志愿书提交时间',
  `recommendable` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '否' COMMENT '是否符合转为预备党员',
  `branch_prepare_time` date NULL DEFAULT NULL COMMENT '预备党员被接受时间（支部大会）',
  `committee_talk` date NULL DEFAULT NULL COMMENT '党委派人谈话时间',
  `examine_time` date NULL DEFAULT NULL COMMENT '党委审批时间',
  `prepare_thought_report` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预备党员阶段思想汇报提交时间',
  `prepare_party_training` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预备党员阶段党校培训班参与时间',
  `apply_full_time` date NULL DEFAULT NULL COMMENT '提出转正申请时间',
  `probationary_public_time` date NULL DEFAULT NULL COMMENT '群众意见调查表（转正前）提交时间',
  `branch_full_time` date NULL DEFAULT NULL COMMENT '支部大会(转正)时间',
  `committee_full_time` date NULL DEFAULT NULL COMMENT '党委转正审批时间',
  `state` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '提交入党申请书' COMMENT '阶段状态',
  `create_time` date NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` date NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_stage
-- ----------------------------

-- ----------------------------
-- Table structure for tb_system_message
-- ----------------------------
DROP TABLE IF EXISTS `tb_system_message`;
CREATE TABLE `tb_system_message`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `send_user_id` bigint(20) UNSIGNED NOT NULL COMMENT '发送人id',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文本内容',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_system_message
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user_message
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_message`;
CREATE TABLE `tb_user_message`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `send_user_id` bigint(20) UNSIGNED NOT NULL COMMENT '发送用户id',
  `recv_user_id` bigint(20) UNSIGNED NOT NULL COMMENT '接收用户id',
  `message_id` bigint(20) NOT NULL COMMENT '消息id',
  `is_read` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否已读',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `need_push_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `need_push` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否推送, 非定时消息默认为1',
  `message_param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '动态参数, JSON TEXT',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `send_index`(`send_user_id`) USING BTREE,
  INDEX `recv_index`(`recv_user_id`, `is_read`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_message
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user_message_text
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_message_text`;
CREATE TABLE `tb_user_message_text`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文本具体内容',
  `attachments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '附件链接, 多个用;分割',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user_message_text
-- ----------------------------

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `enable_flag` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
