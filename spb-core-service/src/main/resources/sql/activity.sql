/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Schema         : smart_party_business

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 03/12/2022 22:53:15
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_activity`;
-- 书记提交的表
CREATE TABLE `tb_activity`
(
    `id`   BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '活动ID',
    `activity_number` VARCHAR(255) NOT NULL COMMENT '活动编号',
    `activity_type` VARCHAR(255) NOT NULL COMMENT '活动类型',
    `activity_name` VARCHAR(255) NOT NULL COMMENT '活动名称',
    `user_number` VARCHAR(255)  NOT NULL COMMENT '学工号',
    `user_name` VARCHAR(255) NOT NULL COMMENT '姓名',
    `development_phase` varchar(36) NOT NULL COMMENT '发展阶段',
    `activity_sponsor` VARCHAR(255) NOT NULL COMMENT '主办单位',
    `activity_date` DATE NOT NULL COMMENT '活动时间',
    `addition_file` varchar(512) NULL COMMENT '附加文件链接或者文件名',
    `applied_study_hour` INT(4) NULL COMMENT '申请学时',
    `state` VARCHAR(255) NOT NULL COMMENT '状态',
    `remark` varchar(512) NULL COMMENT '备注',
    `create_time` DATE NULL COMMENT '创建时间',
    `update_time` DATE NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_activity
-- ----------------------------

SET
FOREIGN_KEY_CHECKS = 1;
