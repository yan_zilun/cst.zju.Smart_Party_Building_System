SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_stage`;
-- 书记提交的表
CREATE TABLE `tb_stage`
(
    `id`   INT(50) NOT NULL AUTO_INCREMENT COMMENT '阶段ID',
    `user_number` VARCHAR(191)  NOT NULL COMMENT '学工号',
    `user_name` VARCHAR(191) NOT NULL COMMENT '用户姓名',
    `development_phase` varchar(36) NOT NULL COMMENT '发展阶段',


    `is_league` varchar(36) NULL COMMENT '是否共青团员',
    `delivery_time` DATE NULL COMMENT '入党申请书递交时间',
    `talk_registration_time` DATE NULL COMMENT '入党申请人谈话登记表提交时间',
    `talker` VARCHAR(191) NULL COMMENT '谈话人',
    `qualification_interval` VARCHAR(191) NULL COMMENT '资格审查时间间隔',
    `is_promote` varchar(36) NOT NULL DEFAULT '否' COMMENT '是否满足推优条件',

    `promote_time` DATE NULL COMMENT '推优时间',
    `activist_time` DATE NULL COMMENT '确定为积极分子时间',
    `thought_report` VARCHAR(191) DEFAULT NULL COMMENT '思想汇报提交时间',
    `educational_visit` VARCHAR(191) DEFAULT NULL COMMENT '入党积极分子和发展对象培养教育考察登记表提交时间',
    `activist_party_training` VARCHAR(191) DEFAULT NULL COMMENT '积极分子阶段党校培训班参与时间',
    `cultivate_contacts` VARCHAR(191) DEFAULT NULL COMMENT '培养联系人',
    `development_public_time` DATE NULL COMMENT '群众意见调查表(确定为发展对象)提交时间',
    `teacher_time` DATE NULL COMMENT '班主任导师意见征求表提交时间',

    `confirm_time` DATE DEFAULT NULL COMMENT '确定为发展对象时间',
    `record_time` DATE DEFAULT NULL COMMENT '发展对象备案时间',
    `party_sponsor` VARCHAR(191) DEFAULT NULL COMMENT '入党介绍人',
    `political_review_time` DATE DEFAULT NULL COMMENT '入党对象政治审查表提交时间',
    `develop_party_training` VARCHAR(191) DEFAULT NULL COMMENT '发展对象阶段党校培训班参与时间',
    `investigate_time` DATE DEFAULT NULL COMMENT '审查时间',
    `preliminary_investigate_time` DATE DEFAULT NULL COMMENT '预审时间',
    `party_application_time` DATE DEFAULT NULL COMMENT '入党志愿书提交时间',
    `recommendable` varchar(4) NOT NULL DEFAULT '否' COMMENT '是否符合转为预备党员',

    --
    `branch_prepare_time` DATE NULL COMMENT '预备党员被接受时间（支部大会）',
    `committee_talk` DATE NULL COMMENT '党委派人谈话时间',
    `examine_time` DATE NULL COMMENT '党委审批时间',
    `prepare_thought_report` VARCHAR(191) DEFAULT NULL COMMENT '预备党员阶段思想汇报提交时间',
    `prepare_party_training` VARCHAR(191) DEFAULT NULL COMMENT '预备党员阶段党校培训班参与时间',
    `apply_full_time` DATE NULL COMMENT '提出转正申请时间',
    `probationary_public_time` DATE NULL COMMENT '群众意见调查表（转正前）提交时间',
    `branch_full_time` DATE NULL COMMENT '支部大会(转正)时间',
    `committee_full_time` DATE NULL COMMENT '党委转正审批时间',

    `state` varchar(191) DEFAULT '提交入党申请书' COMMENT '阶段状态',
    `create_time` DATE NULL COMMENT '创建时间',
    `update_time` DATE NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_activity
-- ----------------------------

SET
FOREIGN_KEY_CHECKS = 1;
