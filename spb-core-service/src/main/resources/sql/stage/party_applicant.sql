SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_party_applicant`;
-- 书记提交的表
CREATE TABLE `tb_party_applicant`
(
    `id`   INT(50) NOT NULL AUTO_INCREMENT COMMENT '阶段ID',
    `user_number` INT(50)  NOT NULL COMMENT '用户学工号',
    `user_name` VARCHAR(191) NOT NULL COMMENT '用户姓名',
    `development_phase` varchar(36) NOT NULL COMMENT '发展阶段',
    `is_league` varchar(36) NULL COMMENT '是否共青团员',
    `delivery_time` DATE NULL COMMENT '入党申请书递交时间',
    `talk_time` DATE NULL COMMENT '谈话时间',
    `create_time` DATE NULL COMMENT '创建时间',
    `update_time` DATE NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_activity
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
