SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_stage`;
-- 书记提交的表
CREATE TABLE `tb_stage`
(
    `id`   INT(50) NOT NULL AUTO_INCREMENT COMMENT '阶段ID',
    `user_number` INT(50)  NOT NULL COMMENT '用户学工号',
    `user_name` VARCHAR(191) NOT NULL COMMENT '用户姓名',
    `development_phase` varchar(36) NOT NULL COMMENT '发展阶段',
    `confirm_time` DATE DEFAULT NULL COMMENT '确定为发展对象时间',
    `record_time` DATE DEFAULT NULL COMMENT '发展对象备案时间',

    `political_review_time` DATE DEFAULT NULL COMMENT '入党对象政治审查表提交时间',

    `investigate_time` DATE DEFAULT NULL COMMENT '审查时间',
    `preliminary_investigate_time` DATE DEFAULT NULL COMMENT '预审时间',
    `party_application_time` DATE DEFAULT NULL COMMENT '入党志愿书提交时间',
    `recommendable` varchar(4) NOT NULL COMMENT '是否符合转为预备党员',
    `create_time` DATE NULL COMMENT '创建时间',
    `update_time` DATE NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_activity
-- ----------------------------

SET
FOREIGN_KEY_CHECKS = 1;
