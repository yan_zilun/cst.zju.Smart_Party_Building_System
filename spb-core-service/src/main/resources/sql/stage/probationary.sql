SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_activist`;
-- 书记提交的表
CREATE TABLE `tb_activist`
(
    `id`   INT(50) NOT NULL AUTO_INCREMENT COMMENT '阶段ID',
    `user_number` INT(50)  NOT NULL COMMENT '用户学工号',
    `user_name` VARCHAR(191) NOT NULL COMMENT '用户姓名',
    `development_phase` varchar(36) NOT NULL COMMENT '发展阶段',
    `branch_prepare_time` DATE NULL COMMENT '支部大会(接受为预备)时间',
    `talk_time` DATE NULL COMMENT '上级党委派人谈话时间',
    `examine_time` DATE NULL COMMENT '党委审批时间',


    `apply_full_time` DATE NULL COMMENT '提出转正申请时间',
    `public_time` DATE NULL COMMENT '群众意见调查表提交时间',
    `branch_full_time` DATE NULL COMMENT '支部大会(转正)时间',
    `create_time` DATE NULL COMMENT '创建时间',
    `update_time` DATE NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_activity
-- ----------------------------

SET
FOREIGN_KEY_CHECKS = 1;
