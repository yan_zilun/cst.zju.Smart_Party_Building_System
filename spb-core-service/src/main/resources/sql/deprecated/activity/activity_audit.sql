/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Schema         : smart_party_business

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 03/12/2022 22:53:15
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_activity_audit
-- ----------------------------
DROP TABLE IF EXISTS `spb_activity_audit`;
-- 活动审批情况
CREATE TABLE `spb_activity_audit`
(
    `id`           INT(50) NOT NULL AUTO_INCREMENT COMMENT '审批ID',
    `activity_id`  VARCHAR(255) NOT NULL COMMENT '活动id',
    `audit_status` VARCHAR(255) NOT NULL COMMENT '审批状态',
    `audit_status` TIME         NOT NULL COMMENT '审批时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_account
-- ----------------------------

SET
FOREIGN_KEY_CHECKS = 1;
