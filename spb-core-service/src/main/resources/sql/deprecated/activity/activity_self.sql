/*
合并俩表暂时用不到
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Schema         : smart_party_business

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 03/12/2022 22:53:15
*/

SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_activity_self
-- ----------------------------
DROP TABLE IF EXISTS `spb_activity_self`;
-- 同学自己提交的活动
CREATE TABLE `spb_activity_self`
(
    `id`   INT(50) NOT NULL AUTO_INCREMENT COMMENT '活动ID',
    `activity_name` VARCHAR(255) NOT NULL COMMENT '活动名称',
    `activity_level` INT(8) NOT NULL COMMENT '活动级别',
    `activity_sponsor` INT(8) NOT NULL COMMENT '主办单位',
    `activity_date` DATE NOT NULL COMMENT '活动时间',
    `applied_study_hour` INT(4) NOT NULL COMMENT '申请学时',
    `activity_pic_url` VARCHAR(512) NOT NULL COMMENT '活动图片链接',
    `activity_comment` TEXT  COMMENT '备注',
    `addition_file_url` varchar(512)  COMMENT '附加文件链接',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_account
-- ----------------------------

SET
FOREIGN_KEY_CHECKS = 1;
