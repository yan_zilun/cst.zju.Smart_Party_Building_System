/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Schema         : smart_party_business

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 03/12/2022 22:53:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for spb_account test
-- ----------------------------
DROP TABLE IF EXISTS `spb_account`;
CREATE TABLE `spb_account`  (
    `id` int(50) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `role` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of spb_account
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
