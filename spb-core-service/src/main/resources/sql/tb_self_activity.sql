/*
Navicat MySQL Data Transfer

Source Server         : smartParty
Source Server Version : 50740
Source Host           : 81.68.143.166:3307
Source Database       : spb

Target Server Type    : MYSQL
Target Server Version : 50740
File Encoding         : 65001

Date: 2023-04-18 19:37:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_self_activity
-- ----------------------------
DROP TABLE IF EXISTS `tb_self_activity`;
CREATE TABLE `tb_self_activity` (
  `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '活动ID',
  `activity_name` varchar(255) NOT NULL COMMENT '活动名称',
  `activity_level` int(4) NOT NULL COMMENT '活动级别 0院级 1校级',
  `user_numer` varchar(64) NOT NULL COMMENT '学工号',
  `activity_sponsor` varchar(255) NOT NULL COMMENT '主办单位',
  `activity_date` date NOT NULL COMMENT '活动时间',
  `activity_graph` varchar(255) NOT NULL COMMENT '活动图谱文件',
  `applied_study_hour` int(4) DEFAULT NULL COMMENT '申请学时',
  `addition_file` varchar(512) DEFAULT NULL COMMENT '附加文件链接或者文件名',
  `audit_status` varchar(255) NOT NULL COMMENT '审核状态',
  `audit_time` date DEFAULT NULL COMMENT '审核时间',
  `create_time` date DEFAULT NULL COMMENT '创建时间',
  `update_time` date DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
