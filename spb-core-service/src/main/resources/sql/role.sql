SET NAMES utf8mb4;
SET
FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ACT_ID_USER
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;

CREATE TABLE `tb_role`
(
    `id`        BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键Id',
    `role_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '角色名称',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY (`role_name`)
) ENGINE=InnoDB CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci ROW_FORMAT = Dynamic COMMENT='角色表';

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role`VALUES (1, '未分配');
INSERT INTO `tb_role`VALUES (2, '系统管理员');
INSERT INTO `tb_role`VALUES (3, '非党员学生');
INSERT INTO `tb_role`VALUES (4, '党员学生');
INSERT INTO `tb_role`VALUES (5, '老师支部书记');
INSERT INTO `tb_role`VALUES (6, '学院党委');

SET
FOREIGN_KEY_CHECKS = 1;
