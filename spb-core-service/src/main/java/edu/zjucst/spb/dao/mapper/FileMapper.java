package edu.zjucst.spb.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.TbFile;

/**
 * 对应tb_file表
 * @author zxz
 */
public interface FileMapper extends BaseMapper<TbFile> {
}
