//package edu.zjucst.spb.dao.user;
//
//import org.mybatis.dynamic.sql.SqlColumn;
//import org.mybatis.dynamic.sql.SqlTable;
//
//import javax.annotation.Generated;
//import java.sql.JDBCType;
//
//public final class UserDynamicSqlSupport {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final User user = new User();
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> id = user.id;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Integer> rev = user.rev;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> first = user.first;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> last = user.last;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> displayName = user.displayName;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> email = user.email;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> pwd = user.pwd;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> pictureId = user.pictureId;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> tenantId = user.tenantId;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> username = user.username;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final class User extends SqlTable {
//        public final SqlColumn<String> id = column("ID_", JDBCType.VARCHAR);
//
//        public final SqlColumn<Integer> rev = column("REV_", JDBCType.INTEGER);
//
//        public final SqlColumn<String> first = column("FIRST_", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> last = column("LAST_", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> displayName = column("DISPLAY_NAME_", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> email = column("EMAIL_", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> pwd = column("PWD_", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> pictureId = column("PICTURE_ID_", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> tenantId = column("TENANT_ID_", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> username = column("username", JDBCType.VARCHAR);
//
//        public User() {
//            super("tb_user");
//        }
//    }
//}
