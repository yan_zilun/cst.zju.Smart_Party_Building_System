package edu.zjucst.spb.dao.branch;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.Branch;


public interface BranchDao extends BaseMapper<Branch> {

}
