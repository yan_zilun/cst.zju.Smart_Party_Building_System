//package edu.zjucst.spb.dao.role;
//
//import edu.zjucst.spb.domain.entity.Role;
//import org.apache.ibatis.annotations.*;
//import org.apache.ibatis.type.JdbcType;
//import org.mybatis.dynamic.sql.BasicColumn;
//import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
//import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
//import org.mybatis.dynamic.sql.select.CountDSLCompleter;
//import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
//import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
//import org.mybatis.dynamic.sql.update.UpdateDSL;
//import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
//import org.mybatis.dynamic.sql.update.UpdateModel;
//import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
//import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
//import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
//
//import javax.annotation.Generated;
//import java.util.Collection;
//import java.util.List;
//import java.util.Optional;
//
//import static edu.zjucst.spb.dao.role.RoleDynamicSqlSupport.*;
//import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
//
//@Mapper
//public interface RoleMapper {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    BasicColumn[] selectList = BasicColumn.columnList(roleId, roleName);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    long count(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
//    int delete(DeleteStatementProvider deleteStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
//    int insert(InsertStatementProvider<Role> insertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
//    int insertMultiple(MultiRowInsertStatementProvider<Role> multipleInsertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @ResultMap("TbRoleResult")
//    Optional<Role> selectOne(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @Results(id="TbRoleResult", value = {
//        @Result(column="role_id", property="roleId", jdbcType=JdbcType.VARCHAR, id=true),
//        @Result(column="role_name", property="roleName", jdbcType=JdbcType.VARCHAR)
//    })
//    List<Role> selectMany(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
//    int update(UpdateStatementProvider updateStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default long count(CountDSLCompleter completer) {
//        return MyBatis3Utils.countFrom(this::count, tbRole, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int delete(DeleteDSLCompleter completer) {
//        return MyBatis3Utils.deleteFrom(this::delete, tbRole, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int deleteByPrimaryKey(String roleId_) {
//        return delete(c ->
//            c.where(roleId, isEqualTo(roleId_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insert(Role record) {
//        return MyBatis3Utils.insert(this::insert, record, tbRole, c ->
//            c.map(roleId).toProperty("roleId")
//            .map(roleName).toProperty("roleName")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertMultiple(Collection<Role> records) {
//        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, tbRole, c ->
//            c.map(roleId).toProperty("roleId")
//            .map(roleName).toProperty("roleName")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertSelective(Role record) {
//        return MyBatis3Utils.insert(this::insert, record, tbRole, c ->
//            c.map(roleId).toPropertyWhenPresent("roleId", record::getRoleId)
//            .map(roleName).toPropertyWhenPresent("roleName", record::getRoleName)
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<Role> selectOne(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectOne(this::selectOne, selectList, tbRole, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<Role> select(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectList(this::selectMany, selectList, tbRole, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<Role> selectDistinct(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, tbRole, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<Role> selectByPrimaryKey(String roleId_) {
//        return selectOne(c ->
//            c.where(roleId, isEqualTo(roleId_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int update(UpdateDSLCompleter completer) {
//        return MyBatis3Utils.update(this::update, tbRole, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateAllColumns(Role record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(roleId).equalTo(record::getRoleId)
//                .set(roleName).equalTo(record::getRoleName);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateSelectiveColumns(Role record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(roleId).equalToWhenPresent(record::getRoleId)
//                .set(roleName).equalToWhenPresent(record::getRoleName);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKey(Role record) {
//        return update(c ->
//            c.set(roleName).equalTo(record::getRoleName)
//            .where(roleId, isEqualTo(record::getRoleId))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKeySelective(Role record) {
//        return update(c ->
//            c.set(roleName).equalToWhenPresent(record::getRoleName)
//            .where(roleId, isEqualTo(record::getRoleId))
//        );
//    }
//}
