package edu.zjucst.spb.dao.meeting;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.Meeting;

//
public interface MeetingDao extends BaseMapper<Meeting> {
}
