package edu.zjucst.spb.dao.role;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.Role;

public interface RoleDao extends BaseMapper<Role> {
}
