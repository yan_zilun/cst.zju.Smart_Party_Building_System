package edu.zjucst.spb.dao.message;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.message.UserMessageText;

/**
 * (UserMessageText)表数据库访问层
 *
 * @author Mrli
 * @since 2023-04-05 14:10:48
 */
public interface UserMessageTextDao extends BaseMapper<UserMessageText> {

}

