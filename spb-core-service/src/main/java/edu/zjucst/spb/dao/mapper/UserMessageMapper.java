package edu.zjucst.spb.dao.mapper;

import edu.zjucst.spb.domain.entity.message.UserMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 江中华
 * @since 2024-03-11
 */
@Component
public interface UserMessageMapper extends BaseMapper<UserMessage> {
}
