package edu.zjucst.spb.dao.spbuser;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.SpbUser;

public interface SpbUserDao extends BaseMapper<SpbUser> {
}
