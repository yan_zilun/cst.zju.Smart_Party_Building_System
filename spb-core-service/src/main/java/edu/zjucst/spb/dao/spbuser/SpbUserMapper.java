//package edu.zjucst.spb.dao.spbuser;
//
//import static edu.zjucst.spb.dao.spbuser.SpbUserDynamicSqlSupport.*;
//import static org.mybatis.dynamic.sql.SqlBuilder.*;
//
//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
//import edu.zjucst.spb.domain.entity.SpbUser;
//import java.util.Collection;
//import java.util.List;
//import java.util.Optional;
//import javax.annotation.Generated;
//
//import edu.zjucst.spb.domain.entity.Stage;
//import org.apache.ibatis.annotations.DeleteProvider;
//import org.apache.ibatis.annotations.InsertProvider;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Result;
//import org.apache.ibatis.annotations.ResultMap;
//import org.apache.ibatis.annotations.Results;
//import org.apache.ibatis.annotations.SelectProvider;
//import org.apache.ibatis.annotations.UpdateProvider;
//import org.apache.ibatis.type.JdbcType;
//import org.mybatis.dynamic.sql.BasicColumn;
//import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
//import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
//import org.mybatis.dynamic.sql.select.CountDSLCompleter;
//import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
//import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
//import org.mybatis.dynamic.sql.update.UpdateDSL;
//import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
//import org.mybatis.dynamic.sql.update.UpdateModel;
//import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
//import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
//import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
//
//@Mapper
//public interface SpbUserMapper {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    BasicColumn[] selectList = BasicColumn.columnList(id, username, pwd, userNumber, roleName, registerTime, sex, birthday, nationality, politicsStatus, address, identityId, qualification, readingStatus, leagueNum, leagueJoinTime, ifApply, developmentPhase, partyJoinTime, partyBranch, phone, email);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    long count(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
//    int delete(DeleteStatementProvider deleteStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
//    int insert(InsertStatementProvider<SpbUser> insertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
//    int insertMultiple(MultiRowInsertStatementProvider<SpbUser> multipleInsertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @ResultMap("SpbUserResult")
//    Optional<SpbUser> selectOne(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @Results(id="SpbUserResult", value = {
//        @Result(column="id", property="id", jdbcType=JdbcType.BIGINT, id=true),
//        @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR),
//        @Result(column="pwd", property="pwd", jdbcType=JdbcType.VARCHAR),
//        @Result(column="user_number", property="userNumber", jdbcType=JdbcType.VARCHAR),
//        @Result(column="role_name", property="roleName", jdbcType=JdbcType.VARCHAR),
//        @Result(column="register_time", property="registerTime", jdbcType=JdbcType.DATE),
//        @Result(column="sex", property="sex", jdbcType=JdbcType.INTEGER),
//        @Result(column="birthday", property="birthday", jdbcType=JdbcType.DATE),
//        @Result(column="nationality", property="nationality", jdbcType=JdbcType.VARCHAR),
//        @Result(column="politics_status", property="politicsStatus", jdbcType=JdbcType.VARCHAR),
//        @Result(column="address", property="address", jdbcType=JdbcType.VARCHAR),
//        @Result(column="identity_id", property="identityId", jdbcType=JdbcType.VARCHAR),
//        @Result(column="qualification", property="qualification", jdbcType=JdbcType.VARCHAR),
//        @Result(column="reading_status", property="readingStatus", jdbcType=JdbcType.VARCHAR),
//        @Result(column="league_num", property="leagueNum", jdbcType=JdbcType.VARCHAR),
//        @Result(column="league_join_time", property="leagueJoinTime", jdbcType=JdbcType.DATE),
//        @Result(column="if_apply", property="ifApply", jdbcType=JdbcType.INTEGER),
//        @Result(column="development_phase", property="developmentPhase", jdbcType=JdbcType.VARCHAR),
//        @Result(column="party_join_time", property="partyJoinTime", jdbcType=JdbcType.DATE),
//        @Result(column="party_branch", property="partyBranch", jdbcType=JdbcType.VARCHAR),
//        @Result(column="phone", property="phone", jdbcType=JdbcType.CHAR),
//        @Result(column="email", property="email", jdbcType=JdbcType.VARCHAR)
//    })
//    List<SpbUser> selectMany(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
//    int update(UpdateStatementProvider updateStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default long count(CountDSLCompleter completer) {
//        return MyBatis3Utils.countFrom(this::count, spbUser, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int delete(DeleteDSLCompleter completer) {
//        return MyBatis3Utils.deleteFrom(this::delete, spbUser, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int deleteByPrimaryKey(Long id_) {
//        return delete(c ->
//            c.where(id, isEqualTo(id_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insert(SpbUser record) {
//        return MyBatis3Utils.insert(this::insert, record, spbUser, c ->
//            c.map(id).toProperty("id")
//            .map(username).toProperty("username")
//            .map(pwd).toProperty("pwd")
//            .map(userNumber).toProperty("userNumber")
//            .map(roleName).toProperty("roleName")
//            .map(registerTime).toProperty("registerTime")
//            .map(sex).toProperty("sex")
//            .map(birthday).toProperty("birthday")
//            .map(nationality).toProperty("nationality")
//            .map(politicsStatus).toProperty("politicsStatus")
//            .map(address).toProperty("address")
//            .map(identityId).toProperty("identityId")
//            .map(qualification).toProperty("qualification")
//            .map(readingStatus).toProperty("readingStatus")
//            .map(leagueNum).toProperty("leagueNum")
//            .map(leagueJoinTime).toProperty("leagueJoinTime")
//            .map(ifApply).toProperty("ifApply")
//            .map(developmentPhase).toProperty("developmentPhase")
//            .map(partyJoinTime).toProperty("partyJoinTime")
//            .map(partyBranch).toProperty("partyBranch")
//            .map(phone).toProperty("phone")
//            .map(email).toProperty("email")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertMultiple(Collection<SpbUser> records) {
//        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, spbUser, c ->
//            c.map(id).toProperty("id")
//            .map(username).toProperty("username")
//            .map(pwd).toProperty("pwd")
//            .map(userNumber).toProperty("userNumber")
//            .map(roleName).toProperty("roleName")
//            .map(registerTime).toProperty("registerTime")
//            .map(sex).toProperty("sex")
//            .map(birthday).toProperty("birthday")
//            .map(nationality).toProperty("nationality")
//            .map(politicsStatus).toProperty("politicsStatus")
//            .map(address).toProperty("address")
//            .map(identityId).toProperty("identityId")
//            .map(qualification).toProperty("qualification")
//            .map(readingStatus).toProperty("readingStatus")
//            .map(leagueNum).toProperty("leagueNum")
//            .map(leagueJoinTime).toProperty("leagueJoinTime")
//            .map(ifApply).toProperty("ifApply")
//            .map(developmentPhase).toProperty("developmentPhase")
//            .map(partyJoinTime).toProperty("partyJoinTime")
//            .map(partyBranch).toProperty("partyBranch")
//            .map(phone).toProperty("phone")
//            .map(email).toProperty("email")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertSelective(SpbUser record) {
//        return MyBatis3Utils.insert(this::insert, record, spbUser, c ->
//            c.map(id).toPropertyWhenPresent("id", record::getId)
//            .map(username).toPropertyWhenPresent("username", record::getUsername)
//            .map(pwd).toPropertyWhenPresent("pwd", record::getPwd)
//            .map(userNumber).toPropertyWhenPresent("userNumber", record::getUserNumber)
//            .map(roleName).toPropertyWhenPresent("roleName", record::getRoleName)
//            .map(registerTime).toPropertyWhenPresent("registerTime", record::getRegisterTime)
//            .map(sex).toPropertyWhenPresent("sex", record::getSex)
//            .map(birthday).toPropertyWhenPresent("birthday", record::getBirthday)
//            .map(nationality).toPropertyWhenPresent("nationality", record::getNationality)
//            .map(politicsStatus).toPropertyWhenPresent("politicsStatus", record::getPoliticsStatus)
//            .map(address).toPropertyWhenPresent("address", record::getAddress)
//            .map(identityId).toPropertyWhenPresent("identityId", record::getIdentityId)
//            .map(qualification).toPropertyWhenPresent("qualification", record::getQualification)
//            .map(readingStatus).toPropertyWhenPresent("readingStatus", record::getReadingStatus)
//            .map(leagueNum).toPropertyWhenPresent("leagueNum", record::getLeagueNum)
//            .map(leagueJoinTime).toPropertyWhenPresent("leagueJoinTime", record::getLeagueJoinTime)
//            .map(ifApply).toPropertyWhenPresent("ifApply", record::getIfApply)
//            .map(developmentPhase).toPropertyWhenPresent("developmentPhase", record::getDevelopmentPhase)
//            .map(partyJoinTime).toPropertyWhenPresent("partyJoinTime", record::getPartyJoinTime)
//            .map(partyBranch).toPropertyWhenPresent("partyBranch", record::getPartyBranch)
//            .map(phone).toPropertyWhenPresent("phone", record::getPhone)
//            .map(email).toPropertyWhenPresent("email", record::getEmail)
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<SpbUser> selectOne(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectOne(this::selectOne, selectList, spbUser, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<SpbUser> select(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectList(this::selectMany, selectList, spbUser, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<SpbUser> selectDistinct(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, spbUser, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<SpbUser> selectByPrimaryKey(Long id_) {
//        return selectOne(c ->
//            c.where(id, isEqualTo(id_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int update(UpdateDSLCompleter completer) {
//        return MyBatis3Utils.update(this::update, spbUser, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateAllColumns(SpbUser record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(id).equalTo(record::getId)
//                .set(username).equalTo(record::getUsername)
//                .set(pwd).equalTo(record::getPwd)
//                .set(userNumber).equalTo(record::getUserNumber)
//                .set(roleName).equalTo(record::getRoleName)
//                .set(registerTime).equalTo(record::getRegisterTime)
//                .set(sex).equalTo(record::getSex)
//                .set(birthday).equalTo(record::getBirthday)
//                .set(nationality).equalTo(record::getNationality)
//                .set(politicsStatus).equalTo(record::getPoliticsStatus)
//                .set(address).equalTo(record::getAddress)
//                .set(identityId).equalTo(record::getIdentityId)
//                .set(qualification).equalTo(record::getQualification)
//                .set(readingStatus).equalTo(record::getReadingStatus)
//                .set(leagueNum).equalTo(record::getLeagueNum)
//                .set(leagueJoinTime).equalTo(record::getLeagueJoinTime)
//                .set(ifApply).equalTo(record::getIfApply)
//                .set(developmentPhase).equalTo(record::getDevelopmentPhase)
//                .set(partyJoinTime).equalTo(record::getPartyJoinTime)
//                .set(partyBranch).equalTo(record::getPartyBranch)
//                .set(phone).equalTo(record::getPhone)
//                .set(email).equalTo(record::getEmail);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateSelectiveColumns(SpbUser record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(id).equalToWhenPresent(record::getId)
//                .set(username).equalToWhenPresent(record::getUsername)
//                .set(pwd).equalToWhenPresent(record::getPwd)
//                .set(userNumber).equalToWhenPresent(record::getUserNumber)
//                .set(roleName).equalToWhenPresent(record::getRoleName)
//                .set(registerTime).equalToWhenPresent(record::getRegisterTime)
//                .set(sex).equalToWhenPresent(record::getSex)
//                .set(birthday).equalToWhenPresent(record::getBirthday)
//                .set(nationality).equalToWhenPresent(record::getNationality)
//                .set(politicsStatus).equalToWhenPresent(record::getPoliticsStatus)
//                .set(address).equalToWhenPresent(record::getAddress)
//                .set(identityId).equalToWhenPresent(record::getIdentityId)
//                .set(qualification).equalToWhenPresent(record::getQualification)
//                .set(readingStatus).equalToWhenPresent(record::getReadingStatus)
//                .set(leagueNum).equalToWhenPresent(record::getLeagueNum)
//                .set(leagueJoinTime).equalToWhenPresent(record::getLeagueJoinTime)
//                .set(ifApply).equalToWhenPresent(record::getIfApply)
//                .set(developmentPhase).equalToWhenPresent(record::getDevelopmentPhase)
//                .set(partyJoinTime).equalToWhenPresent(record::getPartyJoinTime)
//                .set(partyBranch).equalToWhenPresent(record::getPartyBranch)
//                .set(phone).equalToWhenPresent(record::getPhone)
//                .set(email).equalToWhenPresent(record::getEmail);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKey(SpbUser record) {
//        return update(c ->
//            c.set(username).equalTo(record::getUsername)
//            .set(pwd).equalTo(record::getPwd)
//            .set(userNumber).equalTo(record::getUserNumber)
//            .set(roleName).equalTo(record::getRoleName)
//            .set(registerTime).equalTo(record::getRegisterTime)
//            .set(sex).equalTo(record::getSex)
//            .set(birthday).equalTo(record::getBirthday)
//            .set(nationality).equalTo(record::getNationality)
//            .set(politicsStatus).equalTo(record::getPoliticsStatus)
//            .set(address).equalTo(record::getAddress)
//            .set(identityId).equalTo(record::getIdentityId)
//            .set(qualification).equalTo(record::getQualification)
//            .set(readingStatus).equalTo(record::getReadingStatus)
//            .set(leagueNum).equalTo(record::getLeagueNum)
//            .set(leagueJoinTime).equalTo(record::getLeagueJoinTime)
//            .set(ifApply).equalTo(record::getIfApply)
//            .set(developmentPhase).equalTo(record::getDevelopmentPhase)
//            .set(partyJoinTime).equalTo(record::getPartyJoinTime)
//            .set(partyBranch).equalTo(record::getPartyBranch)
//            .set(phone).equalTo(record::getPhone)
//            .set(email).equalTo(record::getEmail)
//            .where(id, isEqualTo(record::getId))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKeySelective(SpbUser record) {
//        return update(c ->
//            c.set(username).equalToWhenPresent(record::getUsername)
//            .set(pwd).equalToWhenPresent(record::getPwd)
//            .set(userNumber).equalToWhenPresent(record::getUserNumber)
//            .set(roleName).equalToWhenPresent(record::getRoleName)
//            .set(registerTime).equalToWhenPresent(record::getRegisterTime)
//            .set(sex).equalToWhenPresent(record::getSex)
//            .set(birthday).equalToWhenPresent(record::getBirthday)
//            .set(nationality).equalToWhenPresent(record::getNationality)
//            .set(politicsStatus).equalToWhenPresent(record::getPoliticsStatus)
//            .set(address).equalToWhenPresent(record::getAddress)
//            .set(identityId).equalToWhenPresent(record::getIdentityId)
//            .set(qualification).equalToWhenPresent(record::getQualification)
//            .set(readingStatus).equalToWhenPresent(record::getReadingStatus)
//            .set(leagueNum).equalToWhenPresent(record::getLeagueNum)
//            .set(leagueJoinTime).equalToWhenPresent(record::getLeagueJoinTime)
//            .set(ifApply).equalToWhenPresent(record::getIfApply)
//            .set(developmentPhase).equalToWhenPresent(record::getDevelopmentPhase)
//            .set(partyJoinTime).equalToWhenPresent(record::getPartyJoinTime)
//            .set(partyBranch).equalToWhenPresent(record::getPartyBranch)
//            .set(phone).equalToWhenPresent(record::getPhone)
//            .set(email).equalToWhenPresent(record::getEmail)
//            .where(id, isEqualTo(record::getId))
//        );
//    }
//}
