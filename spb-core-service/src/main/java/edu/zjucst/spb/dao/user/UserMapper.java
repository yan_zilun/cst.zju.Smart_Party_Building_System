//package edu.zjucst.spb.dao.user;
//
//import org.apache.ibatis.annotations.*;
//import org.apache.ibatis.type.JdbcType;
//import org.mybatis.dynamic.sql.BasicColumn;
//import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
//import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
//import org.mybatis.dynamic.sql.select.CountDSLCompleter;
//import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
//import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
//import org.mybatis.dynamic.sql.update.UpdateDSL;
//import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
//import org.mybatis.dynamic.sql.update.UpdateModel;
//import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
//import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
//import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
//
//import javax.annotation.Generated;
//import java.util.Collection;
//import java.util.List;
//import java.util.Optional;
//
//import static edu.zjucst.spb.dao.user.UserDynamicSqlSupport.*;
//import static org.mybatis.dynamic.sql.SqlBuilder.isEqualTo;
//
//@Mapper
//public interface UserMapper {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    BasicColumn[] selectList = BasicColumn.columnList(id, rev, first, last, displayName, email, pwd, pictureId, tenantId, username);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    long count(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
//    int delete(DeleteStatementProvider deleteStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
//    int insert(InsertStatementProvider<User> insertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
//    int insertMultiple(MultiRowInsertStatementProvider<User> multipleInsertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @ResultMap("UserResult")
//    Optional<User> selectOne(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @Results(id="UserResult", value = {
//        @Result(column="ID_", property="id", jdbcType=JdbcType.VARCHAR, id=true),
//        @Result(column="REV_", property="rev", jdbcType=JdbcType.INTEGER),
//        @Result(column="FIRST_", property="first", jdbcType=JdbcType.VARCHAR),
//        @Result(column="LAST_", property="last", jdbcType=JdbcType.VARCHAR),
//        @Result(column="DISPLAY_NAME_", property="displayName", jdbcType=JdbcType.VARCHAR),
//        @Result(column="EMAIL_", property="email", jdbcType=JdbcType.VARCHAR),
//        @Result(column="PWD_", property="pwd", jdbcType=JdbcType.VARCHAR),
//        @Result(column="PICTURE_ID_", property="pictureId", jdbcType=JdbcType.VARCHAR),
//        @Result(column="TENANT_ID_", property="tenantId", jdbcType=JdbcType.VARCHAR),
//        @Result(column="username", property="username", jdbcType=JdbcType.VARCHAR)
//    })
//    List<User> selectMany(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
//    int update(UpdateStatementProvider updateStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default long count(CountDSLCompleter completer) {
//        return MyBatis3Utils.countFrom(this::count, user, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int delete(DeleteDSLCompleter completer) {
//        return MyBatis3Utils.deleteFrom(this::delete, user, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int deleteByPrimaryKey(String id_) {
//        return delete(c ->
//            c.where(id, isEqualTo(id_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insert(User record) {
//        return MyBatis3Utils.insert(this::insert, record, user, c ->
//            c.map(id).toProperty("id")
//            .map(rev).toProperty("rev")
//            .map(first).toProperty("first")
//            .map(last).toProperty("last")
//            .map(displayName).toProperty("displayName")
//            .map(email).toProperty("email")
//            .map(pwd).toProperty("pwd")
//            .map(pictureId).toProperty("pictureId")
//            .map(tenantId).toProperty("tenantId")
//            .map(username).toProperty("username")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertMultiple(Collection<User> records) {
//        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, user, c ->
//            c.map(id).toProperty("id")
//            .map(rev).toProperty("rev")
//            .map(first).toProperty("first")
//            .map(last).toProperty("last")
//            .map(displayName).toProperty("displayName")
//            .map(email).toProperty("email")
//            .map(pwd).toProperty("pwd")
//            .map(pictureId).toProperty("pictureId")
//            .map(tenantId).toProperty("tenantId")
//            .map(username).toProperty("username")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertSelective(User record) {
//        return MyBatis3Utils.insert(this::insert, record, user, c ->
//            c.map(id).toPropertyWhenPresent("id", record::getId)
//            .map(rev).toPropertyWhenPresent("rev", record::getRev)
//            .map(first).toPropertyWhenPresent("first", record::getFirst)
//            .map(last).toPropertyWhenPresent("last", record::getLast)
//            .map(displayName).toPropertyWhenPresent("displayName", record::getDisplayName)
//            .map(email).toPropertyWhenPresent("email", record::getEmail)
//            .map(pwd).toPropertyWhenPresent("pwd", record::getPwd)
//            .map(pictureId).toPropertyWhenPresent("pictureId", record::getPictureId)
//            .map(tenantId).toPropertyWhenPresent("tenantId", record::getTenantId)
//            .map(username).toPropertyWhenPresent("username", record::getUsername)
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<User> selectOne(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectOne(this::selectOne, selectList, user, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<User> select(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectList(this::selectMany, selectList, user, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<User> selectDistinct(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, user, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<User> selectByPrimaryKey(String id_) {
//        return selectOne(c ->
//            c.where(id, isEqualTo(id_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int update(UpdateDSLCompleter completer) {
//        return MyBatis3Utils.update(this::update, user, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateAllColumns(User record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(id).equalTo(record::getId)
//                .set(rev).equalTo(record::getRev)
//                .set(first).equalTo(record::getFirst)
//                .set(last).equalTo(record::getLast)
//                .set(displayName).equalTo(record::getDisplayName)
//                .set(email).equalTo(record::getEmail)
//                .set(pwd).equalTo(record::getPwd)
//                .set(pictureId).equalTo(record::getPictureId)
//                .set(tenantId).equalTo(record::getTenantId)
//                .set(username).equalTo(record::getUsername);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateSelectiveColumns(User record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(id).equalToWhenPresent(record::getId)
//                .set(rev).equalToWhenPresent(record::getRev)
//                .set(first).equalToWhenPresent(record::getFirst)
//                .set(last).equalToWhenPresent(record::getLast)
//                .set(displayName).equalToWhenPresent(record::getDisplayName)
//                .set(email).equalToWhenPresent(record::getEmail)
//                .set(pwd).equalToWhenPresent(record::getPwd)
//                .set(pictureId).equalToWhenPresent(record::getPictureId)
//                .set(tenantId).equalToWhenPresent(record::getTenantId)
//                .set(username).equalToWhenPresent(record::getUsername);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKey(User record) {
//        return update(c ->
//            c.set(rev).equalTo(record::getRev)
//            .set(first).equalTo(record::getFirst)
//            .set(last).equalTo(record::getLast)
//            .set(displayName).equalTo(record::getDisplayName)
//            .set(email).equalTo(record::getEmail)
//            .set(pwd).equalTo(record::getPwd)
//            .set(pictureId).equalTo(record::getPictureId)
//            .set(tenantId).equalTo(record::getTenantId)
//            .set(username).equalTo(record::getUsername)
//            .where(id, isEqualTo(record::getId))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKeySelective(User record) {
//        return update(c ->
//            c.set(rev).equalToWhenPresent(record::getRev)
//            .set(first).equalToWhenPresent(record::getFirst)
//            .set(last).equalToWhenPresent(record::getLast)
//            .set(displayName).equalToWhenPresent(record::getDisplayName)
//            .set(email).equalToWhenPresent(record::getEmail)
//            .set(pwd).equalToWhenPresent(record::getPwd)
//            .set(pictureId).equalToWhenPresent(record::getPictureId)
//            .set(tenantId).equalToWhenPresent(record::getTenantId)
//            .set(username).equalToWhenPresent(record::getUsername)
//            .where(id, isEqualTo(record::getId))
//        );
//    }
//}
