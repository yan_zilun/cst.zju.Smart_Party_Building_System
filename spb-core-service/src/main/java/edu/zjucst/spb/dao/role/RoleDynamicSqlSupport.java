//package edu.zjucst.spb.dao.role;
//
//import org.mybatis.dynamic.sql.SqlColumn;
//import org.mybatis.dynamic.sql.SqlTable;
//
//import javax.annotation.Generated;
//import java.sql.JDBCType;
//
//public final class RoleDynamicSqlSupport {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final Role tbRole = new Role();
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Long> roleId = tbRole.roleId;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> roleName = tbRole.roleName;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final class Role extends SqlTable {
//        public final SqlColumn<String> roleId = column("role_id", JDBCType.LONGNVARCHAR);
//
//        public final SqlColumn<String> roleName = column("role_name", JDBCType.VARCHAR);
//
//        public Role() {
//            super("tb_role");
//        }
//    }
//}
