//package edu.zjucst.spb.dao.branch;
//
//import static edu.zjucst.spb.dao.branch.BranchDynamicSqlSupport.*;
//import static org.mybatis.dynamic.sql.SqlBuilder.*;
//
//import edu.zjucst.spb.domain.entity.Branch;
//import java.util.Collection;
//import java.util.List;
//import java.util.Optional;
//import javax.annotation.Generated;
//import org.apache.ibatis.annotations.DeleteProvider;
//import org.apache.ibatis.annotations.InsertProvider;
//import org.apache.ibatis.annotations.Mapper;
//import org.apache.ibatis.annotations.Result;
//import org.apache.ibatis.annotations.ResultMap;
//import org.apache.ibatis.annotations.Results;
//import org.apache.ibatis.annotations.SelectProvider;
//import org.apache.ibatis.annotations.UpdateProvider;
//import org.apache.ibatis.type.JdbcType;
//import org.mybatis.dynamic.sql.BasicColumn;
//import org.mybatis.dynamic.sql.delete.DeleteDSLCompleter;
//import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.InsertStatementProvider;
//import org.mybatis.dynamic.sql.insert.render.MultiRowInsertStatementProvider;
//import org.mybatis.dynamic.sql.select.CountDSLCompleter;
//import org.mybatis.dynamic.sql.select.SelectDSLCompleter;
//import org.mybatis.dynamic.sql.select.render.SelectStatementProvider;
//import org.mybatis.dynamic.sql.update.UpdateDSL;
//import org.mybatis.dynamic.sql.update.UpdateDSLCompleter;
//import org.mybatis.dynamic.sql.update.UpdateModel;
//import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
//import org.mybatis.dynamic.sql.util.SqlProviderAdapter;
//import org.mybatis.dynamic.sql.util.mybatis3.MyBatis3Utils;
//
//@Mapper
//public interface BranchMapper {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    BasicColumn[] selectList = BasicColumn.columnList(branchId, branchName, parentBranchId, branchInfo);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    long count(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @DeleteProvider(type=SqlProviderAdapter.class, method="delete")
//    int delete(DeleteStatementProvider deleteStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insert")
//    int insert(InsertStatementProvider<Branch> insertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @InsertProvider(type=SqlProviderAdapter.class, method="insertMultiple")
//    int insertMultiple(MultiRowInsertStatementProvider<Branch> multipleInsertStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @ResultMap("BranchResult")
//    Optional<Branch> selectOne(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @SelectProvider(type=SqlProviderAdapter.class, method="select")
//    @Results(id="BranchResult", value = {
//        @Result(column="branch_id", property="branchId", jdbcType=JdbcType.BIGINT, id=true),
//        @Result(column="branch_name", property="branchName", jdbcType=JdbcType.VARCHAR),
//        @Result(column="parent_branch_id", property="parentBranchId", jdbcType=JdbcType.BIGINT),
//        @Result(column="branch_info", property="branchInfo", jdbcType=JdbcType.VARCHAR)
//    })
//    List<Branch> selectMany(SelectStatementProvider selectStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    @UpdateProvider(type=SqlProviderAdapter.class, method="update")
//    int update(UpdateStatementProvider updateStatement);
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default long count(CountDSLCompleter completer) {
//        return MyBatis3Utils.countFrom(this::count, branch, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int delete(DeleteDSLCompleter completer) {
//        return MyBatis3Utils.deleteFrom(this::delete, branch, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int deleteByPrimaryKey(Long branchId_) {
//        return delete(c ->
//            c.where(branchId, isEqualTo(branchId_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insert(Branch record) {
//        return MyBatis3Utils.insert(this::insert, record, branch, c ->
//            c.map(branchId).toProperty("branchId")
//            .map(branchName).toProperty("branchName")
//            .map(parentBranchId).toProperty("parentBranchId")
//            .map(branchInfo).toProperty("branchInfo")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertMultiple(Collection<Branch> records) {
//        return MyBatis3Utils.insertMultiple(this::insertMultiple, records, branch, c ->
//            c.map(branchId).toProperty("branchId")
//            .map(branchName).toProperty("branchName")
//            .map(parentBranchId).toProperty("parentBranchId")
//            .map(branchInfo).toProperty("branchInfo")
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int insertSelective(Branch record) {
//        return MyBatis3Utils.insert(this::insert, record, branch, c ->
//            c.map(branchId).toPropertyWhenPresent("branchId", record::getBranchId)
//            .map(branchName).toPropertyWhenPresent("branchName", record::getBranchName)
//            .map(parentBranchId).toPropertyWhenPresent("parentBranchId", record::getParentBranchId)
//            .map(branchInfo).toPropertyWhenPresent("branchInfo", record::getBranchInfo)
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<Branch> selectOne(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectOne(this::selectOne, selectList, branch, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<Branch> select(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectList(this::selectMany, selectList, branch, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default List<Branch> selectDistinct(SelectDSLCompleter completer) {
//        return MyBatis3Utils.selectDistinct(this::selectMany, selectList, branch, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default Optional<Branch> selectByPrimaryKey(Long branchId_) {
//        return selectOne(c ->
//            c.where(branchId, isEqualTo(branchId_))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int update(UpdateDSLCompleter completer) {
//        return MyBatis3Utils.update(this::update, branch, completer);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateAllColumns(Branch record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(branchId).equalTo(record::getBranchId)
//                .set(branchName).equalTo(record::getBranchName)
//                .set(parentBranchId).equalTo(record::getParentBranchId)
//                .set(branchInfo).equalTo(record::getBranchInfo);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    static UpdateDSL<UpdateModel> updateSelectiveColumns(Branch record, UpdateDSL<UpdateModel> dsl) {
//        return dsl.set(branchId).equalToWhenPresent(record::getBranchId)
//                .set(branchName).equalToWhenPresent(record::getBranchName)
//                .set(parentBranchId).equalToWhenPresent(record::getParentBranchId)
//                .set(branchInfo).equalToWhenPresent(record::getBranchInfo);
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKey(Branch record) {
//        return update(c ->
//            c.set(branchName).equalTo(record::getBranchName)
//            .set(parentBranchId).equalTo(record::getParentBranchId)
//            .set(branchInfo).equalTo(record::getBranchInfo)
//            .where(branchId, isEqualTo(record::getBranchId))
//        );
//    }
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    default int updateByPrimaryKeySelective(Branch record) {
//        return update(c ->
//            c.set(branchName).equalToWhenPresent(record::getBranchName)
//            .set(parentBranchId).equalToWhenPresent(record::getParentBranchId)
//            .set(branchInfo).equalToWhenPresent(record::getBranchInfo)
//            .where(branchId, isEqualTo(record::getBranchId))
//        );
//    }
//}
