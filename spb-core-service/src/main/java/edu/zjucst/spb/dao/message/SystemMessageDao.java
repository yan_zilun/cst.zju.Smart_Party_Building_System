package edu.zjucst.spb.dao.message;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.message.SystemMessage;


/**
 * (SystemMessage)表数据库访问层
 *
 * @author Mrli
 * @since 2023-04-05 13:34:53
 */
public interface SystemMessageDao extends BaseMapper<SystemMessage> {

}

