//package edu.zjucst.spb.dao.spbuser;
//
//import java.sql.JDBCType;
//import java.util.Date;
//import javax.annotation.Generated;
//import org.mybatis.dynamic.sql.SqlColumn;
//import org.mybatis.dynamic.sql.SqlTable;
//
//public final class SpbUserDynamicSqlSupport {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SpbUser spbUser = new SpbUser();
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Long> id = spbUser.id;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> username = spbUser.username;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> pwd = spbUser.pwd;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> userNumber = spbUser.userNumber;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> roleName = spbUser.roleName;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Date> registerTime = spbUser.registerTime;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Integer> sex = spbUser.sex;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Date> birthday = spbUser.birthday;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> nationality = spbUser.nationality;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> politicsStatus = spbUser.politicsStatus;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> address = spbUser.address;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> identityId = spbUser.identityId;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> qualification = spbUser.qualification;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> readingStatus = spbUser.readingStatus;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> leagueNum = spbUser.leagueNum;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Date> leagueJoinTime = spbUser.leagueJoinTime;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Integer> ifApply = spbUser.ifApply;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> developmentPhase = spbUser.developmentPhase;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Date> partyJoinTime = spbUser.partyJoinTime;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> partyBranch = spbUser.partyBranch;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> phone = spbUser.phone;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> email = spbUser.email;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final class SpbUser extends SqlTable {
//        public final SqlColumn<Long> id = column("id", JDBCType.BIGINT);
//
//        public final SqlColumn<String> username = column("username", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> pwd = column("pwd", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> userNumber = column("user_number", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> roleName = column("role_name", JDBCType.VARCHAR);
//
//        public final SqlColumn<Date> registerTime = column("register_time", JDBCType.DATE);
//
//        public final SqlColumn<Integer> sex = column("sex", JDBCType.INTEGER);
//
//        public final SqlColumn<Date> birthday = column("birthday", JDBCType.DATE);
//
//        public final SqlColumn<String> nationality = column("nationality", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> politicsStatus = column("politics_status", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> address = column("address", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> identityId = column("identity_id", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> qualification = column("qualification", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> readingStatus = column("reading_status", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> leagueNum = column("league_num", JDBCType.VARCHAR);
//
//        public final SqlColumn<Date> leagueJoinTime = column("league_join_time", JDBCType.DATE);
//
//        public final SqlColumn<Integer> ifApply = column("if_apply", JDBCType.INTEGER);
//
//        public final SqlColumn<String> developmentPhase = column("development_phase", JDBCType.VARCHAR);
//
//        public final SqlColumn<Date> partyJoinTime = column("party_join_time", JDBCType.DATE);
//
//        public final SqlColumn<String> partyBranch = column("party_branch", JDBCType.VARCHAR);
//
//        public final SqlColumn<String> phone = column("phone", JDBCType.CHAR);
//
//        public final SqlColumn<String> email = column("email", JDBCType.VARCHAR);
//
//        public SpbUser() {
//            super("tb_spb_user");
//        }
//    }
//}
