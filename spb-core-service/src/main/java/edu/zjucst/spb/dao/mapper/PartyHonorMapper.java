package edu.zjucst.spb.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.PartyHonor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 18:20
 */
@Mapper
public interface PartyHonorMapper extends BaseMapper<PartyHonor> {
}
