package edu.zjucst.spb.dao.mapper.stage;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.stage.ApplicantStage;
import org.springframework.stereotype.Repository;

/**
 * @author ZlunYan
 * @description
 * @create 2024-5-25
 */

@Repository
public interface ApplicantStageMapper extends BaseMapper<ApplicantStage> {
}
