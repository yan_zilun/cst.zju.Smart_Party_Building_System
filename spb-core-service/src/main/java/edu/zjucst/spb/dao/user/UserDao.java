package edu.zjucst.spb.dao.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.SpbUser;

/**
 * (User)表数据库访问层
 *
 * @author Mrli
 * @since 2023-04-05 19:33:11
 */
public interface UserDao extends BaseMapper<SpbUser> {

}

