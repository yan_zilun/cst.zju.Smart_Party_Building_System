package edu.zjucst.spb.dao.mapper;

import edu.zjucst.spb.domain.entity.SelfActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author qingo
 * @since 2023-04-18
 */
@Component
public interface SelfActivityMapper extends BaseMapper<SelfActivity> {

}
