package edu.zjucst.spb.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.Activity;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivityMapper extends BaseMapper<Activity> {
}
