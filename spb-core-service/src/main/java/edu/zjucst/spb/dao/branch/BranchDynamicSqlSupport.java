//package edu.zjucst.spb.dao.branch;
//
//import org.mybatis.dynamic.sql.SqlColumn;
//import org.mybatis.dynamic.sql.SqlTable;
//
//import javax.annotation.Generated;
//import java.sql.JDBCType;
//
//public final class BranchDynamicSqlSupport {
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final Branch branch = new Branch();
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Long> branchId = branch.branchId;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> branchName = branch.branchName;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<Long> parentBranchId = branch.parentBranchId;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final SqlColumn<String> branchInfo = branch.branchInfo;
//
//    @Generated("org.mybatis.generator.api.MyBatisGenerator")
//    public static final class Branch extends SqlTable {
//        public final SqlColumn<Long> branchId = column("branch_id", JDBCType.BIGINT);
//
//        public final SqlColumn<String> branchName = column("branch_name", JDBCType.VARCHAR);
//
//        public final SqlColumn<Long> parentBranchId = column("parent_branch_id", JDBCType.BIGINT);
//
//        public final SqlColumn<String> branchInfo = column("branch_info", JDBCType.VARCHAR);
//
//        public Branch() {
//            super("tb_branch");
//        }
//    }
//}
