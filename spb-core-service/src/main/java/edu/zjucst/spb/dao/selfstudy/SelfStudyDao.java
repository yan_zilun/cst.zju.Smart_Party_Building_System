package edu.zjucst.spb.dao.selfstudy;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.SelfStudy;

public interface SelfStudyDao extends BaseMapper<SelfStudy> {

}
