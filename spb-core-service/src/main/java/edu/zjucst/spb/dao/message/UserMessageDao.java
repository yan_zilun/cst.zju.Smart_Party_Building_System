package edu.zjucst.spb.dao.message;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.dto.MessageQueryDTO;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import edu.zjucst.spb.domain.vo.message.UserNotifyMessageVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (UserMessage)表数据库访问层
 *
 * @author Mrli
 * @since 2023-04-05 14:17:20
 */
public interface UserMessageDao extends BaseMapper<UserMessage> {

    /**
     * 分页查询站内信信息
     *
     * @param recvUserId                接收人id
     * @param MessageQueryParam 分页查询参数
     * @return 查询结果
     */
    List<UserNotifyMessageVO> queryMessage(MessageQueryDTO dto);

    /**
     * 批量修改已读状态
     *
     * @param idList 需要修改的 UserMessage 的 id 列表
     */
    void updateIsReadByIdBatch(@Param("idList") List<Long> idList);
}

