package edu.zjucst.spb.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.Stage;
import org.springframework.stereotype.Repository;

@Repository//代表持久层
public interface StageMapper extends BaseMapper<Stage> {

}
