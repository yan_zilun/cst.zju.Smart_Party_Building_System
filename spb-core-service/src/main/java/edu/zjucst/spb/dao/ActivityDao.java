package edu.zjucst.spb.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.Activity;

/**
* @author 10630
* @description 针对表【spb_activity】的数据库操作Mapper
* @createDate 2022-12-30 18:03:58
* @Entity edu.zjucst.spb.domain.Activity
*/
public interface ActivityDao extends BaseMapper<Activity> {

}




