package edu.zjucst.spb.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import edu.zjucst.spb.domain.entity.BranchActivity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 16:46
 */
@Mapper
public interface BranchActivityMapper extends BaseMapper<BranchActivity> {
}
