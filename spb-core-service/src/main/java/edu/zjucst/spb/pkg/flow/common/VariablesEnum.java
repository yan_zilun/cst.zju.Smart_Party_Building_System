package edu.zjucst.spb.pkg.flow.common;

public enum VariablesEnum {
    /**
     * 院级活动
     */
    COLLEGE_ACTIVITY_LEVEL,

    /**
     * 校级活动
     */
    SCHOOL_ACTIVITY_LEVEL;
}
