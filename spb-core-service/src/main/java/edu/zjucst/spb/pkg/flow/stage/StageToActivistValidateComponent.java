package edu.zjucst.spb.pkg.flow.stage;

import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.service.StageService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * @author ZlunYan
 * @description
 * @create 2023-12-17
 */

@Component
public class StageToActivistValidateComponent implements JavaDelegate {

    private static final int gapMonth = 3;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private StageService stageService;

    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println("更新状态");

        String userId = (String) runtimeService.getVariable(delegateExecution.getId(), "userId");
        Stage stage = stageService.getStageByUserId(userId);

        boolean accept = false;
        if (stage != null && stage.getDeliveryTime() != null) {
            LocalDateTime deliveryTime = stage.getDeliveryTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            LocalDateTime currentDate = LocalDateTime.now();

            accept = currentDate.isAfter(deliveryTime.plusMonths(gapMonth));
        }

        runtimeService.setVariable(delegateExecution.getId(), "accept", accept);
    }
}
