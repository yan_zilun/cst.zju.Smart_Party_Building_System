package edu.zjucst.spb.pkg.flow.stage;

import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.service.StageService;
import liquibase.pro.packaged.A;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-28
 */
@Component
public class StageUpdateComponent implements JavaDelegate {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private StageService stageService;

    @Override
    public void execute(DelegateExecution delegateExecution) {
        System.out.println("更新状态");

        String userId = (String) runtimeService.getVariable(delegateExecution.getId(), "userId");
        Stage stage = stageService.getStageByUserId(userId);

        stage.setDevelopmentPhaseId(stage.getDevelopmentPhaseId()+1);

        System.out.println(stage.getUserName() + " 已经成为 " + stage.getDevelopmentPhase());
        stageService.updateStageByUserId(stage, userId);
    }
}
