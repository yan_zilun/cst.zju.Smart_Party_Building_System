package edu.zjucst.spb.pkg.flow.common;

/**
 * 暴露流程定义中key, 需要与bpmn.xml中的process.id一致
 * 供 processDefinitionKey 查询使用
 */
public interface ProcessDefinition {
    /** 提交活动审批 */
    String SUBMIT_ACTIVITY_PROCESS_KEY = "submit-activity";
}
