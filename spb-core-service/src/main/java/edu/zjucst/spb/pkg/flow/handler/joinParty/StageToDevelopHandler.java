package edu.zjucst.spb.pkg.flow.handler.joinParty;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ZlunYan
 * @description
 * @create 2023-12-17
 */

@Component
public class StageToDevelopHandler implements ExecutionListener {

    @Autowired
    RuntimeService runtimeService;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        runtimeService.setVariable(delegateExecution.getId(), "uploadThought", false);
    }
}
