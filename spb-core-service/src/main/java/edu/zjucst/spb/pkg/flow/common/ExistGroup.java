package edu.zjucst.spb.pkg.flow.common;

/**
 * 存在的组
 */
public interface ExistGroup {
    String STUDENT = "student";
    String SECRETARY = "secretary";
    String COLLEGE = "college";
    String SCHOOL = "school";
}
