package edu.zjucst.spb.pkg.flow.stage;

import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author ZlunYan
 * @description
 * @create 2023-12-17
 */

@Component
public class JoinPartyUpdateThoughtReportTime implements JavaDelegate {

    private static final int gapMonth = 3;

    @Autowired
    private RuntimeService runtimeService;

    @Override
    public void execute(DelegateExecution delegateExecution) {
        LocalDateTime thoughtReportGapTime = (LocalDateTime) runtimeService.getVariable(delegateExecution.getId(), "thoughtReportGapTime");
        Integer thoughtReportRequire = (Integer) runtimeService.getVariable(delegateExecution.getId(), "thoughtReportRequire");

        Map<String, Object> v = new HashMap<>();
//        v.put("thoughtReportGapTime", thoughtReportGapTime.plusMonths(gapMonth));
        v.put("thoughtReportGapTime", thoughtReportGapTime.plusMinutes(1));  //test: 1 min
        v.put("thoughtReportRequire", thoughtReportRequire+1);

        runtimeService.setVariables(delegateExecution.getId(), v);
    }
}
