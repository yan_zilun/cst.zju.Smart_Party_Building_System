package edu.zjucst.spb.pkg.flow.handler;


import edu.zjucst.spb.pkg.flow.common.ExistGroup;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;

/**
 * @author MrLi
 * @date 2022-12-30 15:45
 **/
public class SchoolActivityHandler implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        // 1. 从数据库中拿出当前设置的默认审批人

        // 2. 添加候选人and候选组
        delegateTask.addCandidateGroup(ExistGroup.SCHOOL);
    }
}
