package edu.zjucst.spb.pkg.flow.stage;

import edu.zjucst.spb.domain.entity.Stage;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Component("flowStageCheckComponent")
public class StageCheckComponent {

    private static final int AGE_THRESHOLD = 18;

    private static class STAGE_NUMBER {
        public static final Integer LEAGUE_MEMBER = 0;

    }

    public static boolean checkAdultAndLeagueMember(Stage stage) {
        LocalDate birthday = stage.getBirthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate currentDate = LocalDate.now();
        Period age = Period.between(birthday, currentDate);

        return age.getYears()>=AGE_THRESHOLD && stage.getDevelopmentPhaseId().equals(STAGE_NUMBER.LEAGUE_MEMBER);
    }

    public static boolean checkUploadThoughtReportTime(Stage stage) {
        LocalDate nxtTime = stage.getNxtThoughtReportUploadTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return nxtTime != null && nxtTime.isBefore(LocalDate.now());
    }
}
