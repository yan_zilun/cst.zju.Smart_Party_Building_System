package edu.zjucst.spb.pkg.flow.handler.joinParty;

import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.service.StageService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ZlunYan
 * @description
 * @create 2023-12-26
 */

@Component
public class StageToApplicantHandler implements ExecutionListener {

    private static final int deliveryGapMonths = 3;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private StageService stageService;


    @Override
    public void notify(DelegateExecution delegateExecution) {
        String taskId = delegateExecution.getId();
        String userId = (String) runtimeService.getVariable(taskId, "userId");
        Stage stage = stageService.getStageByUserId(userId);

        Map<String, Object> v = new HashMap<>();

        if (stage != null && stage.getDeliveryTime() != null) {
            LocalDateTime deliveryGapTime = stage.getDeliveryTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

//            v.put("deliveryGapTime", deliveryGapTime.plusMonths(deliveryGapMonths).toString());
            v.put("deliveryGapTime", deliveryGapTime.plusMinutes(1).toString());  // test: 1 min
            System.out.println("deliveryGapTime: " + deliveryGapTime.plusMinutes(1).toString());
        }

        runtimeService.setVariables(taskId, v);
    }
}
