package edu.zjucst.spb.pkg.flow.handler.joinParty;

import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.service.StageService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ZlunYan
 * @description
 * @create 2023-12-17
 */

@Component
public class StageToActivistHandler implements ExecutionListener {

    private static final int activistGapYears = 1;

    private static final int thoughtReportGapMonth = 3;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private StageService stageService;

    @Override
    public void notify(DelegateExecution delegateExecution) {
        String taskId = delegateExecution.getId();
        String userId = (String) runtimeService.getVariable(taskId, "userId");
        Stage stage = stageService.getStageByUserId(userId);

        Map<String, Object> v = new HashMap<>();
        v.put("thoughtReportRequire", 0);
        v.put("uploadThought", true);

        if (stage != null && stage.getActivistTime() != null) {
            LocalDateTime activistTime = stage.getActivistTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

//            v.put("activistGapTime", activistTime.plusYears(activistGapYears).toString());
            v.put("activistGapTime", activistTime.plusMinutes(1).toString());  // test: 1 min
            v.put("thoughtReportGapTime", activistTime.plusMonths(thoughtReportGapMonth).toString());
        }

        runtimeService.setVariables(taskId, v);
    }
}
