package edu.zjucst.spb.pkg.flow.handler.joinParty;

import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Component
public class UserTaskEndResultHandler implements TaskListener {

    @Autowired
    private TaskService taskService;


    @Override
    public void notify(DelegateTask delegateTask) {
        String taskId = delegateTask.getId();
        System.out.println("taskId = " + taskId);
        boolean accept = (boolean) taskService.getVariable(taskId, "accept");
        if (accept) {
            System.out.println(delegateTask.getName() + "通过");
        } else System.out.println(delegateTask.getName() + "失败");
    }
}
