package edu.zjucst.spb.enums;

public interface RoleConstant {
    String ANONYMOUS = "未分配";

    String ADMIN = "系统管理员";

    String UN_PARTY_MEMBER = "非党员学生";

    String PARTY_MEMBER = "党员学生";

    String BRANCH_SECRETARY = "老师支部书记";

    String PARTY_COMMITTEE = "学院党委";
}
