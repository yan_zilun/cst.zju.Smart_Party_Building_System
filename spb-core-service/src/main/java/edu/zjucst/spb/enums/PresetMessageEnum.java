package edu.zjucst.spb.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 系统预设的消息内容模板
 *
 * @author MrLi
 * @date 2023-05-26 21:41
 **/
@AllArgsConstructor
public enum PresetMessageEnum {
    // 关于发展对象政治审查工作
    APPLY_TALK_RECORD_SUBMIT(NoticeMessageSenderEnum.CST_BRANCH_GROUP_PARTY.getUserId(), 1L, "关于发展对象政治审查工作(谈话、查档、函调)的通知"),

    // repeat
    SUBMIT_APPLICATION_TO_JOIN_THE_PARTY(NoticeMessageSenderEnum.CST_BRANCH_GROUP.getUserId(), 2L,"《中国共产党入党志愿书》提交"),
    // more
    DEVELOP_PARTY_TRAINING(NoticeMessageSenderEnum.CST_BRANCH_GROUP.getUserId(), 3L,"发展对象党校培训"),

    DEVELOP_PARTY_TRAINING_SUBMIT(NoticeMessageSenderEnum.CST_BRANCH_GROUP_PARTY.getUserId(), 4L,"关于发展对象政治审查工作(《入党对象政治审查综合表》提交)的通知"),

    DEVELOP_PARTY_TRAINING_SUBMIT_REPLY(NoticeMessageSenderEnum.CST_BRANCH_GROUP_PARTY.getUserId(), 5L,"关于发展对象政治审查工作(审查、预审)的通知"),

    ;


    /**
     * 发送这条信息的对象id
     * {@see edu.zjucst.spb.enums.NoticeMessageSenderEnum}
     */
    @Getter
    private Long fromUserNumber;
    /**
     * 数据库对应的模板消息id
     */
    @Getter
    private Long messageId;
    /**
     * 对当条消息描述
     */
    @Getter
    private String desc;

}
