package edu.zjucst.spb.enums;

/**
 * 权限枚举，公开或私人
 * @author zhaoxinzhi
 */
public enum PrivilegeEnum {
    PUBLIC(0, "public"),
    PRIVATE(1, "private"),
    UNKNOWN(999, "unknown"),
    ;

    private final Integer code;
    private final String name;

    PrivilegeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static Integer getCodeByName(String name) {
        for(PrivilegeEnum privilegeEnum: PrivilegeEnum.values()){
            if( privilegeEnum.getName().equals(name) ) {
                return privilegeEnum.getCode();
            }
        }
        return UNKNOWN.getCode();
    }

    public static PrivilegeEnum getEnumByCode(int code) {
        for(PrivilegeEnum privilegeEnum: PrivilegeEnum.values()){
            if( privilegeEnum.getCode()==code) {
                return privilegeEnum;
            }
        }
        return UNKNOWN;
    }
}
