package edu.zjucst.spb.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */
@Getter
@AllArgsConstructor
public enum JoinPartyExceptionEnum {

    USER_ID_NOT_EXIST("1001", "用户Id不存在"),

    QUALIFICATIONS_FAIL("1002", "未年满18岁或非共青团员"),

    BRANCH_GROUP_NOT_EXIST("1003", "支部信息不存在"),

    TASK_NOT_EXIST("1004", "该审核任务不存在"),

    MULTIPLE_APPLICATION("1005", "重复申请入党"),

    FILE_NOT_EXIST("1006", "文件不存在"),

    TASK_LIST_NOT_EXIST("1007", "未选中需要处理的任务列表"),

    UPLOAD_THOUGHT_REPORT_FAIL("1008", "未到下一次上传思想汇报时间或无需上传思想汇报"),

    PARTY_GROUP_NOT_EXIST("1009", "党委信息不存在"),

    PARAM_ERROR("1010", "传入参数错误")
        ;

    final String code;

    final String description;
    final String prefix = "joinParty";
}
