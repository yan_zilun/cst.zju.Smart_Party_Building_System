package edu.zjucst.spb.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户业务状态枚举
 * @author qingo
 * @date 2022/12/22 10:34
 **/
@Getter
@AllArgsConstructor
public enum UserBusinessStatusEnum{
    VERIFICATION_EXPIRED("0001", "验证码过期，请重试"),

    USERNAME_OR_PWD_ERR("0002","账号或密码错误!"),
    PWD_NOT_SAME_ERR("0003","两次输入密码不一致!"),
    VERIFICATION_ERROR("0004","验证码错误"),
    USER_NOT_EXIST("2001", "登录用户不存在"),
    PASSWORD_ERROR("2002", "登录密码错误"),
    REDIS_CONNECTION_FAILURE("3001","Redis连接失败"),
    NETWORK_ERROR("3002","网络错误"),
    MESSAGE_NOT_EXIST("4001","消息不存在"),
    SEND_USER_NOT_EXIST("4002","发送消息用户不存在"),
    RECEIVE_USER_NOT_EXIST("4003","接受消息用户不存在");

    final String code;

    final String description;
    final String prefix = "user";
}
