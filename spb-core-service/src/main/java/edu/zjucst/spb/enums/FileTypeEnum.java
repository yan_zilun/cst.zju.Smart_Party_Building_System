package edu.zjucst.spb.enums;

import io.swagger.models.auth.In;

/**
 * 文件类型枚举
 * @author zhaoxinzhi
 */

public enum FileTypeEnum {
    WORD(0, "word"),
    PDF(1, "pdf"),
    IMAGE(2, "image"),
    EXCEL(3, "excel"),
    UNKNOWN(999, "unknown")
    ;

    private final Integer code;
    private final String name;

    FileTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static Integer getCodeByName(String name) {
        for(FileTypeEnum fileTypeEnum: FileTypeEnum.values()){
            if( fileTypeEnum.getName().equals(name) ) {
                return fileTypeEnum.getCode();
            }
        }
        return UNKNOWN.getCode();
    }
}
