package edu.zjucst.spb.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FlowBuildExceptionEnum {

//    VERIFICATION_EXPIRED("0001", "验证码错误或已过期，请重试"),

    USER_ID_NOT_EXIST("1001", "用户Id不存在"),

    USER_ID_EXIST("1002", "用户Id已经存在"),

    USER_NOT_IN_GROUP("1003", "用户不存在于该用户组中"),

    USER_IN_GROUP("1004", "用户已经存在于该用户组中"),

    GROUP_ID_NOT_EXIST("2001", "用户组Id不存在"),

    GROUP_NAME_NOT_EXIST("2002", "用户组名不存在"),

    GROUP_NAME_EXIST("2003", "用户组名已经存在"),

    GROUP_NOT_EMPTY("2004", "用户组不为空"),
    ;

    final String code;

    final String description;
    final String prefix = "flow_build";
}
