package edu.zjucst.spb.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author MrLi
 * @date 2023-05-26 19:51
 **/
@AllArgsConstructor
public enum NoticeMessageSenderEnum {

    CST_BRANCH_GROUP(10001L, "软件学院支委会"),

    CST_BRANCH_PARTY(10002L, "软件学院党委"),

    CST_BRANCH_GROUP_PARTY(10003L, "软件学院支委会、党委"),


    ;

    // 信息ID编号
    @Getter
    public Long userId;
    @Getter
    public String desc;


}
