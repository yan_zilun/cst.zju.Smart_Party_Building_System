package edu.zjucst.spb.enums;

/**
 * flow流程定义key的枚举
 * @author cy
 * @date 2023/5/22 15:08
 */
public enum FlowProcessEnum {

    AUTONOMOUS_ACTIVITIES("Autonomousactivities2", "autonomous activity"), // 自主活动流程
    UNKNOWN("UNKNOWN", "unknown"), // 非法
    ;
    private final String processKey;
    private final String processName;

    FlowProcessEnum(String processKey, String processName) {
        this.processKey = processKey;
        this.processName = processName;
    }

    public String getProcessKey() {
        return processKey;
    }

    public String getProcessName() {
        return processName;
    }

    public static String getProcessKeyByProcessName(String processName){
        for (FlowProcessEnum flowProcessEnum: FlowProcessEnum.values()){
            if(flowProcessEnum.getProcessName().equals(processName)){
                return flowProcessEnum.getProcessKey();
            }
        }
        return UNKNOWN.getProcessKey();
    }

}
