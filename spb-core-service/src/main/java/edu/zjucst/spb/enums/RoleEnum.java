package edu.zjucst.spb.enums;

/**
 * 用户权限角色枚举类
 * 与tb_role保持一致
 * @date 2023年4月20日
 */
public enum RoleEnum {
    ANONYMOUS(0, "未分配"),

    ADMIN(1, "系统管理员"),

    UN_PARTY_MEMBER(2, "非党员学生"),

    PARTY_MEMBER(3, "党员学生"),

    BRANCH_SECRETARY(4, "老师支部书记"),

    PARTY_COMMITTEE(5, "学院党委"),

    ;

    private final Integer code;
    private final String name;

    RoleEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
