package edu.zjucst.spb.util;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import edu.zjucst.spb.dao.message.UserMessageDao;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import edu.zjucst.spb.enums.PresetMessageEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * 定时通知处理工具
 *
 * @author MrLi
 * @date 2023-05-26 21:54
 **/
@Service
@Slf4j
public class TocTaskManager {

    @Autowired
    private UserMessageDao userMessageDao;

    /**
     * @param toUserNumber 接收者的学号
     * @param message      模板消息
     * @param param        动态参数
     */
    public void createTask(Long toUserNumber, PresetMessageEnum messageEnum, LocalDateTime date, Map<String, Object> param) {
        UserMessage msg = UserMessage.builder()
            .messageId(messageEnum.getMessageId())
            .sendUserId(messageEnum.getFromUserNumber())
            .needPush(true)
            .isRead(false)
            .needPushTime(date)
            .build();
        int result = userMessageDao.insert(msg);
        if (result > 0) {
            log.info("创建定时消息成功, {}", msg);
        } else {
            log.warn("创建定时消息失败, {}", msg);
        }
    }


    /**
     * 取消定时通知
     */
    public void cancelTask(Long toUserNumber, PresetMessageEnum messageEnum, Map<String, Object> param) {
        UserMessage msg = UserMessage.builder()
            .messageId(messageEnum.getMessageId())
            .sendUserId(messageEnum.getFromUserNumber())
            .build();
        int result = userMessageDao.delete(new LambdaQueryWrapper<>(msg));
        if (result > 0) {
            log.info("取消定时消息成功, {}", msg);
        } else {
            log.warn("取消定时消息失败, {}", msg);
        }
    }
}
