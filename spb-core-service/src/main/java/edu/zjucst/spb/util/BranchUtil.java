package edu.zjucst.spb.util;


import edu.zjucst.spb.domain.entity.SpbUser;

public class BranchUtil {


    //根据用户检查是否属于某个branch
    public static boolean checkBranch(SpbUser user, String partyBranch){
        return user.getPartyBranch().equals(partyBranch);
    }

}
