package edu.zjucst.spb.util.converMapper;

import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.domain.vo.stage.*;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StageConvertMapper {

    StageConvertMapper INSTANCE = Mappers.getMapper(StageConvertMapper.class);

    // zlun add
//    @Mapping(source = "userName", target = "userName")
    NewStageVO stageConvertNewStageVO(Stage stage);

    List<NewStageVO> stageConvertNewStageVO(List<Stage> stages);

    Stage stageInsertVOConvertstage(StageInsertVO stageInsertVO);
    // zlun add end

    ApplicantStageVO stageConvertApplicantStageVO(Stage stage);

    List<ApplicantStageVO> stageConvertApplicantStageVO(List<Stage> stages);

    ActivistStageVO stageConvertActivistStageVO(Stage stage);

    List<ActivistStageVO> stageConvertActivistStageVO(List<Stage> stages);

    DevelopStageVO stageConvertDevelopStageVO(Stage stage);

    List<DevelopStageVO> stageConvertDevelopStageVO(List<Stage> stages);

    PreparePMStageVO stageConvertPreparePMStageVO(Stage stage);

    List<PreparePMStageVO> stageConvertPreparePMStageVO(List<Stage> stages);

    StageStateVO stageConvertStageStateVO(Stage stage);
}
