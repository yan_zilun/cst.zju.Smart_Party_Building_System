package edu.zjucst.spb.util.converMapper;

import edu.zjucst.spb.domain.entity.stage.ApplicantStage;
import edu.zjucst.spb.domain.param.stage.v2.ApplicantStageUpdateParam;
import edu.zjucst.spb.domain.vo.stage.v2.ApplicantStageVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StageConvertMapperV2 {
    StageConvertMapperV2 INSTANCE = Mappers.getMapper(StageConvertMapperV2.class);

    ApplicantStageVO stageConvertApplicantStageVO(ApplicantStage stage);

    List<ApplicantStageVO> stageConvertApplicantStageVO(List<ApplicantStage> stages);

    ApplicantStage updateApplicantParamConvertStage(ApplicantStageUpdateParam param);
}
