package edu.zjucst.spb.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static String DATE_FORMAT = "yyyy-MM-dd";
    public static Date transferToDate(String dateString) throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        sdf.setLenient(false); // 设置为非宽容模式
        return sdf.parse(dateString);
    }

}
