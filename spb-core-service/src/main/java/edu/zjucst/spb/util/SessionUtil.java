package edu.zjucst.spb.util;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import java.util.Optional;

/**
 * 拿取subject进行session操作
 *
 * @author MrLi
 * @date 2023-04-05 21:34
 **/
public class SessionUtil {
    static Subject subject = SecurityUtils.getSubject();

    static Session session = subject.getSession();

    public static Long getUserId() {
        return Optional.ofNullable(session.getAttribute("userId")).map(value -> (Long) value).orElse(null);
    }


    public static void setUserId(Long userId) {
        session.setAttribute("userId", userId);
    }


    public static Long getBranchId() {
        return Optional.ofNullable(session.getAttribute("branchId")).map(value -> (Long) value).orElse(null);
    }


    public static void setBranchId(Long branchId) {
        session.setAttribute("branchId", branchId);
    }


    public static Long getRoleId() {
        return Optional.ofNullable(session.getAttribute("roleId")).map(value -> (Long) value).orElse(null);
    }


    public static void setRoleId(Long roleId) {
        session.setAttribute("roleId", roleId);
    }
}
