package edu.zjucst.spb.util.converMapper;


import edu.zjucst.spb.domain.entity.flow.FlowGroup;
import edu.zjucst.spb.domain.entity.flow.joinParty.MyProcessTask;
import org.flowable.idm.api.Group;
import org.flowable.task.api.Task;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface JoinPartyConvertMapper {

    JoinPartyConvertMapper INSTANCE = Mappers.getMapper(JoinPartyConvertMapper.class);

    MyProcessTask taskConvertMyProcessTask(Task task);

    List<MyProcessTask> taskConvertMyProcessTask(List<Task> tasks);
}
