package edu.zjucst.spb.util.converMapper;

import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.vo.PersonalInformationVO;
import edu.zjucst.spb.domain.vo.SelectUserVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserConvertMapper {
    UserConvertMapper INSTANCE = Mappers.getMapper(UserConvertMapper.class);
    @Mapping(source = "username" , target = "userName")
    SelectUserVO spbUserConvertSelectUserVO(SpbUser spbUser);

    List<SelectUserVO> spbUserConvertSelectUserVO(List<SpbUser> spbUser);

    @Mapping(source = "username" , target = "userName")
    PersonalInformationVO spbUserConvertPersonalInformationVO(SpbUser spbUser);

}
