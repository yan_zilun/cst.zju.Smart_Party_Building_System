package edu.zjucst.spb.util.converMapper;


import edu.zjucst.spb.domain.entity.flow.FlowGroup;
import edu.zjucst.spb.domain.entity.flow.FlowUser;
import org.flowable.idm.api.User;
import org.mapstruct.Mapper;
import org.flowable.idm.api.Group;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface FlowBuildConvertMapper {
    FlowBuildConvertMapper INSTANCE = Mappers.getMapper(FlowBuildConvertMapper.class);

    @Mapping(source = "id", target = "groupId")
    @Mapping(source = "name", target = "groupName")
    FlowGroup groupConvertFlowGroup(Group group);

    List<FlowGroup> groupConvertFlowGroup(List<Group> groups);

    @Mapping(source = "id", target = "userId")
    @Mapping(source = "displayName", target = "username")
    FlowUser userConvertFlowUser(User user);

    List<FlowUser> userConvertFlowUser(List<User> users);
}
