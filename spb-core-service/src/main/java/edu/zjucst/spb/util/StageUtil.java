//package edu.zjucst.spb.util;
//
//import edu.zjucst.spb.domain.entity.SpbUser;
//import edu.zjucst.spb.domain.param.stage.ActivistStageParam;
//import edu.zjucst.spb.domain.param.stage.ApplicantStageParam;
//import edu.zjucst.spb.domain.param.stage.DevelopStageParam;
//import edu.zjucst.spb.domain.param.stage.PreparePMStageParam;
//import edu.zjucst.spb.domain.entity.Stage;
//import edu.zjucst.spb.domain.vo.UserVo;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import static edu.zjucst.spb.constant.StageConstant.*;
//
//public class StageUtil {
//
//    //检查入党申请书递交时间和当前时间差是否大于等于给定间隔
//    public static boolean checkInterval(Stage stage,String Interval){
//        Calendar c1 = Calendar.getInstance();
//        Calendar c2 = Calendar.getInstance();
//
//        c1.setTime(new Date());
//        c2.setTime(stage.getDeliveryTime());
//
//        int y = c1.get(Calendar.YEAR)-c2.get(Calendar.YEAR);
//        int month = 0;
//        if (y<0)
//        {
//            month = -y*12;
//        }else if(y>0)
//        {
//            month =  y*12;
//        }
//        int result = (c1.get(Calendar.MONDAY) - c2.get(Calendar.MONTH)) + month;
//        result = (c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH))>0 ? result : result-1;
//        String str = Interval;
//        String str2 = "";
//        if(str != null && !"".equals(str)){
//            for(int i = 0; i < str.length(); i++){
//                if(str.charAt(i) >= 48 && str.charAt(i) <= 57){
//                    str2 += str.charAt(i);
//                }
//            }
//        }
//        return result>=Integer.parseInt(str2);
//    }
//
//    public static boolean checkAdult(SpbUser user) {
//        if(user==null){
//            return false;
//        }
//        Date date = user.getBirthday();
//        if(date==null){
//            return false;
//        }
//        Calendar current = Calendar.getInstance();
//        Calendar birthDay = Calendar.getInstance();
//        birthDay.setTime(date);
//        Integer year = current.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
//        if (year > 18) {
//            return true;
//        } else if (year < 18) {
//            return false;
//        }
//        // 如果年相等，就比较月份
//        Integer month = current.get(Calendar.MONTH) - birthDay.get(Calendar.MONTH);
//        if (month > 0) {
//            return true;
//        } else if (month < 0) {
//            return false;
//        }
//        // 如果月也相等，就比较天
//        Integer day = current.get(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH);
//        return  day >= 0;
//    }
//
//    public static List<Stage> applicantStageParamtoStageList(ApplicantStageParam applicantStageParam){
////        List<UserVo> userList = applicantStageParam.getUserList();
////        List<Stage> stageList = new ArrayList<>();
////        for (UserVo user:userList
////        ) {
////            Stage stage = new Stage();
////            stage.setUserName(user.getUsername());
////            stage.setUserNumber(user.getUserNumber());
////            stage.setDevelopmentPhase(applicantStageParam.getDevelopmentPhase());
////            //团员这块不用设置，后端查询
////            //stage.setIsLeague(applicantStageParam.getIsLeague());
////            stage.setDeliveryTime(applicantStageParam.getDeliveryTime());
////            stage.setTalkRegistrationTime(applicantStageParam.getTalkRegistrationTime());
////            stage.setTalker(applicantStageParam.getTalker());
////            stage.setQualificationInterval(applicantStageParam.getQualificationInterval());
////            stage.setIsPromote(applicantStageParam.getIsPromote());
////            stage.setState(applicantStageParam.getState());
////            stageList.add(stage);
////        }
////        return stageList;
//    }
//
//    public static List<Stage> activistStageParamtoStageList(ActivistStageParam activistStageParam){
//        List<UserVo> userList = activistStageParam.getUserList();
//        List<Stage> stageList = new ArrayList<>();
//        for (UserVo user:userList
//        ) {
//            Stage stage = new Stage();
//            stage.setUserName(user.getUsername());
//            stage.setUserNumber(user.getUserNumber());
//            stage.setDevelopmentPhase(activistStageParam.getDevelopmentPhase());
//            stage.setPromoteTime(activistStageParam.getPromoteTime());
//            stage.setActivistTime(activistStageParam.getActivistTime());
//            stage.setThoughtReport(activistStageParam.getThoughtReport());
//            stage.setEducationalVisit(activistStageParam.getEducationalVisit());
//            //党校培训自动接入
//            stage.setCultivateContacts(activistStageParam.getCultivateContacts());
//            stage.setDevelopmentPublicTime(activistStageParam.getDevelopmentPublicTime());
//            stage.setTeacherTime(activistStageParam.getTeacherTime());
//            stage.setState(activistStageParam.getState());
//            stageList.add(stage);
//        }
//        return stageList;
//    }
//
//    public static List<Stage> developStageParamtoStageList(DevelopStageParam developStageParam){
//        List<UserVo> userList = developStageParam.getUserList();
//        List<Stage> stageList = new ArrayList<>();
//        for (UserVo user:userList
//        ) {
//            Stage stage = new Stage();
//            stage.setUserName(user.getUsername());
//            stage.setUserNumber(user.getUserNumber());
//            stage.setDevelopmentPhase(developStageParam.getDevelopmentPhase());
//            stage.setConfirmTime(developStageParam.getConfirmTime());
//            stage.setRecordTime(developStageParam.getRecordTime());
//            stage.setPartySponsor(developStageParam.getPartySponsor());
//            stage.setPoliticalReviewTime(developStageParam.getPoliticalReviewTime());
//            //党校自动接入
//            stage.setInvestigateTime(developStageParam.getInvestigateTime());
//            stage.setPreliminaryInvestigateTime(developStageParam.getPreliminaryInvestigateTime());
//            stage.setPartyApplicationTime(developStageParam.getPartyApplicationTime());
//            stage.setRecommendable(developStageParam.getRecommendable());
//            stage.setState(developStageParam.getState());
//            stageList.add(stage);
//        }
//        return stageList;
//    }
//
//    public static List<Stage> preparePMStageParamtoStageList(PreparePMStageParam preparePMStageParam){
//        List<UserVo> userList = preparePMStageParam.getUserList();
//        List<Stage> stageList = new ArrayList<>();
//        for (UserVo user:userList
//        ) {
//            Stage stage = new Stage();
//            stage.setUserName(user.getUsername());
//            stage.setUserNumber(user.getUserNumber());
//            stage.setDevelopmentPhase(preparePMStageParam.getDevelopmentPhase());
//            stage.setBranchPrepareTime(preparePMStageParam.getBranchPrepareTime());
//            stage.setCommitteeTalk(preparePMStageParam.getCommitteeTalk());
//            stage.setExamineTime(preparePMStageParam.getExamineTime());
//            stage.setPrepareThoughtReport(preparePMStageParam.getPrepareThoughtReport());
//            stage.setPreparePartyTraining(preparePMStageParam.getPreparePartyTraining());
//            //“入党介绍人”已经确定，可以查看，不需要重新设置。
//            //stage.setPartySponsor(preparePMStageParam.getPartySponsor());
//            stage.setApplyFullTime(preparePMStageParam.getApplyFullTime());
//            stage.setProbationaryPublicTime(preparePMStageParam.getProbationaryPublicTime());
//            stage.setBranchFullTime(preparePMStageParam.getBranchFullTime());
//            stage.setCommitteeFullTime(preparePMStageParam.getCommitteeFullTime());
//            stage.setState(preparePMStageParam.getState());
//            stageList.add(stage);
//        }
//        return stageList;
//    }
//    //更新可以直接设置的字段 资格审查间隔,是否可以推优，团员身份，思想汇报，教育考察登记表，
//    public static Stage simpleEditableStage(Stage stage,String roleName) {
//        Stage resStage = new Stage();
//        resStage.setUserNumber(stage.getUserNumber());
//        resStage.setId(stage.getId());
//        if(roleName.equals("学院党委")||roleName.equals("学院团委")){
//            resStage.setQualificationInterval(stage.getQualificationInterval());
//        }
//        resStage.setIsLeague(stage.getIsLeague());
//        resStage.setIsPromote(stage.getIsPromote());
//        if(roleName.equals("支部书记")){
//            resStage.setThoughtReport(stage.getThoughtReport());
//            resStage.setPrepareThoughtReport(stage.getPrepareThoughtReport());
//            resStage.setEducationalVisit(stage.getEducationalVisit());
//        }
//        return resStage;
//    }
//    //根据状态更新部分字段
//    public static Stage isFieldEditableForPhaseStatus(Stage stage,String roleName) {
//        Stage resStage = new Stage();
//        resStage.setUserNumber(stage.getUserNumber());
//        resStage.setId(stage.getId());
//        boolean role1 = roleName.equals("支部书记");
//        boolean role2 = roleName.equals("学院党委");
//        boolean role3 = (roleName.equals("学院党委") || roleName.equals("学院团委"));
//        switch (stage.getState()) {
//            case "提交入党申请书":{
//                if(stage.getDeliveryTime()!=null && role3){
//                    resStage.setDeliveryTime(stage.getDeliveryTime());
//                    resStage.setState("确定谈话人");
//                }
//                break;
//            }
//            case "确定谈话人":{
//                if(stage.getTalker()!=null && role1){
//                    resStage.setTalker(stage.getTalker());
//                    resStage.setState("申请人谈话");
//                }
//                break;
//            }
//            case "申请人谈话":{
//                if(stage.getTalkRegistrationTime()!=null && role1){
//                    resStage.setTalkRegistrationTime(stage.getTalkRegistrationTime());
//                    resStage.setState("变更身份为积极分子");
//                }
//                break;
//            }
//            case "变更身份为积极分子":{
//                if(stage.getDevelopmentPhase().equals(ACTIVIST) && role3){
//                    resStage.setDevelopmentPhase(ACTIVIST);
//                    resStage.setState("成为积极分子");
//                }
//                break;
//            }
//            case "成为积极分子":{
//                if(stage.getPromoteTime()!=null && role2){
//                    resStage.setPromoteTime(stage.getPromoteTime());
//                    resStage.setState("积极分子确定时间");
//                }
//                break;
//            }
//            case "积极分子确定时间":{
//                if(stage.getActivistTime()!=null && role1){
//                    resStage.setActivistTime(stage.getActivistTime());
//                    resStage.setState("确定培养联系人");
//                }
//                break;
//            }
//            case "确定培养联系人":{
//                if(stage.getCultivateContacts()!=null && role1){
//                    resStage.setCultivateContacts(stage.getCultivateContacts());
//                    resStage.setState("参加积极分子阶段党校培训班");
//                }
//                break;
//            }
//            case "参加积极分子阶段党校培训班":{
//                if(stage.getActivistPartyTraining()!=null && role2){
//                    resStage.setActivistPartyTraining(stage.getActivistPartyTraining());
//                    resStage.setState("提交群众意见调查表");
//                }
//                break;
//            }
//            case "提交群众意见调查表":{
//                if(stage.getDevelopmentPublicTime()!=null && role2){
//                    resStage.setDevelopmentPublicTime(stage.getDevelopmentPublicTime());
//                    resStage.setState("提交班主任导师意见征求表");
//                }
//                break;
//            }
//            case "提交班主任导师意见征求表":{
//                if(stage.getTeacherTime()!=null && role2){
//                    resStage.setTeacherTime(stage.getTeacherTime());
//                    resStage.setState("变更身份为发展对象");
//                }
//                break;
//            }
//            case "变更身份为发展对象":{
//                if(stage.getDevelopmentPhase().equals(DEVELOP) && role2){
//                    resStage.setDevelopmentPhase(DEVELOP);
//                    resStage.setState("成为发展对象");
//                }
//                break;
//            }
//            case "成为发展对象":{
//                if(stage.getConfirmTime()!=null && role2){
//                    resStage.setConfirmTime(stage.getConfirmTime());
//                    resStage.setState("发展对象备案时间");
//                }
//                break;
//            }
//            case "发展对象备案时间":{
//                if(stage.getRecordTime()!=null && role1){
//                    resStage.setRecordTime(stage.getRecordTime());
//                    resStage.setState("确定入党介绍人");
//                }
//                break;
//            }
//            case "确定入党介绍人":{
//                if(stage.getPartySponsor()!=null && role1){
//                    resStage.setPartySponsor(stage.getPartySponsor());
//                    resStage.setState("提交入党对象政治审查综合表");
//                }
//                break;
//            }
//            case "提交入党对象政治审查综合表":{
//                if(stage.getPoliticalReviewTime()!=null && role2){
//                    resStage.setPoliticalReviewTime(stage.getPoliticalReviewTime());
//                    resStage.setState("参加发展对象阶段党校培训班");
//                }
//                break;
//            }
//            case "参加发展对象阶段党校培训班":{
//                if(stage.getDevelopPartyTraining()!=null && role2){
//                    resStage.setDevelopPartyTraining(stage.getDevelopPartyTraining());
//                    resStage.setState("资格审查");
//                }
//                break;
//            }
//            case "资格审查":{
//                if(stage.getInvestigateTime()!=null && role2){
//                    resStage.setInvestigateTime(stage.getInvestigateTime());
//                    resStage.setState("资格预审");
//                }
//                break;
//            }
//            case "资格预审":{
//                if(stage.getPreliminaryInvestigateTime()!=null && role1){
//                    resStage.setPreliminaryInvestigateTime(stage.getPreliminaryInvestigateTime());
//                    resStage.setState("提交入党志愿书");
//                }
//                break;
//            }
//            case "提交入党志愿书":{
//                if(stage.getPartyApplicationTime()!=null && role2){
//                    resStage.setPartyApplicationTime(stage.getPartyApplicationTime());
//                    resStage.setState("变更身份为预备党员");
//                }
//                break;
//            }
//            case "变更身份为预备党员":{
//                if(stage.getDevelopmentPhase().equals(PREPARE) && role2){
//                    resStage.setDevelopmentPhase(PREPARE);
//                    resStage.setState("接受预备党员");
//                }
//                break;
//            }
//            case "接受预备党员":{
//                if(stage.getBranchPrepareTime()!=null && role2){
//                    resStage.setBranchPrepareTime(stage.getBranchPrepareTime());
//                    resStage.setState("上级党委派人谈话");
//                }
//                break;
//            }
//            case "上级党委派人谈话":{
//                if(stage.getCommitteeTalk()!=null && role2){
//                    resStage.setCommitteeTalk(stage.getCommitteeTalk());
//                    resStage.setState("党委审批通过");
//                }
//                break;
//            }
//            case "党委审批通过":{
//                if(stage.getExamineTime()!=null && role2){
//                    resStage.setExamineTime(stage.getExamineTime());
//                    resStage.setState("参加预备党员阶段党校培训班");
//                }
//                break;
//            }
//            case "参加预备党员阶段党校培训班":{
//                if(stage.getPreparePartyTraining()!=null && role2){
//                    resStage.setPreparePartyTraining(stage.getPreparePartyTraining());
//                    resStage.setState("提出转正");
//                }
//                break;
//            }
//            case "提出转正":{
//                if(stage.getApplyFullTime()!=null && role2){
//                    resStage.setApplyFullTime(stage.getApplyFullTime());
//                    resStage.setState("征求群众意见");
//                }
//                break;
//            }
//            case "征求群众意见":{
//                if(stage.getProbationaryPublicTime()!=null && role2){
//                    resStage.setProbationaryPublicTime(stage.getProbationaryPublicTime());
//                    resStage.setState("预备党员转正");
//                }
//                break;
//            }
//            case "预备党员转正":{
//                if(stage.getBranchFullTime()!=null && role2){
//                    resStage.setBranchFullTime(stage.getBranchFullTime());
//                    resStage.setState("党委审批预备党员转正");
//                }
//                break;
//            }
//            case "党委审批预备党员转正":{
//                if(stage.getCommitteeFullTime()!=null && role2){
//                    resStage.setCommitteeFullTime(stage.getCommitteeFullTime());
//                    resStage.setState("变更身份为党员");
//                }
//                break;
//            }
//            case "变更身份为党员":{
//                if(stage.getDevelopmentPhase().equals(OFFICIAL) && role2){
//                    resStage.setDevelopmentPhase(OFFICIAL);
//                    resStage.setState("END");
//                }
//                break;
//            }
//            default:
//                return null;
//        }
//        return resStage;
//    }
//}
