//package edu.zjucst.spb.exception;
//
//import edu.zjucst.spb.enums.UserBusinessStatusEnum;
//
///**
// * 用户不存在异常
// *
// * @author qingo
// * @date 2022/12/22 11:49
// */
//public class NoAccountFoundException extends SpbUserAccountException {
//    public NoAccountFoundException() {
//        super();
//        retCode = UserBusinessStatusEnum.USER_NOT_EXIST.getCode();
//        retMessage = UserBusinessStatusEnum.USER_NOT_EXIST.getDescription();
//    }
//
//    public NoAccountFoundException(String retCode, String retMessage) {
//        super(retCode, retMessage);
//    }
//
//    public String getRetCode() {
//        return retCode;
//    }
//
//    public String getRetMessage() {
//        return retMessage;
//    }
//
//
//}
