package edu.zjucst.spb.exception;

import edu.zjucst.spb.enums.ResponseStatusEnum;

/**
 * 未登录异常
 *
 * @author MrLi
 * @date 2022-12-03 21:53
 **/
public class NotLoginException extends SpbUserAccountException {

    public NotLoginException() {
        super();
        retCode = ResponseStatusEnum.AUTHENTICATION_ERROR.getCode();
        retMessage = ResponseStatusEnum.AUTHENTICATION_ERROR.getDescription();
    }

    public NotLoginException(String retCode, String retMessage) {
        super(retCode, retMessage);
    }

    public String getRetCode() {
        return retCode;
    }

    public String getRetMessage() {
        return retMessage;
    }
}
