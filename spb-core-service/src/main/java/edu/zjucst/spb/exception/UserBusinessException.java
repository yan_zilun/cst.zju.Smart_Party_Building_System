package edu.zjucst.spb.exception;

import edu.zjucst.spb.enums.UserBusinessStatusEnum;

/**
 * 实现xxxx功能
 *
 * @author qingo
 * @date 2022/12/22 11:49
 */


public class UserBusinessException extends BizException {


    public UserBusinessException() {
        super();
    }

    public UserBusinessException(UserBusinessStatusEnum statusEnum) {
        super(statusEnum.getPrefix() + ":" + statusEnum.getCode(), statusEnum.getDescription());
    }


}
