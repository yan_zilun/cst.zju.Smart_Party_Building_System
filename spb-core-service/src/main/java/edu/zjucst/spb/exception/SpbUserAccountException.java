package edu.zjucst.spb.exception;

/**
 * 用户相关异常
 *
 * @author MrLi
 * @date 2022-12-03 21:53
 **/
public class SpbUserAccountException extends BizException {

    public SpbUserAccountException() {
        super();
    }

    public SpbUserAccountException(String retCode, String retMessage) {
        this.retCode = retCode;
        this.retMessage = retMessage;
    }

    public String getRetCode() {
        return retCode;
    }

    public String getRetMessage() {
        return retMessage;
    }
}
