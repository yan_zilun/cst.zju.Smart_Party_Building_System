package edu.zjucst.spb.domain.param.stage.joinParty;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2023-12-15
 */

@Data
public class JoinPartyTaskListParam {

//    @ApiModelProperty("上报表单对应的taskId列表")
//    private List<String> taskIdList;

    @ApiModelProperty("taskId")
    private String taskId;

    @ApiModelProperty("(选用)用于一些审批的通过或否决,默认为false")
    private boolean accept;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("(选用)时间(可用于申请书提交时间、推优时间等时间信息)")
    private Date time;

    @ApiModelProperty("(选用)人名列表(可用于培养人、谈话人等)[1-2个元素]")
    private String name1;

    @ApiModelProperty("(选用)人名列表2(可用于培养人、谈话人等)[1-2个元素]")
    private String name2;

//    @ApiModelProperty("(选用)上传文件的id(可用于入党申请书、思想汇报等)")
//    private Integer fileId;
}
