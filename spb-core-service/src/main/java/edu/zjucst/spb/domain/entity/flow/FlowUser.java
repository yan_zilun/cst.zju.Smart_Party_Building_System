package edu.zjucst.spb.domain.entity.flow;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-29
 */

@Data
public class FlowUser {

    @ApiModelProperty("用户id")
    String userId;

    @ApiModelProperty("姓名")
    String username;

    @ApiModelProperty("用户组名")
    String groupName;
}
