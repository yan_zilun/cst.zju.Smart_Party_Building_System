package edu.zjucst.spb.domain.param.flow;

import com.baomidou.mybatisplus.annotation.TableField;
import edu.zjucst.spb.domain.PageQry;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Data
public class FlowGroupMemberQueryParam extends PageQry implements Serializable {

    @TableField(exist = false)
    protected static final long serialVersionUID = 1L;

    @ApiModelProperty("用户组名")
    private String groupName;
}
