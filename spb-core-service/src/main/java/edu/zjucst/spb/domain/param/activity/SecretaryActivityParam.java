package edu.zjucst.spb.domain.param.activity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @TableName spb_activity
 */
@Data
public class SecretaryActivityParam extends ActivityParam implements Serializable {
    @ApiModelProperty("参与人员")
    private List<String> participants;
}
