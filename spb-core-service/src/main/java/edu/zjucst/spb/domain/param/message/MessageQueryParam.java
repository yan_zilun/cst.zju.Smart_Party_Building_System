package edu.zjucst.spb.domain.param.message;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.PageQry;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author MrLi
 * @date 2023-04-08 14:12
 **/
@Data
public class MessageQueryParam extends PageQry {
    @ApiModelProperty(value = "是否已读")
    Integer isRead;

}

