package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 自主学习类
 * @author 江中华
 * @since 2024-03-11
 */
@Data
@TableName("tb_self_study")
@ApiModel(value = "SelfStudy对象", description = "")
public class SelfStudy implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("活动ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("活动名称")
    @TableField("activity_name")
    private String activityName;

    @ApiModelProperty("学工号")
    @TableField("user_number")
    private String userNumber;

    @ApiModelProperty("申请时间")
    @TableField("applied_time")
    private Date appliedDate;

    @ApiModelProperty("申请学时")
    @TableField("applied_study_hour")
    private Integer appliedStudyHour;

    @ApiModelProperty("发展阶段id")
    @TableField("development_phase_id")
    private Integer developmentPhaseId;
//
//    @ApiModelProperty("附加文件链接或者文件名")
//    @TableField("addition_file")
//    private String additionFile;
//
//    @ApiModelProperty("审核状态")
//    @TableField("audit_status")
//    private String auditStatus;
//
//    @ApiModelProperty("审核时间")
//    @TableField("audit_time")
//    private LocalDateTime auditTime;
//
//    @ApiModelProperty("创建时间")
////    @TableField(fill = FieldFill.INSERT)
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime createTime;
//
//    @ApiModelProperty("更新时间")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
////    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private LocalDateTime updateTime;

}
