package edu.zjucst.spb.domain;
import io.swagger.annotations.ApiModelProperty;

/**
 * 分页对象, 需要分页的内容DO可以继承该类==>mapper.xml记得加上pageStart和pageSize参数
 *
 * @author MrLi
 * @date 2023-05-19 19:26
 **/
public class PageQry {

    /**
     * 每页个数
     */
    @ApiModelProperty(value = "每页元素个数(默认10)")
    private Integer pageSize = 10;


    /**
     * 页码数
     */
    @ApiModelProperty(value = "页码数(默认1)")
    private Integer pageNumber = 1;

    /**
     * 生成的起始元素index
     */
    @ApiModelProperty(value = "生成的起始元素index（不用填）")
    private Integer pageStart ;


    public Integer getPageStart(){
        if (pageStart == null) {
            return (pageNumber - 1) * pageSize;
        }
        return pageStart;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }
}
