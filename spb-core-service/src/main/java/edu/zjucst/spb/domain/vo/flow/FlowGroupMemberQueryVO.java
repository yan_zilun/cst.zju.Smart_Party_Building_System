package edu.zjucst.spb.domain.vo.flow;

import edu.zjucst.spb.domain.entity.flow.FlowUser;
import edu.zjucst.spb.domain.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Data
public class FlowGroupMemberQueryVO extends PageVO {

    @ApiModelProperty("查询列表")
    List<FlowUser> flowUserList;
}
