package edu.zjucst.spb.domain.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * (SpbUser)表实体类
 *
 * @author Mrli
 * @since 2023-04-05 23:21:25
 */
@Data
@TableName("tb_spb_user")
@ApiModel(value = "spb_user", description = "spb业务系统-用户模型")
public class SpbUser {
    @ApiModelProperty(value = "主键Id")
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "学号")
    private String userNumber;

    @ApiModelProperty(value = "权限，学生/支部书记/学院党委/系统管理员")
    private String roleName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "注册时间")
    private Date registerTime;

    @ApiModelProperty(value = "性别, 默认为, 0未知1男2女")
    private Integer sex;

    @ApiModelProperty(value = "生日")
    private Date birthday;

    @ApiModelProperty(value = "民族")
    private String nationality;

    @ApiModelProperty(value = "政治面貌")
    private String politicsStatus;

    @ApiModelProperty(value = "家庭住址")
    private String address;

    @ApiModelProperty(value = "身份证号")
    private String identityId;

    @ApiModelProperty(value = "学历")
    private String qualification;

    @ApiModelProperty(value = "在读状态")
    private String readingStatus;

    @ApiModelProperty(value = "团员编号")
    private String leagueNum;

    @ApiModelProperty(value = "入团时间")
    private Date leagueJoinTime;

    @ApiModelProperty(value = "是否提交入党申请书")
    private Integer ifApply;

    @ApiModelProperty(value = "入党状态，如入党申请阶段/积极分子阶段/发展对象阶段/预备党员阶段")
    private String developmentPhase;

    @ApiModelProperty(value = "入党时间")
    private Date partyJoinTime;

    @ApiModelProperty(value = "所在党支部")
    private String partyBranch;

    @ApiModelProperty(value = "电话号码")
    private String phone;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", username=").append(username);
        sb.append(", pwd=").append(pwd);
        sb.append(", userNumber=").append(userNumber);
        sb.append(", roleName=").append(roleName);
        sb.append(", registerTime=").append(registerTime);
        sb.append(", sex=").append(sex);
        sb.append(", birthday=").append(birthday);
        sb.append(", nationality=").append(nationality);
        sb.append(", politicsStatus=").append(politicsStatus);
        sb.append(", address=").append(address);
        sb.append(", identityId=").append(identityId);
        sb.append(", qualification=").append(qualification);
        sb.append(", readingStatus=").append(readingStatus);
        sb.append(", leagueNum=").append(leagueNum);
        sb.append(", leagueJoinTime=").append(leagueJoinTime);
        sb.append(", ifApply=").append(ifApply);
        sb.append(", developmentPhase=").append(developmentPhase);
        sb.append(", partyJoinTime=").append(partyJoinTime);
        sb.append(", partyBranch=").append(partyBranch);
        sb.append(", phone=").append(phone);
        sb.append(", email=").append(email);
        sb.append("]");
        return sb.toString();
    }
}
