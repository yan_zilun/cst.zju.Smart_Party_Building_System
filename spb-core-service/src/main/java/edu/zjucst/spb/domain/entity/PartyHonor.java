package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 18:18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PartyHonor {
    @TableId(type = IdType.AUTO)
    Integer id;
    Long userId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    Date honorTime;
    String honorName;
    String grantUnit;
    String remark;
}
