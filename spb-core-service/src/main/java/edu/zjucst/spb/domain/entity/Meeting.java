package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

//
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_meeting")
@ApiModel(value = "meeting对象", description = "")
public class Meeting implements Serializable {

    @TableId(type = IdType.AUTO)  // 从一开始自增  不加就随机
    private Long id;

    @ApiModelProperty(value = "会议类别")
    private String claasification;

    @ApiModelProperty(value = "会议议题")
    private String topic;

    @ApiModelProperty(value = "参会人数")
    private Long member_count;

    @ApiModelProperty(value = "所属组织")
    private String organization;

    @ApiModelProperty(value = "会议地点")
    private String location;

    @ApiModelProperty(value = "会议开始日期")
    private Date begin_date;

    @ApiModelProperty(value = "会议结束日期")
    private Date end_date;

    @ApiModelProperty(value = "课时")
    private Long lesson_time;

    @ApiModelProperty(value = "费用")
    private Long fee;

    @ApiModelProperty(value = "授课人")
    private String lecturer;

    @ApiModelProperty(value = "主持人")
    private String host;

    @ApiModelProperty(value = "远程会议")
    private String remote;

    @ApiModelProperty(value = "是否有积极分子")
    private Boolean has_activity;

    @ApiModelProperty(value = "是否有发展对象")
    private Boolean has_development;
}
