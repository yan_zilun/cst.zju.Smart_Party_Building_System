package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件 实体类
 *
 * @author zxz
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_file")
@ApiModel(value = "文件对象", description = "")
public class TbFile {
    @ApiModelProperty("文件id")
    @TableId(type = IdType.AUTO)
    Integer id;
    @ApiModelProperty("文件名")
    String  name;
    @ApiModelProperty("文件类型")
    Integer type;
    @ApiModelProperty("文件存储路径")
    String  path;
    @ApiModelProperty("上传文件的用户id")
    Long    creatorId;
    @ApiModelProperty("文件是公有还是私有")
    Integer privilege;

}
