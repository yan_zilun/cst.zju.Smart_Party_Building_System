package edu.zjucst.spb.domain.entity.message;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * (UserMessage)表实体类
 *
 * @author Mrli
 * @since 2023-04-05 15:14:48
 */
@Builder
@Data
public class UserMessage {
    /**
     * 活动ID
     */
    @ApiModelProperty(value = "活动ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 发送用户id
     */
    @ApiModelProperty(value = "发送用户id")
    @TableField("send_user_id")
    private Long sendUserId;

    /**
     * 接收用户id
     */
    @ApiModelProperty(value = "接收用户id")
    @TableField("recv_user_id")
    private Long recvUserId;

    /**
     * 消息id
     */
    @ApiModelProperty(value = "消息id")
    @TableField("message_id")
    private Long messageId;

    /**
     * 是否已读
     */
    @ApiModelProperty(value = "是否已读")
    @TableField("is_read")
    private Boolean isRead;

    /**
     * 是否已读
     */
    @ApiModelProperty(value = "是否已推送")
    @TableField("need_push")
    private Boolean needPush;


    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 推送时间
     */
    @ApiModelProperty(value = "推送时间, isPush=1则无意义")
    @TableField("need_push_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime needPushTime;
}

