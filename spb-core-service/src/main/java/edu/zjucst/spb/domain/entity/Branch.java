package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.annotation.Generated;

@Data
@TableName("tb_branch")
@ApiModel(value = "branch", description = "spb业务系统-党支部模型")
public class Branch {
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @ApiModelProperty(value = "主键Id")
    @TableId(type = IdType.AUTO)
    //设置主键自增
    private Long id;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @ApiModelProperty(value = "党支部名称")
    private String branchName;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @ApiModelProperty(value = "党支部上级支部Id")
    private Long parentBranchId;

    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    @ApiModelProperty(value = "党支部信息")
    private String branchInfo;

    @Override
    @Generated("org.mybatis.generator.api.MyBatisGenerator")
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", branchId=").append(id);
        sb.append(", branchName=").append(branchName);
        sb.append(", parentBranchId=").append(parentBranchId);
        sb.append(", branchInfo=").append(branchInfo);
        sb.append("]");
        return sb.toString();
    }
}
