package edu.zjucst.spb.domain.param.activity;

import edu.zjucst.spb.domain.PageQry;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取自主添加的活动
 */
@Data
public class GetActivityParam extends PageQry {

    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @ApiModelProperty("活动时间 一周内：7 一月内：30 半年内：160")
    private Integer days;

    @ApiModelProperty("活动编号")
    private String activityNumber;


}
