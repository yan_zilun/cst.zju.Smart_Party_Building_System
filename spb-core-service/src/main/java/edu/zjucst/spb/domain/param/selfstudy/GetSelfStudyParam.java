package edu.zjucst.spb.domain.param.selfstudy;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取自主添加的活动
 */
@Data
public class GetSelfStudyParam {
    @ApiModelProperty("活动名称")
    private String activityName;

    @ApiModelProperty("学工号")
    private String userNumber;

    @ApiModelProperty("发展阶段id")
    private Integer developmentPhaseID;

    @ApiModelProperty("申请学时")
    private Integer appliedStudyHour;

//    @ApiModelProperty("审核状态 已提交/审核成功/审核失败")
//    private String auditStatus;
}
