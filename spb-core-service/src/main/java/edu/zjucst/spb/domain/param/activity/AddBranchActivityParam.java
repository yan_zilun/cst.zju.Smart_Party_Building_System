package edu.zjucst.spb.domain.param.activity;

import cn.hutool.core.bean.BeanUtil;
import edu.zjucst.spb.domain.entity.SelfActivity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

/**
 * 团委添加支部活动
 */
@Data
public class AddBranchActivityParam {
    @ApiModelProperty("活动ID 修改时传入，新增时为null")
    private Integer id;

    @ApiModelProperty("活动名称")
    private String activityName;

    @ApiModelProperty("活动级别 0院级 1校级")
    private Integer activityLevel;

    @ApiModelProperty("参与人员学工号")
    private List<String> userNumbers;

    @ApiModelProperty("主办单位")
    private String activitySponsor;

    @ApiModelProperty("活动时间")
    private LocalDate activityDate;

    @ApiModelProperty("活动图谱文件")
    private String activityGraph;

    @ApiModelProperty("申请学时")
    private Integer appliedStudyHour;

    @ApiModelProperty("附加文件链接")
    private String additionFile;

    public SelfActivity transform(String user) {
        SelfActivity activity = new SelfActivity();
        BeanUtil.copyProperties(this,activity);
        activity.setUserNumber(user);
        return activity;
    }
}
