package edu.zjucst.spb.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SelectUserVO implements Serializable {
    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("学工号")
    private String userNumber;
    @ApiModelProperty("手机号")
    private String phone;
}
