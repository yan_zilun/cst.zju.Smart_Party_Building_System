package edu.zjucst.spb.domain.param.activity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.vo.UserVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @TableName spb_activity
 */
@Data
public class ActivityParam implements Serializable {
    @TableField(exist = false)
    protected static final long serialVersionUID = 1L;
    @ApiModelProperty("活动名称")
    private String activityName;
    @ApiModelProperty("活动编号")
    private String activityNumber;
    @ApiModelProperty("活动类型")
    private String activityType;
    @ApiModelProperty("主办单位")
    private String activitySponsor;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("活动日期")
    private String activityDate;
    @ApiModelProperty("提交文件")
    private String additionFile;
    @ApiModelProperty("申请学时")
    private Integer appliedStudyHour;
    @ApiModelProperty("参与人员")
    private List<UserVo> userList;
    @ApiModelProperty("已参与/已提交人员")
    private List<UserVo> userState;
    @ApiModelProperty("备注")
    private String remark;
}
