package edu.zjucst.spb.domain.vo.stage;

import edu.zjucst.spb.domain.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ActivistQueryVO extends PageVO {

    @ApiModelProperty("入党积极分子阶段列表")
    private List<ActivistStageVO> activistStageVOList;

}
