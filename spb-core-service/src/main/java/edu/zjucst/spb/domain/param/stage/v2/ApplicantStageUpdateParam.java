package edu.zjucst.spb.domain.param.stage.v2;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ZlunYan
 * @description
 * @create 2024-5-25
 */

@Data
public class ApplicantStageUpdateParam {
    @ApiModelProperty(value = "学号")
    private String userNumber;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("入党申请书递交时间")
    private Date deliveryTime;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("入党申请人谈话登记表提交时间")
    private Date talkRegistrationTime;

    @ApiModelProperty("谈话人")
    private String talker;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("确定为积极分子时间")
    private Date activistTime;
}
