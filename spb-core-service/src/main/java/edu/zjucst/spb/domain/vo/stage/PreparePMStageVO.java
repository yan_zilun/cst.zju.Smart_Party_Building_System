package edu.zjucst.spb.domain.vo.stage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class PreparePMStageVO implements Serializable {

    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("学工号")
    private String userNumber;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @ApiModelProperty("入党介绍人")
    private String partySponsor;
    @ApiModelProperty("支部大会(接受为预备)时间")
    private Date branchPrepareTime;
    @ApiModelProperty("上级党委派人谈话时间")
    private Date committeeTalk;
    @ApiModelProperty("党委审批时间")
    private Date examineTime;
    @ApiModelProperty("预备党员阶段思想汇报提交时间")
    private String prepareThoughtReport;
    @ApiModelProperty("预备党员阶段党校培训班参与时间")
    private String preparePartyTraining;
    @ApiModelProperty("提出转正申请时间")
    private Date applyFullTime;
    @ApiModelProperty("群众意见调查表（转正前）提交时间")
    private Date probationaryPublicTime;
    @ApiModelProperty("支部大会(转正)时间")
    private Date branchFullTime;
    @ApiModelProperty("党委转正审批时间")
    private Date committeeFullTime;
    @ApiModelProperty("阶段状态")
    private String state;
}
