package edu.zjucst.spb.domain.param.user;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import edu.zjucst.spb.domain.entity.SpbUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SignUpParam {
    @ApiModelProperty(value = "学号")
    private String userNumber;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "二次确认密码")
    private String pwdConfirmed;

    public SpbUser transform() {
        SpbUser user = new SpbUser();
        String snowFlake = IdUtil.getSnowflakeNextIdStr();
        // 使用雪花算法生成ID
        user.setId(Long.valueOf(snowFlake.substring(Math.max(0,snowFlake.length()-15))));
        BeanUtil.copyProperties(this, user);
        return user;
    }
}
