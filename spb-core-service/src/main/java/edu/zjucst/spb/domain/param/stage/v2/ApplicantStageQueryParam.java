package edu.zjucst.spb.domain.param.stage.v2;

import edu.zjucst.spb.domain.PageQry;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZlunYan
 * @description
 * @create 2024-5-25
 */

@Data
public class ApplicantStageQueryParam extends PageQry implements Serializable {
    protected static final long serialVersionUID = 1L;
}
