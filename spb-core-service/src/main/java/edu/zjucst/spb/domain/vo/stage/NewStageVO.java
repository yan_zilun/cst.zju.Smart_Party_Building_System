package edu.zjucst.spb.domain.vo.stage;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-29
 */

@Data
public class NewStageVO implements Serializable {

    @ApiModelProperty("用户学工号")
    private String userNumber;

    @ApiModelProperty("用户姓名")
    private String userName;

    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("生日")
    private Date birthday;

    //  入党申请书递交时间
    @ApiModelProperty("入党申请书递交时间")
    private Date deliveryTime;
}
