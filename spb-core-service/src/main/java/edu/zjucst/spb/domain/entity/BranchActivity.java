package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 16:47
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BranchActivity {
    @TableId(type = IdType.AUTO)
    Integer id;
    String activityName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    Date activityStartTime;
    Integer activityDuringTime;
    String activityPlace;
    String activityType;
    Long userId;
    String attendStatus;
    String remark;

}
