package edu.zjucst.spb.domain.vo.stage;

import edu.zjucst.spb.domain.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class PreparePMQueryVO extends PageVO {

    @ApiModelProperty("预备党员阶段列表")
    private List<PreparePMStageVO> prepareStageVOList;

}
