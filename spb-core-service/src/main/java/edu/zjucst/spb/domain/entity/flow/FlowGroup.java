package edu.zjucst.spb.domain.entity.flow;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Data
public class FlowGroup {

    @ApiModelProperty("用户组id")
    String groupId;

    @ApiModelProperty("用户组名")
    String groupName;
}
