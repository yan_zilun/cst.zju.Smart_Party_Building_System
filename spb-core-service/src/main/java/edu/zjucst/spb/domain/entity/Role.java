package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_role")
@ApiModel(value = "role对象", description = "")
public class Role {
    @ApiModelProperty(value = "权限Id")
    @TableId("id")
    private Long roleId;

    @ApiModelProperty(value = "权限名称")
    private String roleName;
}
