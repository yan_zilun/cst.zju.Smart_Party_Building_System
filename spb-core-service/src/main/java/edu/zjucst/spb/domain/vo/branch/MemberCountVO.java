package edu.zjucst.spb.domain.vo.branch;

import lombok.Data;

/**
 * @title:
 * @Author luxing
 * @Date: 2024-04-24 13:41
 * @wiki
 */
@Data
public class MemberCountVO {
    // 党员人数
    private Integer partyMember;
    // 预备党员人数
    private Integer preparePartyMember;
}
