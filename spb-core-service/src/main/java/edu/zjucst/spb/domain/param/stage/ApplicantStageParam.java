package edu.zjucst.spb.domain.param.stage;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.vo.UserVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class ApplicantStageParam implements Serializable {
    @ApiModelProperty("需要修改的用户")
    private List<UserVo> userList;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;
    @ApiModelProperty("是否共青团员")
    private String isLeague;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("入党申请书递交时间")
    private Date deliveryTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("入党申请人谈话登记表提交时间")
    private Date talkRegistrationTime;
    @ApiModelProperty("谈话人")
    private String talker;
    @ApiModelProperty("资格审查时间间隔")
    private String qualificationInterval;
    @ApiModelProperty("是否满足推优条件（系统判断）")
    private String isPromote;
    @ApiModelProperty("阶段状态")
    private String state;
}
