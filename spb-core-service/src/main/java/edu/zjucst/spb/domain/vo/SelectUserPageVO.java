package edu.zjucst.spb.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class SelectUserPageVO extends PageVO{

    @ApiModelProperty("用户列表")
    private List<SelectUserVO> UserVOList;

}
