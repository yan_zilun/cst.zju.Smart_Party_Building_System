package edu.zjucst.spb.domain.param.stage.joinParty;

import com.baomidou.mybatisplus.annotation.TableField;
import edu.zjucst.spb.domain.PageQry;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Data
public class TODOTaskQueryParam extends PageQry implements Serializable {

    @TableField(exist = false)
    protected static final long serialVersionUID = 1L;

    @ApiModelProperty("党支部名称[测试用]")
    private String branchName;
}
