package edu.zjucst.spb.domain.entity.stage;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author ZlunYan
 * @description 入党申请人阶段
 * @create 2024-5-25
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_applicant_stage")
@ApiModel(value = "applicant stage", description = "")
public class ApplicantStage extends BaseStage {
    @TableId(type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户学工号")
    private String userNumber;

    @ApiModelProperty("入党申请书递交时间")
    private Date deliveryTime;
    @ApiModelProperty("入党申请人谈话登记表提交时间")
    private Date talkRegistrationTime;
    @ApiModelProperty("谈话人(姓名)")
    private String talker;
    @ApiModelProperty("确定为积极分子时间")
    private Date activistTime;
}
