package edu.zjucst.spb.domain.vo.stage;

import edu.zjucst.spb.domain.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ApplicantQueryVO extends PageVO {

    @ApiModelProperty("入党申请人阶段列表")
    private List<ApplicantStageVO> applicantStageVOList;
}
