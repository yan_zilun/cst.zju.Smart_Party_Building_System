package edu.zjucst.spb.domain.param.flow;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StartProcessInstanceParam {
    @ApiModelProperty(value = "流程定义名")
    String processName;

    @ApiModelProperty(value = "发起人")
    String userId;

    @ApiModelProperty(value = "启动参数")
    // 等级
    // {"activity_level": 1}
    // 指定审批组
    // {"group1": 1}
    Map<String, Object> startParam;

}
