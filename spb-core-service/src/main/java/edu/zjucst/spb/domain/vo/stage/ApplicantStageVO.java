package edu.zjucst.spb.domain.vo.stage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ApplicantStageVO implements Serializable {
    @ApiModelProperty("用户学工号")
    private String userNumber;
    @ApiModelProperty("用户姓名")
    private String userName;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;
    @ApiModelProperty("是否共青团员")
    private String isLeague;
    @ApiModelProperty("入党申请书递交时间")
    private Date deliveryTime;
    @ApiModelProperty("入党申请人谈话登记表提交时间")
    private Date talkRegistrationTime;
    @ApiModelProperty("谈话人")
    private String talker;
    @ApiModelProperty("资格审查时间间隔")
    private String qualificationInterval;
    @ApiModelProperty("是否满足推优条件")
    private String isPromote;
    @ApiModelProperty("阶段状态")
    private String state;
}
