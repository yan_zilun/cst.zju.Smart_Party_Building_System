package edu.zjucst.spb.domain.param.flow;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompleteTaskParam {
    @ApiModelProperty(value = "任务id")
    String taskId;

    @ApiModelProperty(value = "任务相关参数")
    Map<String, Object> taskParam;
}
