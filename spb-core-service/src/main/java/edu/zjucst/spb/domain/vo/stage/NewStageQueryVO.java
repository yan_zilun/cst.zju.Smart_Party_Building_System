package edu.zjucst.spb.domain.vo.stage;

import edu.zjucst.spb.domain.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-29
 */

@Data
public class NewStageQueryVO extends PageVO {

    @ApiModelProperty("查询列表")
    private List<NewStageVO> stageVOList;
}
