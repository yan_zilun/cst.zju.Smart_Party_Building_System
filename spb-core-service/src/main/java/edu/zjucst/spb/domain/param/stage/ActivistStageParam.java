package edu.zjucst.spb.domain.param.stage;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.vo.UserVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class ActivistStageParam {

    @ApiModelProperty("需要修改的用户")
    private List<UserVo> userList;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("推优时间")
    private Date promoteTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("确定为积极分子时间")
    private Date activistTime;
    @ApiModelProperty("思想汇报提交时间(自动接入)")
    private String thoughtReport;
    @ApiModelProperty("入党积极分子和发展对象培养教育考察登记表提交时间")
    private String educationalVisit;
    @ApiModelProperty("党校培训班参与时间(自动接入)")
    private String activistPartyTraining;
    @ApiModelProperty("培养联系人")
    private String cultivateContacts;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("群众意见调查表(确定为发展对象)提交时间")
    private Date developmentPublicTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("班主任导师意见征求表提交时间")
    private Date teacherTime;
    @ApiModelProperty("阶段状态")
    private String state;
}
