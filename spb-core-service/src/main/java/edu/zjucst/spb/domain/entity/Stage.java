package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_stage")
@ApiModel(value = "stage对象", description = "")
/**
 * @author ZlunYan
 * @description
 * @create 2023-11-28
 */
public class Stage {

    private static final Integer uploadThoughtReportGap = 3;

    @TableId(type = IdType.AUTO)  //从一开始自增  不加就随机
    private Integer id;

    @ApiModelProperty("用户学工号")
    private String userNumber;

    @ApiModelProperty("用户姓名")
    private String userName;

    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @ApiModelProperty("发展阶段ID")
    private Integer developmentPhaseId;

    @ApiModelProperty("生日")
    private Date birthday;

    //  共青团员阶段
    @ApiModelProperty("入党申请书递交时间")
    private Date deliveryTime;

    // 入党申请人阶段
    @ApiModelProperty("入党申请人谈话登记表提交时间")
    private Date talkRegistrationTime;
    @ApiModelProperty("谈话人(姓名)")  // TODO 是否需要以学工号存储
    private String talker;
//    @ApiModelProperty("推优时间")
//    private Date promoteTime;
    @ApiModelProperty("确定为积极分子时间")
    private Date activistTime;

    @TableField(exist = false)
    @ApiModelProperty("下一次上交思想汇报的时间")
    private Date nxtThoughtReportUploadTime;


    // 积极分子阶段
    @ApiModelProperty("思想汇报提交次数")  // TODO 下一次提交思想汇报时间 需要提交几次思想汇报的计算(通过确定为积极分子的时间计算)
    private Integer thoughtReportCnt = 0;
    @ApiModelProperty("完成日常考察时间")
    private Date educationalVisitTime;
    @ApiModelProperty("完成积极分子党校培训班时间")
    private Date activistPartyTrainingTime;

    @ApiModelProperty("培养联系人(姓名)")
    private String cultivateContacts1;
    @ApiModelProperty("培养联系人2(姓名)")
    private String cultivateContacts2;

    @ApiModelProperty("完成群调时间")
    private Date developmentPublicTime;
//    @ApiModelProperty("班主任导师意见征求表提交时间")
//    private Date teacherTime;
    @ApiModelProperty("确定为发展对象时间")
    private Date confirmTime;
    @ApiModelProperty("发展对象备案时间")
    private Date recordTime;


    // 记录开展入党流程的id
    @ApiModelProperty("记录开展入党流程的id")
    private String processId;

    public Date getNxtThoughtReportUploadTime() {
        if (this.activistTime == null || this.developmentPhaseId != 2) return null;

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.activistTime);
        calendar.add(Calendar.MONTH, uploadThoughtReportGap * this.thoughtReportCnt);

        return calendar.getTime();
    }

    public void setDevelopmentPhaseId(Integer developmentPhaseId) {
        this.developmentPhaseId = developmentPhaseId;
        this.developmentPhase = DevelopmentPhaseId2Phase.getPhase().get(developmentPhaseId);
    }

    static class DevelopmentPhaseId2Phase {
        private static final List<String> phase = Arrays.asList(
            "共青团员",
            "入党申请人",
            "积极分子",
            "发展对象",
            "预备党员",
            "正式党员"
        );

        public static List<String> getPhase() {
            return phase;
        }
    }
}

//public class Stage {
//    @TableId(type = IdType.AUTO)  //从一开始自增  不加就随机
//    private Integer id;
//
//    @ApiModelProperty("用户学工号")
//    private String userNumber;
//    @ApiModelProperty("用户姓名")
//    private String userName;
//    @ApiModelProperty("发展阶段")
//    private String developmentPhase;
//    @ApiModelProperty("发展阶段ID")
//    private Integer developmentPhaseId;
//
//    // 共青团员阶段
//    @ApiModelProperty("是否共青团员")
//    private Integer isLeague;
//    @ApiModelProperty("入党申请书递交时间")
//    private Date deliveryTime;
//
//    // 入党申请人阶段
//    @ApiModelProperty("入党申请人谈话登记表提交时间")
//    private Date talkRegistrationTime;
//    @ApiModelProperty("谈话人")
//    private String talker;
//    @ApiModelProperty("推优时间")
//    private Date promoteTime;
//    @ApiModelProperty("确定为积极分子时间")
//    private Date activistTime;
//
//    // 积极分子阶段
//    @ApiModelProperty("思想汇报提交时间")
//    private String thoughtReport;
//    @ApiModelProperty("入党积极分子和发展对象培养教育考察登记表提交时间")
//    private String educationalVisit;
//    @ApiModelProperty("党校培训班参与时间")
//    private String activistPartyTraining;
//    @ApiModelProperty("培养联系人")
//    private String cultivateContacts;
//    @ApiModelProperty("群众意见调查表(确定为发展对象)提交时间")
//    private Date developmentPublicTime;
//    @ApiModelProperty("班主任导师意见征求表提交时间")
//    private Date teacherTime;
//    @ApiModelProperty("确定为发展对象时间")
//    private Date confirmTime;
//    @ApiModelProperty("发展对象备案时间")
//    private Date recordTime;
//
//    // 发展对象阶段
//    @ApiModelProperty("入党介绍人")
//    private String partySponsor;
//    @ApiModelProperty("入党对象政治审查表提交时间")
//    private Date politicalReviewTime;
//    @ApiModelProperty("发展对象阶段党校培训班参与时间")
//    private String developPartyTraining;
//    @ApiModelProperty("审查时间")
//    private Date investigateTime;
//    @ApiModelProperty("预审时间")
//    private Date preliminaryInvestigateTime;
//    @ApiModelProperty("入党志愿书提交时间")
//    private Date partyApplicationTime;
//    @ApiModelProperty("是否符合转为预备党员")
//    private String recommendable;
//    @ApiModelProperty("支部大会(接受为预备)时间")
//    private Date branchPrepareTime;
//
//    // 预备党员阶段
//    @ApiModelProperty("上级党委派人谈话时间")
//    private Date committeeTalk;
//    @ApiModelProperty("党委审批时间")
//    private Date examineTime;
//    @ApiModelProperty("预备党员阶段思想汇报提交时间")
//    private String prepareThoughtReport;
//    @ApiModelProperty("预备党员阶段党校培训班参与时间")
//    private String preparePartyTraining;
//    @ApiModelProperty("提出转正申请时间")
//    private Date applyFullTime;
//    @ApiModelProperty("群众意见调查表（转正前）提交时间")
//    private Date probationaryPublicTime;
//    @ApiModelProperty("支部大会(转正)时间")
//    private Date branchFullTime;
//    @ApiModelProperty("党委转正审批时间")
//    private Date committeeFullTime;
//    @ApiModelProperty("阶段状态")
//    private String state;
//
//    @TableField(fill = FieldFill.INSERT)
//    private Date createTime;
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private Date updateTime;
//}
