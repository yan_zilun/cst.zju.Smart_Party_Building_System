package edu.zjucst.spb.domain.entity.message;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * (UserMessageText)表实体类
 *
 * @author Mrli
 * @since 2023-04-05 14:10:48
 */
@Data
public class UserMessageText {
    //消息ID
    @ApiModelProperty("消息内容id")
    @TableId(value = "id", type = IdType.AUTO)
    private Long          id;
    /**
     * 标题
     */
    @ApiModelProperty("标题")
    @TableField("title")
    private String        title;
    /**
     * 子标题
     */
    @ApiModelProperty("子标题")
    @TableField("subtitle")
    private String        subtitle;
    /**
     * 文本具体内容
     */
    @ApiModelProperty("文本具体内容")
    @TableField("content")
    private String        content;
    /**
     * 附件链接, 多个用;分割
     */
    @ApiModelProperty("附件链接, 多个用;分割")
    @TableField("attachments")
    private String        attachments;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("create_time")
    private LocalDateTime createTime;

    //更新时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("update_time")
    private LocalDateTime updateTime;


    public UserMessageText(String content) {
        this.content = content;
        this.attachments = "";
        this.title = "";
    }

    public UserMessageText(String title, String attachments, String content) {
        this.content = content;
        this.attachments = attachments;
        this.title = title;
    }

    public UserMessageText(Long id, String title, String subtitle, String attachments, String content,
                           LocalDateTime createTime, LocalDateTime updateTime) {
        this.id = id;
        this.content = content;
        this.attachments = attachments;
        this.title = title;
        this.subtitle = subtitle;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

}

