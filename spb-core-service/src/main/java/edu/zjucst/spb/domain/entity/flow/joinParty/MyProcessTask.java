package edu.zjucst.spb.domain.entity.flow.joinParty;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Data
public class MyProcessTask {

    @ApiModelProperty("任务Id")
    String id;

    @ApiModelProperty("任务名")
    String name;

    @ApiModelProperty("任务描述")
    String description;

    @ApiModelProperty("任务处理人")
    String assignee;
}
