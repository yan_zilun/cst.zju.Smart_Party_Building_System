package edu.zjucst.spb.domain.param.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginParam {
    @ApiModelProperty(value = "学号")
    private String userNumber;

    @ApiModelProperty(value = "密码")
    private String pwd;

    @ApiModelProperty(value = "验证码key")
    private String codeKey;

    @ApiModelProperty(value = "验证码")
    private String verifyCode;
}
