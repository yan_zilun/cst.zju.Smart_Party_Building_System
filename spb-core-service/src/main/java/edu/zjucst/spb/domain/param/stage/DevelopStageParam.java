package edu.zjucst.spb.domain.param.stage;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.vo.UserVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class DevelopStageParam {
    @ApiModelProperty("需要修改的用户")
    private List<UserVo> userList;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("确定为发展对象时间")
    private Date confirmTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("发展对象备案时间")
    private Date recordTime;
    @ApiModelProperty("入党介绍人")
    private String partySponsor;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("入党对象政治审查表提交时间")
    private Date politicalReviewTime;
    @ApiModelProperty("发展对象阶段党校培训班参与时间(自动接入)")
    private String developPartyTraining;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("审查时间")
    private Date investigateTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("预审时间")
    private Date preliminaryInvestigateTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("入党志愿书提交时间")
    private Date partyApplicationTime;
    @ApiModelProperty("是否符合转为预备党员")
    private String recommendable;
    @ApiModelProperty("阶段状态")
    private String state;

}
