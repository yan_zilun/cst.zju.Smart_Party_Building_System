package edu.zjucst.spb.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserVo {

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "学号")
    private String userNumber;

}
