package edu.zjucst.spb.domain.param.message;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GetUserMessageParam {
    /**
     * 活动ID
     */
    @ApiModelProperty(value = "活动ID")
    private Long id;

    /**
     * 发送用户id
     */
    @ApiModelProperty(value = "发送用户学号")
    private Long sendUserId;

    /**
     * 接收用户id
     */
    @ApiModelProperty(value = "接收用户学号")
    private Long recvUserId;

    /**
     * 消息id
     */
    @ApiModelProperty(value = "消息id")
    private Long messageId;

    /**
     * 是否已读
     */
    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;

    /**
     * 是否已读
     */
    @ApiModelProperty(value = "是否已推送")
    private Boolean needPush;
}
