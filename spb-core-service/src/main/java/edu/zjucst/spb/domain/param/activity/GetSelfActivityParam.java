package edu.zjucst.spb.domain.param.activity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 获取自主添加的活动
 */
@Data
public class GetSelfActivityParam{

    @ApiModelProperty("活动名称")
    private String activityName;

    @ApiModelProperty("学工号")
    private String userNumber;

    @ApiModelProperty("学年度")
    private Integer studyYear;

    @ApiModelProperty("审核状态 已提交/审核成功/审核失败")
    private String auditStatus;


}
