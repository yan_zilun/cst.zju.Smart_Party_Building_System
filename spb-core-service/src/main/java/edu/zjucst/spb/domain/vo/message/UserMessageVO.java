package edu.zjucst.spb.domain.vo.message;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

public class UserMessageVO {
    /**
     * 活动ID
     */
    @ApiModelProperty(value = "活动ID")
    private Long id;

    /**
     * 发送用户id
     */
    @ApiModelProperty(value = "发送用户学号")
    private Long sendUserId;

    /**
     * 接收用户id
     */
    @ApiModelProperty(value = "接收用户学号")
    private Long recvUserId;

    /**
     * 消息id
     */
    @ApiModelProperty(value = "消息id")
    private Long messageId;

    /**
     * 是否已读
     */
    @ApiModelProperty(value = "是否已读")
    private Boolean isRead;

    /**
     * 是否已读
     */
    @ApiModelProperty(value = "是否已推送")
    private Boolean needPush;


    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;

    /**
     * 推送时间
     */
    @ApiModelProperty(value = "推送时间, isPush=1则无意义")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime needPushTime;

//    public UserMessage transform() {
//        UserMessage userMessage = new UserMessage();
//        BeanUtil.copyProperties(this, userMessage);
//        return userMessage;
//    }
}
