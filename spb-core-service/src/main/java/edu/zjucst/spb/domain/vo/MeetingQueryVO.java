package edu.zjucst.spb.domain.vo;

import edu.zjucst.spb.domain.entity.Meeting;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

//
@Data
public class MeetingQueryVO extends PageVO{

    @ApiModelProperty("会议列表数据")
    private List<Meeting> meetings;
}
