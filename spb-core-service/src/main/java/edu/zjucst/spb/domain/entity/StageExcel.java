package edu.zjucst.spb.domain.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class StageExcel {
    private String developmentPhase;
    private String userNumber;
    private String userName;
    private String isLeague;
    private String deliveryTime;
    private String talkRegistrationTime;
    private String promoteTime;
    private String activistTime;
    private String educationalVisit;
    private String partySponsor;
    private String probationaryPublicTime;
    private String confirmTime;
    private String recordTime;


}
