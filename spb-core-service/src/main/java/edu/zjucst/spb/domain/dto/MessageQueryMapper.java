package edu.zjucst.spb.domain.dto;


import edu.zjucst.spb.domain.param.message.MessageQueryParam;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MessageQueryMapper {
    MessageQueryMapper INSTANCE = Mappers.getMapper(MessageQueryMapper.class);


    MessageQueryDTO param2dto(MessageQueryParam param);

}
