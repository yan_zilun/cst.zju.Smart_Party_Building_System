package edu.zjucst.spb.domain.entity.message;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * (SystemMessage)表实体类
 *
 * @author Mrli
 * @since 2023-04-05 13:34:53
 */
@Data
public class SystemMessage {
    //消息ID
    private Long id;
    //发送人id
    private Long sendUserId;
    //文本内容
    private String content;

    //添加时自动填充
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    //添加或修改时自动填充
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    // @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}

