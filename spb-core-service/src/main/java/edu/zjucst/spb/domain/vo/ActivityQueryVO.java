package edu.zjucst.spb.domain.vo;

import edu.zjucst.spb.domain.entity.Activity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ActivityQueryVO  extends  PageVO{
    @ApiModelProperty("活动列表数据")
    private List<Activity> activities;
}
