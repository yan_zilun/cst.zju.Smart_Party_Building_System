package edu.zjucst.spb.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PageVO {

    @ApiModelProperty("当面页面")
    private Long current;

    @ApiModelProperty("页面大小")
    private Long pageSize;

    @ApiModelProperty("总页面数")
    private Long pages;


}
