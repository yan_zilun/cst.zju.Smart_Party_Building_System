package edu.zjucst.spb.domain.dto;

import edu.zjucst.spb.domain.param.message.MessageQueryParam;
import lombok.Data;

/**
 * @author MrLi
 * @date 2023-05-19 20:11
 **/
@Data
public class MessageQueryDTO extends MessageQueryParam {

    private Long recvUserId;

}
