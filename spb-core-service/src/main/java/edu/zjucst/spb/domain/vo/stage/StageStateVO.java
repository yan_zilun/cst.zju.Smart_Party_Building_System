package edu.zjucst.spb.domain.vo.stage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StageStateVO {
    @ApiModelProperty("发展阶段")
    private String developmentPhase;
    @ApiModelProperty("阶段状态")
    private String state;
}
