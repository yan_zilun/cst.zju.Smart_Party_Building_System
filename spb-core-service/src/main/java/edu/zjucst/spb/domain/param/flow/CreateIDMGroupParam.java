package edu.zjucst.spb.domain.param.flow;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateIDMGroupParam {
    @ApiModelProperty(value = "用户组name")
    String name;
}
