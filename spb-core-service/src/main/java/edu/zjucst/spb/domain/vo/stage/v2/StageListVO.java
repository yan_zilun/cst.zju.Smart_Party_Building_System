package edu.zjucst.spb.domain.vo.stage.v2;

import edu.zjucst.spb.domain.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2024-5-25
 */

@Data
public class StageListVO<T> extends PageVO {

    @ApiModelProperty("查询列表")
    private List<T> stageVOList;
}
