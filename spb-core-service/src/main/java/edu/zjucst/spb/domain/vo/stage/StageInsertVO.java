package edu.zjucst.spb.domain.vo.stage;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-29
 */
@Data
public class StageInsertVO {

    @ApiModelProperty("用户学工号")
    private String userNumber;

    @ApiModelProperty("用户姓名")
    private String userName;

    @ApiModelProperty("发展阶段ID 0:共青团员 1:积极分子 2:发展对象 3:预备党员 4:正式党员")
    private Integer developmentPhaseId;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("生日 yyyy-MM-dd")
    private Date birthday;

}
