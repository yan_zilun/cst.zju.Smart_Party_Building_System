package edu.zjucst.spb.domain.vo.stage;

import edu.zjucst.spb.domain.vo.PageVO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class DevelopQueryVO extends PageVO {

    @ApiModelProperty("发展对象阶段列表")
    private List<DevelopStageVO> developStageVOList;
}
