package edu.zjucst.spb.domain.vo;

import cn.hutool.core.bean.BeanUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.entity.SelfActivity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 自主添加活动
 */
@Data
public class MySelfActivityVO {
    @ApiModelProperty("活动ID")
    private Integer id;

    @ApiModelProperty("活动名称")
    private String activityName;

    @ApiModelProperty("活动级别 0院级 1校级")
    private Integer activityLevel;

    @ApiModelProperty("学工号")
    private String userNumber;

    @ApiModelProperty("主办单位")
    private String activitySponsor;

    @ApiModelProperty("活动时间")
    private LocalDate activityDate;

    @ApiModelProperty("提交时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("审核状态 已提交/审核成功/审核失败")
    private String auditStatus;

    @ApiModelProperty("审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime auditTime;

    public SelfActivity transform() {
        SelfActivity activity = new SelfActivity();
        BeanUtil.copyProperties(this, activity);
        return activity;
    }
}
