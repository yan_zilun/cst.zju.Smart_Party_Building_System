package edu.zjucst.spb.domain.param.stage.joinParty;

import com.baomidou.mybatisplus.annotation.TableField;
import edu.zjucst.spb.domain.PageQry;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Data
public class MyProcessQueryParam extends PageQry implements Serializable {
    @TableField(exist = false)
    protected static final long serialVersionUID = 1L;

    @ApiModelProperty("学工号")
    private String userNumber;
}
