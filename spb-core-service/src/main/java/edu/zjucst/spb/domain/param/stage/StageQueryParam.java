package edu.zjucst.spb.domain.param.stage;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.PageQry;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class StageQueryParam extends PageQry implements Serializable {
    @TableField(exist = false)
    protected static final long serialVersionUID = 1L;
    @ApiModelProperty("姓名(选填)")
    private String userName;
    @ApiModelProperty("学工号(选填)")
    private String userNumber;

    @ApiModelProperty("入党状态(选填) 0:共青团员 1:入党申请人 2:积极分子 3:发展对象 4:预备党员 5:正式党员")
    private Integer developId;

    @ApiModelProperty("日期查询类型(选填) -1:不进行日期约束 0:生日时间 1:递交入党申请书时间 2:成为积极分子时间 3:成为发展对象时间 4:成为预备党员时间 5:成为正式党员时间")
    private Integer dateType;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("起始时间时间(选填)")
    private Date startTime;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("终止时间时间(选填)")
    private Date endTime;
}
