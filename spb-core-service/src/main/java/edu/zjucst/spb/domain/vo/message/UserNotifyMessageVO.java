package edu.zjucst.spb.domain.vo.message;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * (UserMessage)表实体类
 *
 * @author Mrli
 * @since 2023-04-05 14:17:20
 */
@Data
public class UserNotifyMessageVO {
    // 发送人名称
    private String sendUserName;

    // 消息标题
    private String messageTitle;

    // 消息内容
    private String messageText;

    // 附件链接
    private String attachments;

    // 是否已读
    private Integer isRead;

    // 创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    // 更新时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
}

