package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @TableName spb_activity
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_activity")
@ApiModel(value = "activity对象", description = "")
public class Activity implements Serializable {
    @TableId(type = IdType.AUTO)  // 从一开始自增  不加就随机
    private Integer id;
    @ApiModelProperty("活动编号")
    private String activityNumber;
    @ApiModelProperty("活动类型")
    private String activityType;
    @ApiModelProperty("活动名称")
    private String activityName;
    @ApiModelProperty("用户学工号")
    private String userNumber;
    @ApiModelProperty("用户姓名")
    private String userName;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;
    @ApiModelProperty("活动主办方")
    private String activitySponsor;
    @ApiModelProperty("活动日期")
    private Date activityDate;
    @ApiModelProperty("提交文件")
    private String additionFile;
    @ApiModelProperty("申请学时")
    private Integer appliedStudyHour;
    @ApiModelProperty("状态")
    private String state;
    @ApiModelProperty("备注")
    private String remark;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
