package edu.zjucst.spb.domain.param.flow;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateIDMUserParam {
    @ApiModelProperty(value = "用户id")
    String id;

    @ApiModelProperty(value = "用户名")
    String displayName;

}
