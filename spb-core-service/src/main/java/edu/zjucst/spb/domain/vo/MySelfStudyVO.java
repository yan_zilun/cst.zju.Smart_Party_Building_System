package edu.zjucst.spb.domain.vo;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.entity.SelfStudy;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 自主添加活动
 */
@Data
public class MySelfStudyVO {
    @ApiModelProperty("活动ID")
    private Integer id;

    @ApiModelProperty("活动名称")
    private String activityName;

    @ApiModelProperty("学工号")
    private String userNumber;

    @ApiModelProperty("发展阶段id")
    private Integer developmentPhaseId;

    @ApiModelProperty("申请时间")
    private Date appliedDate;

    @ApiModelProperty("申请学时")
    private Integer appliedStudyHour;

//    @ApiModelProperty("审核状态 已提交/审核成功/审核失败")
//    private String auditStatus;
//
//    @ApiModelProperty("审核时间")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    private LocalDateTime auditTime;

    public SelfStudy transform() {
        SelfStudy selfStudy = new SelfStudy();
        BeanUtil.copyProperties(this, selfStudy);
        return selfStudy;
    }
}
