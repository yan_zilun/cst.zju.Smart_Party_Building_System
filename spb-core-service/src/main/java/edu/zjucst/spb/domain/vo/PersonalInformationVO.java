package edu.zjucst.spb.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class PersonalInformationVO {
    @ApiModelProperty(value = "用户名")
    private String userName;


    @ApiModelProperty(value = "学号")
    private String userNumber;


    @ApiModelProperty(value = "性别, 默认为, 0未知1男2女")
    private Integer sex;


    @ApiModelProperty(value = "政治面貌")
    private String politicsStatus;


    @ApiModelProperty(value = "入党状态，如入党申请阶段/积极分子阶段/发展对象阶段/预备党员阶段")
    private String developmentPhase;

    @ApiModelProperty(value = "所在党支部")
    private String partyBranch;

}
