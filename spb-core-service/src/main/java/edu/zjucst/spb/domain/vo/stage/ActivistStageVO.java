package edu.zjucst.spb.domain.vo.stage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ActivistStageVO implements Serializable {

    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("学工号")
    private String userNumber;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @ApiModelProperty("推优时间")
    private Date promoteTime;
    @ApiModelProperty("确定为积极分子时间")
    private Date activistTime;
    @ApiModelProperty("思想汇报提交时间")
    private String thoughtReport;
    @ApiModelProperty("入党积极分子和发展对象培养教育考察登记表提交时间")
    private String educationalVisit;
    @ApiModelProperty("党校培训班参与时间")
    private String activistPartyTraining;
    @ApiModelProperty("培养联系人")
    private String cultivateContacts;
    @ApiModelProperty("群众意见调查表(确定为发展对象)提交时间")
    private Date developmentPublicTime;
    @ApiModelProperty("班主任导师意见征求表提交时间")
    private Date teacherTime;
    @ApiModelProperty("阶段状态")
    private String state;
}
