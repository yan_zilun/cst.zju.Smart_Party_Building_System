package edu.zjucst.spb.domain.param.flow;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeployProcessDefinitionParam {
    @ApiModelProperty(value = "流程定义文件名")
    String file;

    @ApiModelProperty(value = "流程定义名")
    String name;
}
