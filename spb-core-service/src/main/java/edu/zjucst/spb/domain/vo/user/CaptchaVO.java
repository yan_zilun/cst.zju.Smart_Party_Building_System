package edu.zjucst.spb.domain.vo.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CaptchaVO  {

    @ApiModelProperty(value = "验证码key")
    private String key;
    @ApiModelProperty(value = "验证码图片 base64编码")
    private String image;

}
