package edu.zjucst.spb.domain.param.stage;

import com.fasterxml.jackson.annotation.JsonFormat;
import edu.zjucst.spb.domain.vo.UserVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class PreparePMStageParam {

    @ApiModelProperty("需要修改的用户")
    private List<UserVo> userList;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("支部大会(接受为预备)时间")
    private Date branchPrepareTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("上级党委派人谈话时间")
    private Date committeeTalk;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("党委审批时间")
    private Date examineTime;
    @ApiModelProperty("预备党员阶段思想汇报提交时间(自动接入)")
    private String prepareThoughtReport;
    @ApiModelProperty("预备党员阶段党校培训班参与时间(自动接入)")
    private String preparePartyTraining;
    @ApiModelProperty("入党介绍人")
    private String partySponsor;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("提出转正申请时间")
    private Date applyFullTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("群众意见调查表（转正前）提交时间")
    private Date probationaryPublicTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("支部大会(转正)时间")
    private Date branchFullTime;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("党委转正审批时间")
    private Date committeeFullTime;
    @ApiModelProperty("阶段状态")
    private String state;
}
