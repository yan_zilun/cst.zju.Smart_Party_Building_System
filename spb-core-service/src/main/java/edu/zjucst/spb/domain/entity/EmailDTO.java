package edu.zjucst.spb.domain.entity;

import lombok.Data;

/**
 * @author MrLi
 * @date 2023-05-26 20:00
 **/
@Data
public class EmailDTO {
    /**
     * 收件人邮箱
     */
    private String toMail;

    /**
     * 抄送人
     */
    private String cc;

    /**
     * 主题
     */
    private String subject;

    /**
     * 正文
     */
    private String content;
}
