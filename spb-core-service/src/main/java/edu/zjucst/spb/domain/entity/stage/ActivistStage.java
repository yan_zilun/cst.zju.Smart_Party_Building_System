package edu.zjucst.spb.domain.entity.stage;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author ZlunYan
 * @description 积极分子阶段
 * @create 2024-5-25
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_activist_stage")
@ApiModel(value = "activist stage", description = "")
public class ActivistStage extends BaseStage {
    @TableId(type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户学工号")
    private String userNumber;

    @ApiModelProperty("思想汇报提交次数")
    private Integer thoughtReportCnt = 0;
    @ApiModelProperty("完成日常考察时间")
    private Date educationalVisitTime;
    @ApiModelProperty("完成积极分子党校培训班时间")
    private Date activistPartyTrainingTime;

    @ApiModelProperty("培养联系人(姓名)")
    private String cultivateContacts1;
    @ApiModelProperty("培养联系人2(姓名)")
    private String cultivateContacts2;

    @ApiModelProperty("完成群调时间")
    private Date developmentPublicTime;
    @ApiModelProperty("确定为发展对象时间")
    private Date confirmTime;
    @ApiModelProperty("发展对象备案时间")
    private Date recordTime;
}
