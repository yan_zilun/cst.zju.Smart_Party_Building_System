package edu.zjucst.spb.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 自主活动类
 * @author qingo
 * @since 2023-04-18
 */
@Data
@TableName("tb_self_activity")
@ApiModel(value = "SelfActivity对象", description = "")
public class SelfActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("活动ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("活动名称")
    @TableField("activity_name")
    private String activityName;

    @ApiModelProperty("活动级别 0院级 1校级")
    @TableField("activity_level")
    private Integer activityLevel;

    @ApiModelProperty("学工号")
    @TableField("user_number")
    private String userNumber;

    @ApiModelProperty("主办单位")
    @TableField("activity_sponsor")
    private String activitySponsor;

    @ApiModelProperty("活动时间")
    @TableField("activity_date")
    private Date activityDate;

    @ApiModelProperty("活动图谱文件")
    @TableField("activity_graph")
    private String activityGraph;

    @ApiModelProperty("申请学时")
    @TableField("applied_study_hour")
    private Integer appliedStudyHour;

    @ApiModelProperty("附加文件链接或者文件名")
    @TableField("addition_file")
    private String additionFile;

    @ApiModelProperty("审核状态")
    @TableField("audit_status")
    private String auditStatus;

    @ApiModelProperty("审核时间")
    @TableField("audit_time")
    private LocalDateTime auditTime;

    @ApiModelProperty("创建时间")
//    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


}




