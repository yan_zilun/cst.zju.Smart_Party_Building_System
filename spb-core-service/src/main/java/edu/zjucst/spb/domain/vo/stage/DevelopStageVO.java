package edu.zjucst.spb.domain.vo.stage;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class DevelopStageVO implements Serializable {
    @ApiModelProperty("姓名")
    private String userName;
    @ApiModelProperty("学工号")
    private String userNumber;
    @ApiModelProperty("发展阶段")
    private String developmentPhase;

    @ApiModelProperty("确定为发展对象时间")
    private Date confirmTime;
    @ApiModelProperty("发展对象备案时间")
    private Date recordTime;
    @ApiModelProperty("入党介绍人")
    private String partySponsor;
    @ApiModelProperty("入党对象政治审查表提交时间")
    private Date politicalReviewTime;
    @ApiModelProperty("发展对象阶段党校培训班参与时间")
    private String developPartyTraining;
    @ApiModelProperty("审查时间")
    private Date investigateTime;
    @ApiModelProperty("预审时间")
    private Date preliminaryInvestigateTime;
    @ApiModelProperty("入党志愿书提交时间")
    private Date partyApplicationTime;
    @ApiModelProperty("是否符合转为预备党员")
    private String recommendable;
    @ApiModelProperty("阶段状态")
    private String state;

}
