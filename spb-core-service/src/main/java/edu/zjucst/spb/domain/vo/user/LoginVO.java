package edu.zjucst.spb.domain.vo.user;

import lombok.Data;

/**
 * @author MrLi
 * @date 2023-05-16 21:02
 **/
@Data
public class LoginVO {

//    主键Id
    private Long userId;
    /**
     * 学号
     */

    private String userNumber;
    /**
     * 用户名
     */
    private String username;


    /**
     * 权限
     * @see edu.zjucst.spb.domain.entity.SpbUser#roleName
     */
    private String roleName;
}
