package edu.zjucst.spb.service.joinParty.impl;

import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.domain.entity.flow.joinParty.MyProcessTask;
import edu.zjucst.spb.domain.param.stage.joinParty.JoinPartyTaskListParam;
import edu.zjucst.spb.domain.vo.flow.joinParty.MyProcessTaskVO;
import edu.zjucst.spb.enums.JoinPartyExceptionEnum;
import edu.zjucst.spb.exception.BizException;
import edu.zjucst.spb.pkg.flow.stage.StageCheckComponent;
import edu.zjucst.spb.service.FileService;
import edu.zjucst.spb.service.joinParty.JoinPartyFlowService;
import edu.zjucst.spb.service.StageService;
import edu.zjucst.spb.util.converMapper.JoinPartyConvertMapper;
import org.flowable.engine.IdentityService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.form.api.FormInfo;
import org.flowable.form.model.FormField;
import org.flowable.form.model.SimpleFormModel;
import org.flowable.idm.api.Group;
import org.flowable.task.api.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@Service
public class JoinPartyFlowServiceImpl implements JoinPartyFlowService {

    private static final String joinPartyProcessName = "joinParty5";


    @Autowired
    private IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private StageService stageService;

    @Autowired
    private FileService fileService;

    @Override
    public void startProcess(String userId, String branchName) {
        // 检查是否年满18 是否为共青团员 党支部信息是否正确
        Map<String, Object> variables = new HashMap<>();

        Stage stage = stageService.getStageByUserId(userId);
        if (stage == null)
            throw new BizException(JoinPartyExceptionEnum.USER_ID_NOT_EXIST.getCode(), JoinPartyExceptionEnum.USER_ID_NOT_EXIST.getDescription());
        if (stage.getProcessId() != null && !stage.getProcessId().equals(""))
            throw new BizException(JoinPartyExceptionEnum.MULTIPLE_APPLICATION.getCode(), JoinPartyExceptionEnum.MULTIPLE_APPLICATION.getDescription());
        if (!StageCheckComponent.checkAdultAndLeagueMember(stage))
            throw new BizException(JoinPartyExceptionEnum.QUALIFICATIONS_FAIL.getCode(), JoinPartyExceptionEnum.QUALIFICATIONS_FAIL.getDescription());

        variables.put("userId", userId);

        Group group = identityService.createGroupQuery().groupName(branchName).singleResult();
        if (group == null)
            throw new BizException(JoinPartyExceptionEnum.BRANCH_GROUP_NOT_EXIST.getCode(), JoinPartyExceptionEnum.BRANCH_GROUP_NOT_EXIST.getDescription());

        variables.put("group", group.getId());

        // 党委
        Group partyGroup = identityService.createGroupQuery().groupName("党委").singleResult();
        if (partyGroup == null)
            throw new BizException(JoinPartyExceptionEnum.PARTY_GROUP_NOT_EXIST.getCode(), JoinPartyExceptionEnum.PARTY_GROUP_NOT_EXIST.getDescription());
        variables.put("partyGroup", partyGroup.getId());

        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(joinPartyProcessName, variables);
        System.out.println("processInstance = " + processInstance);


        stage.setProcessId(processInstance.getProcessInstanceId());
        stageService.updateStageByUserId(stage, userId);
    }

    @Override
    public void startUploadThoughtReport(String userId, Integer fileId) {
        Stage stage = stageService.getStageByUserId(userId);
        if (stage == null)
            throw new BizException(JoinPartyExceptionEnum.USER_ID_NOT_EXIST.getCode(), JoinPartyExceptionEnum.USER_ID_NOT_EXIST.getDescription());
        if (fileService.getFileById(fileId) == null)  // TODO 检查是否是由userId上传的思想汇报
            throw new BizException(JoinPartyExceptionEnum.FILE_NOT_EXIST.getCode(), JoinPartyExceptionEnum.FILE_NOT_EXIST.getDescription());
        if (!StageCheckComponent.checkUploadThoughtReportTime(stage))
            throw new BizException(JoinPartyExceptionEnum.UPLOAD_THOUGHT_REPORT_FAIL.getCode(), JoinPartyExceptionEnum.UPLOAD_THOUGHT_REPORT_FAIL.getDescription());

        stage.setThoughtReportCnt(stage.getThoughtReportCnt());
        stageService.updateStageByUserId(stage, userId);

        // 对flow中存储的值进行更新
        Execution execution = runtimeService.createExecutionQuery().processInstanceId(stage.getProcessId()).singleResult();
        runtimeService.setVariable(execution.getId(), "thoughtReportCnt", stage.getThoughtReportCnt());
    }

    @Override
    public void acceptActivist(String userId) {
        // TODO 权限验证
        runtimeService.signalEventReceived("activistSignal");
    }

    @Override
    public MyProcessTask queryMyCreateProcess(String userId) {
//        Task task = new TaskEntityImpl();


        Stage stage = stageService.getStageByUserId(userId);
        if (stage == null)
            throw new BizException(JoinPartyExceptionEnum.USER_ID_NOT_EXIST.getCode(), JoinPartyExceptionEnum.USER_ID_NOT_EXIST.getDescription());

//        return taskService.createTaskQuery().processInstanceId(stage.getProcessId()).active().list();
        Task task = taskService.createTaskQuery().processInstanceId(stage.getProcessId()).active().singleResult();

        return JoinPartyConvertMapper.INSTANCE.taskConvertMyProcessTask(task);
    }

    @Override
    public MyProcessTaskVO queryMyTODOProcess(Integer pageNumber, Integer pageSize, String groupName) {
//        return taskService.createTaskQuery().taskCandidateOrAssigned(userId).active().list();
        Group group = identityService.createGroupQuery().groupName(groupName).singleResult();
        if (group == null)
            throw new BizException(JoinPartyExceptionEnum.BRANCH_GROUP_NOT_EXIST.getCode(), JoinPartyExceptionEnum.BRANCH_GROUP_NOT_EXIST.getDescription());

//        return taskService.createTaskQuery().taskCandidateGroup(group.getId()).active().list();
        MyProcessTaskVO vo = new MyProcessTaskVO();
        List<Task> tasks = taskService.createTaskQuery().taskCandidateGroup(group.getId()).active().listPage((pageNumber-1) * pageSize, pageSize);

        vo.setTaskList(JoinPartyConvertMapper.INSTANCE.taskConvertMyProcessTask(tasks));
        vo.setPages((identityService.createGroupQuery().count() + pageSize - 1)/pageSize);
        vo.setCurrent(pageNumber.longValue());
        vo.setPageSize(pageSize.longValue());
        return vo;
    }

//    @Override
//    public void simpleCompleteTask(String processId, boolean accept) {
//        Task task = taskService.createTaskQuery().processInstanceId(processId).active().singleResult();
//        if (task != null) {
//            Map<String, Object> v = new HashMap<>();
//            v.put("accept", accept);
//
//            taskService.complete(task.getId(), v);
//        }
//    }

//    @Override
//    public void simpleCompleteTaskByTaskId(String taskId, boolean accept) {
//        Task task = taskService.createTaskQuery().taskId(taskId).active().singleResult();
//        if (task == null)
//            throw new BizException(FlowProcessExceptionEnum.TASK_NOT_EXIST.getCode(), FlowProcessExceptionEnum.TASK_NOT_EXIST.getDescription());
//
//        //  这个好像只能设置全局变量
////            taskService.setVariable(taskId, "accept", true);
//
//        // TODO 部分信息 例如日期、培养人之类的 需要先存到formService中 然后通过handler从process中拿到变量存到数据库中去
//
//        Map<String, Object> v = new HashMap<>();
//        v.put("accept", true);
//
//        taskService.complete(taskId,v);
//    }

//    @Override
//    public int completeTaskListByTaskId(JoinPartyTaskListParam param) {
//        if (param.getTaskIdList() == null || param.getTaskIdList().size() == 0)
//            throw new BizException(JoinPartyExceptionEnum.TASK_LIST_NOT_EXIST.getCode(), JoinPartyExceptionEnum.TASK_LIST_NOT_EXIST.getDescription());
//
//
//        AtomicInteger successCnt = new AtomicInteger();
//
//        param.getTaskIdList().forEach(taskId -> {
//            Task task = taskService.createTaskQuery().taskId(taskId).active().singleResult();
//            if (task == null) return;
//
//            Stage stage = stageService.getStageByProcessId(task.getProcessInstanceId());
//            // TODO 在单个的task中还是判断一下
//
//            FormInfo formInfo = taskService.getTaskFormModel(task.getId());
//            SimpleFormModel formModel = (SimpleFormModel) formInfo.getFormModel();
//            List<FormField> fields = formModel.getFields();
//
//            Map<String, Object> v = new HashMap<>();
//
//            for (FormField field : fields) {
//                if (field.getType().equals("boolean")) {
//                    v.put(field.getId(), param.isAccept());
//                } else if (field.getType().equals("date")) {
//                    if (param.getTime() == null) return;
//
//                    try {
//                        Field stageField = Stage.class.getDeclaredField(field.getId());
//                        stageField.setAccessible(true);
//                        stageField.set(stage, param.getTime());
//                    } catch (Exception e) {
//                        return;
//                    }
//                } else if (field.getType().equals("string1")) {
//                    if (param.getName1() == null) return;
//
//                    try {
//                        Field stageField = Stage.class.getDeclaredField(field.getId());
//                        stageField.setAccessible(true);
//                        stageField.set(stage, param.getName1());
//                    } catch (Exception e) {
//                        return;
//                    }
//                } else if (field.getType().equals("string2")) {
//                    if (param.getName2() == null && field.getPlaceholder().equals("true")) return;
//
//                    try {
//                        Field stageField = Stage.class.getDeclaredField(field.getId());
//                        stageField.setAccessible(true);
//                        stageField.set(stage, param.getName2());
//                    } catch (Exception e) {
//                        return;
//                    }
//                }
//            }
//
//            // TODO stage需要进行的修改统一在这里进行
//            stageService.updateStageByUserId(stage, stage.getUserNumber());
//
//            // complete可以增加每个任务的环境变量（就是一个任务关联一些独有变量），比如一个任务一个表单,scope 要设置为true(true为局部，false为全局 [ 默认 ])
//            taskService.complete(taskId, v, false);
//            successCnt.getAndIncrement();
//        });
//
//
//        return successCnt.get();
//    }

    @Override
    public void completeTaskByTaskId(JoinPartyTaskListParam param) {
        if (param.getTaskId() == null)
            throw new BizException(JoinPartyExceptionEnum.TASK_NOT_EXIST.getCode(), JoinPartyExceptionEnum.TASK_NOT_EXIST.getDescription());
        Task task = taskService.createTaskQuery().taskId(param.getTaskId()).active().singleResult();
        if (task == null)
            throw new BizException(JoinPartyExceptionEnum.TASK_NOT_EXIST.getCode(), JoinPartyExceptionEnum.TASK_NOT_EXIST.getDescription());

        Stage stage = stageService.getStageByProcessId(task.getProcessInstanceId());

        FormInfo formInfo = taskService.getTaskFormModel(task.getId());
        SimpleFormModel formModel = (SimpleFormModel) formInfo.getFormModel();
        List<FormField> fields = formModel.getFields();

        Map<String, Object> v = new HashMap<>();

        for (FormField field : fields) {
            if (field.getType().equals("boolean")) {
                v.put(field.getId(), param.isAccept());
            } else if (field.getType().equals("date")) {
                if (param.getTime() == null)
                    throw new BizException(JoinPartyExceptionEnum.PARAM_ERROR.getCode(), JoinPartyExceptionEnum.PARAM_ERROR.getDescription() + ": " + field.getId());

                try {
                    Field stageField = Stage.class.getDeclaredField(field.getId());
                    stageField.setAccessible(true);
                    stageField.set(stage, param.getTime());
                } catch (Exception e) {
                    throw new BizException(JoinPartyExceptionEnum.PARAM_ERROR.getCode(), JoinPartyExceptionEnum.PARAM_ERROR.getDescription() + ": " + field.getId());
                }
            } else if (field.getType().equals("string1")) {
                if (param.getName1() == null)
                    throw new BizException(JoinPartyExceptionEnum.PARAM_ERROR.getCode(), JoinPartyExceptionEnum.PARAM_ERROR.getDescription() + ": " + field.getId());

                try {
                    Field stageField = Stage.class.getDeclaredField(field.getId());
                    stageField.setAccessible(true);
                    stageField.set(stage, param.getName1());
                } catch (Exception e) {
                    throw new BizException(JoinPartyExceptionEnum.PARAM_ERROR.getCode(), JoinPartyExceptionEnum.PARAM_ERROR.getDescription() + ": " + field.getId());
                }
            } else if (field.getType().equals("string2")) {
                if (param.getName2() == null && field.getPlaceholder().equals("true"))
                    throw new BizException(JoinPartyExceptionEnum.PARAM_ERROR.getCode(), JoinPartyExceptionEnum.PARAM_ERROR.getDescription() + ": " + field.getId());

                try {
                    Field stageField = Stage.class.getDeclaredField(field.getId());
                    stageField.setAccessible(true);
                    stageField.set(stage, param.getName2());
                } catch (Exception e) {
                    throw new BizException(JoinPartyExceptionEnum.PARAM_ERROR.getCode(), JoinPartyExceptionEnum.PARAM_ERROR.getDescription() + ": " + field.getId());
                }
            }
        }

        // TODO stage需要进行的修改统一在这里进行
        stageService.updateStageByUserId(stage, stage.getUserNumber());

        // complete可以增加每个任务的环境变量（就是一个任务关联一些独有变量），比如一个任务一个表单,scope 要设置为true(true为局部，false为全局 [ 默认 ])
        taskService.complete(param.getTaskId(), v);
    }

    @Override
    public void simpleDeleteProcess(String processId, String reason) {
        runtimeService.deleteProcessInstance(processId, reason);
    }


    @Override
    public Integer queryFileIdByTaskId(String taskId) {
        Task task = taskService.createTaskQuery().taskId(taskId).active().singleResult();
        if (task == null)
            throw new BizException(JoinPartyExceptionEnum.TASK_NOT_EXIST.getCode(), JoinPartyExceptionEnum.TASK_NOT_EXIST.getDescription());
        Integer fileId = (Integer) taskService.getVariable(taskId, "fileId");
        if (fileId == null)
            throw new BizException(JoinPartyExceptionEnum.FILE_NOT_EXIST.getCode(), JoinPartyExceptionEnum.FILE_NOT_EXIST.getDescription());
        return fileId;
    }

//    @Override
//    public void startUploadThoughtReport(String userNumber, String branchName, Integer fileId) {
//        Stage stage = stageService.getStageByUserId(userNumber);
//        if (stage == null)
//            throw new BizException(FlowProcessExceptionEnum.USER_ID_NOT_EXIST.getCode(), FlowProcessExceptionEnum.USER_ID_NOT_EXIST.getDescription());
//
//        Date date = stage.getNxtThoughtReportUploadTime();
//        Date currentDate = new Date();
//        if (currentDate.compareTo(date) < 0)
//            throw new BizException();
//
//        Map<String, Object> variables = new HashMap<>();
//        variables.put("userId", stage.getUserNumber());
//        variables.put("fileId", fileId);
//
//        Group group = identityService.createGroupQuery().groupName(branchName).singleResult();
//        if (group == null)
//            throw new BizException(FlowProcessExceptionEnum.BRANCH_GROUP_NOT_EXIST.getCode(), FlowProcessExceptionEnum.BRANCH_GROUP_NOT_EXIST.getDescription());
//
//        variables.put("group", group.getId());
//
//        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(thoughtReportProcessName, variables);
//        System.out.println("processInstance = " + processInstance);
//    }
}
