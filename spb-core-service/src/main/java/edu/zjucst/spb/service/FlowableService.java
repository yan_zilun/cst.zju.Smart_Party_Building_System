package edu.zjucst.spb.service;

import liquibase.pro.packaged.S;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;

import java.util.List;
import java.util.Map;

/**
 * @author yzl
 **/
public interface FlowableService {

    /**
     * 完成用户任务
     * @param taskId 任务id
     * @param taskParam 任务相关参数
     */
    void completeTask(String taskId, Map<String, Object> taskParam);

    /**
     * 部署流程定义
     * @param file 流程定义文件名（xml文件）
     * @param name 流程定义名称
     */
    void deployProcessDefinition(String file, String name);

    /**
     * 删除流程定义
     * @param id 流程定义id
     * @param cascade 是否级联删除（若级联删除，则会把该流程定义下的流程实例也删除）
     */
    void deleteProcessDefinition(String id, boolean cascade);

    /**
     * 启动流程实例
     * @param processKey 流程定义的key
     * @param userId 发起人
     * @param startParam 启动参数
     */
    void startProcessInstance(String processKey, String userId, Map<String, Object> startParam);

    /**
     * 根据用户id查找它发起的流程实例
     * @param userId 用户id
     * @return 流程实例列表，元素为流程实例的id
     */
    List<String> queryProcessInstanceByOriginator(String userId);

    /**
     * 根据用户id查找需要它审批的任务。（先查找用户所在的用户组，再查找用户组的任务）
     * @param userId 用户id
     * @return 任务列表，元素为任务id
     */
    List<String> queryTaskByUserId(String userId);

    /**
     * 根据用户组id查找需要它审批的任务
     * @param groupId 用户组id
     * @return 任务列表，元素为任务id
     */
    List<String> queryTaskByGroupId(String groupId);

    /**
     * 根据流程实例id查找任务
     * @param processInstanceId 流程实例id
     * @return 任务列表，元素为任务id
     */
    List<String> queryTaskByProcessInstanceId(String processInstanceId);

    /**
     * 创建用户
     * @param id 用户id
     * @param displayName 用户名
     */
    void createIDMUser(String id, String displayName);

    /**
     * 删除用户
     * @param id 用户id
     */
    void deleteIDMUser(String id);

    /**
     * 创建用户组
     * @param groupName 用户组组名（例如：第一党支部、第二党支部）
     * @return group在flowable中存储的id
     */
    String createIDMGroup(String groupName);

    /**
     * 删除用户组
     * @param id 用户组id
     */
    void deleteIDMGroup(String id);

    /**
     * 将用户加入用户组
     * @param userid 用户id
     * @param groupid 用户组id
     */
    void relateIDMUser2Group(String userid, String groupid);

    /**
     * 将用户移出用户组
     * @param userid 用户id
     * @param groupid 用户组id
     */
    void unRelateIDMUser2Group(String userid, String groupid);

    /**
     * 查询流程实例的流程变量
     * @param processInstanceId 流程实例id
     * @param variableName 流程变量名
     * @return 查询的值
     */
    Object queryVariableValue(String processInstanceId, String variableName);

    /**
     * 修改流程实例的流程变量
     * @param processInstanceId 流程实例id
     * @param variableName 流程变量名
     * @param value 修改的值
     */
    void setVariableValue(String processInstanceId, String variableName, Object value);

    /**
     * 给任务增加一个审批用户组。不检测重复，即可能添加重复的关联记录，需要避免。
     * @param taskId 任务id
     * @param groupId 用户组id
     */
    void addCandidateGroup(String taskId, String groupId);

    /**
     * 给任务删除一个审批用户组。
     * @param taskId 任务id
     * @param groupId 用户组id
     */
    void deleteCandidateGroup(String taskId, String groupId);

    /**
     * 根据任务id查询该任务的所有审批用户组
     * @param taskId 任务id
     * @return 用户组列表，元素为用户组id
     */
    List<String> queryTaskCandidateGroup(String taskId);
}
