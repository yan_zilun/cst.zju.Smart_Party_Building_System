package edu.zjucst.spb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.param.user.LoginParam;
import edu.zjucst.spb.domain.param.user.SignUpParam;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.vo.branch.MemberCountVO;
import edu.zjucst.spb.domain.vo.user.LoginVO;
import org.springframework.stereotype.Service;

/**
 * 用户接口
 *
 * @author qingo
 * @date 2022/12/22 16:49
 * @wiki(不存在可无) 所述模块在语雀上的流程url
 */
@Service
public interface UserService extends IService<SpbUser> {
    ResponseResult<Object> signUp(SignUpParam dto);

    ResponseResult<LoginVO> login(LoginParam loginParam);
    //统计支部的党员和预备党员人数
    MemberCountVO countPartyMember(String name);
}
