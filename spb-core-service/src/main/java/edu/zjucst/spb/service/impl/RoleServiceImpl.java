package edu.zjucst.spb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import edu.zjucst.spb.dao.role.RoleDao;
import edu.zjucst.spb.domain.entity.Role;
import edu.zjucst.spb.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleDao roleDao;

    @Override
    @Transactional
    public List<Role> findRole(Role role) {
//        SelectStatementProvider selectStatement = SqlBuilder.select(roleMapper.selectList)
//            .from(tbRole)
//            .build()
//            .render(RenderingStrategies.MYBATIS3);
//
//        return roleMapper.selectMany(selectStatement);
        // and 查询 改为：id不为空，id查询，roleName不为空，roleName查询
        return roleDao.selectList(new LambdaQueryWrapper<Role>()
            .eq(!ObjectUtils.isEmpty(role.getRoleId()), Role::getRoleId, role.getRoleId())
            .eq(!ObjectUtils.isEmpty(role.getRoleName()), Role::getRoleName, role.getRoleName())
        );
    }

    // 根据权限ID查询权限
    @Override
    public Role findRoleById(Long Id) {
        return roleDao.selectOne(new LambdaQueryWrapper<Role>()
            .eq(Role::getRoleId, Id)
        );
    }

    // 根据权限名查询权限
    public Role findRoleByName(String roleName) {
        return roleDao.selectOne(new LambdaQueryWrapper<Role>()
            .eq(Role::getRoleName, roleName)
        );
    }
}
