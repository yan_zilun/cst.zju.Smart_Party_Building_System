package edu.zjucst.spb.service.message.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.zjucst.spb.dao.message.SystemMessageDao;
import edu.zjucst.spb.domain.entity.message.SystemMessage;
import edu.zjucst.spb.service.message.SystemMessageService;
import org.springframework.stereotype.Service;

/**
 * (SystemMessage)表服务实现类
 *
 * @author Mrli
 * @since 2023-04-05 13:34:54
 */
@Service("systemMessageService")
public class SystemMessageServiceImpl extends ServiceImpl<SystemMessageDao, SystemMessage> implements SystemMessageService {

}

