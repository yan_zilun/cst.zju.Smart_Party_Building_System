package edu.zjucst.spb.service;

import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.TbFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 文件service的接口
 * @author zxz
 */
public interface FileService {

    ResponseResult<Object> upload(MultipartFile file, Integer privilege);
    List<TbFile> listPublicFiles();
    TbFile getFileById(int id);
}
