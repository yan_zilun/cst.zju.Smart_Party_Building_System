package edu.zjucst.spb.service.message;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import edu.zjucst.spb.domain.entity.message.UserMessageText;
import edu.zjucst.spb.domain.param.message.GetUserMessageParam;
import edu.zjucst.spb.domain.param.message.MessageQueryParam;
import edu.zjucst.spb.domain.param.selfstudy.GetSelfStudyParam;
import edu.zjucst.spb.domain.vo.MySelfStudyVO;
import edu.zjucst.spb.domain.vo.message.UserMessageVO;
import edu.zjucst.spb.domain.vo.message.UserNotifyMessageVO;

import java.time.LocalDateTime;
import java.util.List;


/**
 * (UserMessage)UserMessage表的服务接口
 *
 * @author Mrli
 * @since 2023-04-05 14:17:20
 */
public interface UserMessageService extends IService<UserMessage> {

    /**
     * 创建一条需要定时推送的预设模板消息
     * @param messageId 预设消息的id
     */
    boolean sendPresetMessage(Long sendUserId, Long recvUserId, Long messageId, LocalDateTime dateTime);

    /**
     * 立马发送模板消息
     * @param sendUserId
     * @param recvUserId
     * @param messageId
     * @return
     */
    boolean sendPresetMessageRightNow(Long sendUserId, Long recvUserId, Long messageId);

    /**
     * 创建一条用户推送信息
     *
     * @param actorId 发送消息人的ID
     * @param recvId  接收人的ID
     */
    ResponseResult sendMessage(Long sendUserId, Long recvUserId, String content);

    ResponseResult sendMessageMessageToGroup(Long sendUserId, List<Long> userList, String content);

    List<Long> updateBatchByIdList(Long sendUserId, List<Long> idList);

    /**
     * 分页查询信息
     */
//    PageInfo<UserNotifyMessageVO> queryMessageByPage(Long recvUserId, MessageQueryParam messageQueryParam);

    /**
     * 查询信息列表
     */
//    List<UserMessageVO> getUserMessageList(GetUserMessageParam dto);
    List<UserMessage> getUserMessageList(GetUserMessageParam dto);

    /**
     * 查询信息内容列表
     */
//    List<UserMessageVO> getUserMessageList(GetUserMessageParam dto);
    List<UserMessageText> getUserMessageTextList(GetUserMessageParam dto);

//    获取当前用户全部未读消息
    List<UserMessage> getAllNOReadMessage(Long recvUserId);
}

