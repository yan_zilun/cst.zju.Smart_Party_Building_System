package edu.zjucst.spb.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import edu.zjucst.spb.domain.entity.Activity;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.entity.message.SystemMessage;
import edu.zjucst.spb.domain.vo.UserVo;

import java.util.List;




public interface ActivityService extends IService<Activity> {

    /**
     * 插入活动列表
     * @param activityList 需要插入的活动列表
     * @return  成功插入个数
     */
    int insert(List<Activity> activityList);

    /**
     *根据发展阶段 时间范围  活动编号三个阶段筛选活动列表
     * @param developmentPhase 发展阶段
     * @param days  一个月 半年 一年内  单位月
     * @param activityNumber  活动编号
     * @return 符合条件的活动列表
     */
    IPage<Activity> selectActivityList(Page<Activity> page,String developmentPhase, Integer days, String activityNumber);

    /**
     *根据活动编号，需要修改状态的用户列表，状态更新活动
     * @param activityNumber 活动编号
     * @param userList 用户列表
     * @param state 状态
     * @return 更新记录条数
     */
    int update(String activityNumber, List<UserVo> userList, String state);

    /**
     *删除给定活动编号的所有活动
     * @param activityNumber 活动编号
     * @return 删除记录条数
     */
    int delete(String activityNumber);

    /**
     * 根据activityNumber判断当前活动是否存在
     * @param activityNumber
     * @return
     */
    boolean hasExistedActivityNumber(String activityNumber);

    /**
     * 根据当天日期自动生成不重复的活动编号
     * @return 活动编号
     */
    String selectActivityNumber();
}
