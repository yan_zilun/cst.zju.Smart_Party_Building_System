//package edu.zjucst.spb.service.impl;
//
//import edu.zjucst.spb.domain.entity.Stage;
//import edu.zjucst.spb.enums.FlowProcessExceptionEnum;
//import edu.zjucst.spb.exception.BizException;
//import edu.zjucst.spb.pkg.flow.stage.StageCheckComponent;
//import edu.zjucst.spb.service.BranchService;
//import edu.zjucst.spb.service.FlowProcessService;
//import edu.zjucst.spb.service.StageService;
//import org.flowable.engine.IdentityService;
//import org.flowable.engine.RuntimeService;
//import org.flowable.engine.TaskService;
//import org.flowable.engine.runtime.ProcessInstance;
//import org.flowable.idm.api.Group;
//import org.flowable.task.api.Task;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
///**
// * @author ZlunYan
// * @description
// * @create 2023-11-28
// */
//
//@Service
//public class FlowProcessServiceImpl implements FlowProcessService {
//
//
//    @Autowired
//    private IdentityService identityService;
//
//
//    @Autowired
//    private RuntimeService runtimeService;
//
//    @Autowired
//    private TaskService taskService;
//
//    @Autowired
//    private StageService stageService;
//
//    @Autowired
//    private BranchService branchService;
//
//    @Override
//    public void startProcess(String processName, String userId, String branchName) {
//        // 检查是否年满18 是否为共青团员 党支部信息是否正确
//        Map<String, Object> variables = new HashMap<>();
//
//        Stage stage = stageService.getStageByUserId(userId);
//        if (stage == null)
//            throw new BizException(FlowProcessExceptionEnum.USER_ID_NOT_EXIST.getCode(), FlowProcessExceptionEnum.USER_ID_NOT_EXIST.getDescription());
//        if (!StageCheckComponent.checkAdultAndLeagueMember(stage))
//            throw new BizException(FlowProcessExceptionEnum.QUALIFICATIONS_FAIL.getCode(), FlowProcessExceptionEnum.QUALIFICATIONS_FAIL.getDescription());
//
//        variables.put("userId", userId);
//        // TODO 接入党支部信息
////        variables.put("branchId", branchService.getSecretaryId("test"));
//
//        Group group = identityService.createGroupQuery().groupName(branchName).singleResult();
//        if (group == null)
//            throw new BizException(FlowProcessExceptionEnum.BRANCH_GROUP_NOT_EXIST.getCode(), FlowProcessExceptionEnum.BRANCH_GROUP_NOT_EXIST.getDescription());
//
//        variables.put("group", group.getId());
//
//        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processName, variables);
//        System.out.println("processInstance = " + processInstance);
//
//
//        stage.setProcessId(processInstance.getProcessInstanceId());
//        stageService.updateStageByUserId(stage, userId);
//    }
//
//    @Override
//    public List<Task> queryMyCreateProcess(String userId) {
//        Stage stage = stageService.getStageByUserId(userId);
//        return taskService.createTaskQuery().processInstanceId(stage.getProcessId()).active().list();
//    }
//
//    @Override
//    public List<Task> queryMyTODOProcess(String groupId) {
////        return taskService.createTaskQuery().taskCandidateOrAssigned(userId).active().list();
//        return taskService.createTaskQuery().taskCandidateGroup(groupId).active().list();
//    }
//
//    @Override
//    public void simpleCompleteTask(String taskId, boolean accept) {
//        Task task = taskService.createTaskQuery().processInstanceId(taskId).active().singleResult();
//        if (task != null) {
//            Map<String, Object> v = new HashMap<>();
//            v.put("accept", true);
//
//            taskService.complete(task.getId(), v);
//        }
//    }
//
//    @Override
//    public void simpleCompleteTaskByTaskId(String taskId, boolean accept) {
//        Task task = taskService.createTaskQuery().taskId(taskId).active().singleResult();
//        if (task != null) {
//            Map<String, Object> v = new HashMap<>();
//            v.put("accept", true);
//
//            taskService.complete(task.getId(), v);
//        }
//    }
//
//    @Override
//    public void simpleDeleteProcess(String processId, String reason) {
//        runtimeService.deleteProcessInstance(processId, reason);
//    }
//}
