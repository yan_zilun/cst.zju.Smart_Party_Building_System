package edu.zjucst.spb.service.joinParty;

import edu.zjucst.spb.domain.entity.flow.joinParty.MyProcessTask;
import edu.zjucst.spb.domain.param.stage.joinParty.JoinPartyTaskListParam;
import edu.zjucst.spb.domain.vo.flow.joinParty.MyProcessTaskVO;

public interface JoinPartyFlowService {

    void startProcess(String userId, String branchName);

    void startUploadThoughtReport(String userId, Integer fileId);

    void acceptActivist(String userId);

    MyProcessTask queryMyCreateProcess(String userId);

    MyProcessTaskVO queryMyTODOProcess(Integer pageNumber, Integer pageSize, String groupName);

//    void simpleCompleteTask(String processId, boolean accept);

//    void simpleCompleteTaskByTaskId(String taskId, boolean accept);

//    int completeTaskListByTaskId(JoinPartyTaskListParam param);

    void completeTaskByTaskId(JoinPartyTaskListParam param);


    void simpleDeleteProcess(String processId, String reason);

    Integer queryFileIdByTaskId(String taskId);
}
