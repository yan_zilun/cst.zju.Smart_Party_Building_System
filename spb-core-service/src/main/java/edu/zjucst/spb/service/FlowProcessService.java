//package edu.zjucst.spb.service;
//
//import org.flowable.task.api.Task;
//
//import java.util.List;
//
//public interface FlowProcessService {
//
//    void startProcess(String processName, String userId, String branchName);
//
//    List<Task> queryMyCreateProcess(String userId);
//
//    List<Task> queryMyTODOProcess(String groupId);
//
//    void simpleCompleteTask(String taskId, boolean accept);
//
//    void simpleCompleteTaskByTaskId(String taskId, boolean accept);
//
//    void simpleDeleteProcess(String processId, String reason);
//}
