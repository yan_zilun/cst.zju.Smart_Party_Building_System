package edu.zjucst.spb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Activity;
import edu.zjucst.spb.domain.entity.SpbUser;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SpbUserService {

    int addUser(SpbUser spbUser);

    ResponseResult<Object> deleteUser(Long userId);

    ResponseResult<Object> deleteUsers(List<Long> spbUserList);

    ResponseResult<Object> updateUser(SpbUser spbUser);

    ResponseResult<Object> updateUsers(List<SpbUser> spbUserList);

    /**
     * 根据查询条件查询信息
     *
     * @return
     */
    List<SpbUser> findUser(SpbUser spbUser);

    /**
     * 根据用户学工号查询用户
     * @param userNumber 学工号
     * @return 用户
     */
    SpbUser findUserByNumber(String userNumber);

    /**
     * 根据阶段查询该阶段用户列表
     * @param developmentPhase 发展阶段
     * @return 用户列表
     */
    IPage<SpbUser> selectByPhase(Page<SpbUser> page, String partyBranch, String developmentPhase);


    /**
     * 查询给定活动 给定参与、提交状态 的用户列表
     * @param activityNumber 活动编号
     * @param state 参与、提交状态
     * @return 用户列表
     */
    IPage<SpbUser> selectByState(Page<Activity> page, String partyBranch, String activityNumber, String state);

    /**
     * 查询给定权限的用户列表
     * @param roleName 权限名称
     * @return 用户列表
     */
    IPage<SpbUser> selectByRoleName(Page<SpbUser> page,String partyBranch,String roleName);

    /**
     * 查询用户的党员发展阶段
     *
     * @param userNumber 学号/工号
     */
}
