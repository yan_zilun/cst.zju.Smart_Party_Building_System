package edu.zjucst.spb.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.zjucst.spb.dao.mapper.SelfActivityMapper;
import edu.zjucst.spb.dao.mapper.SelfStudyMapper;
import edu.zjucst.spb.dao.selfstudy.SelfStudyDao;
import edu.zjucst.spb.dao.user.UserDao;
import edu.zjucst.spb.service.SpbUserService;
import edu.zjucst.spb.domain.entity.SelfActivity;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.param.activity.AddBranchActivityParam;
import edu.zjucst.spb.domain.param.activity.AddSelfActivityParam;
import edu.zjucst.spb.domain.param.activity.GetSelfActivityParam;
import edu.zjucst.spb.domain.entity.SelfStudy;
import edu.zjucst.spb.domain.param.selfstudy.GetSelfStudyParam;
import edu.zjucst.spb.domain.vo.MySelfStudyVO;
import edu.zjucst.spb.service.SelfStudyService;
import edu.zjucst.spb.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.HashMap;
import java.util.Map;

/**
 * 自主学习相关服务实现类
 *
 * @author 江中华
 * @since 2024-03-11
 */
@Service
public class SelfStudyServiceImpl extends ServiceImpl<SelfStudyMapper, SelfStudy> implements SelfStudyService {
    @Autowired
    private SelfStudyMapper selfStudyMapper;

    @Autowired
    private SpbUserService spbUserService;

    @Override
    public List<MySelfStudyVO> getMySelfStudyByPage(GetSelfStudyParam dto) {
        QueryWrapper<SelfStudy> queryWrapper = new QueryWrapper<SelfStudy>();

        if (dto.getDevelopmentPhaseID() != null) {
            queryWrapper.eq("development_phase_id", dto.getDevelopmentPhaseID());
        }
        //目前采取前端传入用户学工号，也可从用户token中取用户学工号
//        Long userId = SessionUtil.getUserId();
//        String userNumber = userDao.selectById(userId).getUserNumber();
//        queryWrapper.eq("user_number",userNumber);
        //TODO 分页
        List<SelfStudy> selfStudies = selfStudyMapper.selectList(queryWrapper);
        return selfStudies.stream().map((x) -> {
            MySelfStudyVO vo = new MySelfStudyVO();
            BeanUtil.copyProperties(x, vo);
            return vo;
        }).collect(Collectors.toList());
    }

    public List<MySelfStudyVO> getMySelfStudyByDevelopmentPhaseID(String userNumber, Integer developmentPhaseID){
        QueryWrapper<SelfStudy> queryWrapper = new QueryWrapper<SelfStudy>();
        queryWrapper.eq("development_phase_id", developmentPhaseID);
        queryWrapper.eq("user_number", userNumber);
        List<SelfStudy> selfStudies = selfStudyMapper.selectList(queryWrapper);
        return selfStudies.stream().map((x) -> {
            MySelfStudyVO vo = new MySelfStudyVO();
            BeanUtil.copyProperties(x, vo);
            return vo;
        }).collect(Collectors.toList());
    }

    public Integer getAllStudyHour(String userNumber){
        QueryWrapper<SelfStudy> queryWrapper = new QueryWrapper<SelfStudy>();
        queryWrapper.eq("user_number", userNumber);
        List<SelfStudy> selfStudies = selfStudyMapper.selectList(queryWrapper);

        Integer totalHours = 0;
        for (SelfStudy selfStudy : selfStudies) {
            totalHours += selfStudy.getAppliedStudyHour();
        }
        return totalHours;
    }

    public Integer getAllStudyHourNowDevelopmentPhase(String userNumber){
        Map<String, Integer> developmentPhaseToId = new HashMap<>();
        developmentPhaseToId.put("未确认发展阶段", 0);
        developmentPhaseToId.put("入党申请人", 1);
        developmentPhaseToId.put("积极分子", 2);
        developmentPhaseToId.put("发展对象", 3);
        developmentPhaseToId.put("预备党员", 4);
        developmentPhaseToId.put("党员", 5);

        SpbUser user = spbUserService.findUserByNumber(userNumber);
        String developmentPhase = user.getDevelopmentPhase();
        Integer developmentPhaseId = developmentPhaseToId.get(developmentPhase);

        QueryWrapper<SelfStudy> queryWrapper = new QueryWrapper<SelfStudy>();
        queryWrapper.eq("user_number", userNumber).eq("development_phase_id", developmentPhaseId);
        List<SelfStudy> selfStudies = selfStudyMapper.selectList(queryWrapper);

        Integer totalHours = 0;
        for (SelfStudy selfStudy : selfStudies) {
            totalHours += selfStudy.getAppliedStudyHour();
        }
        return totalHours;
    }
}
