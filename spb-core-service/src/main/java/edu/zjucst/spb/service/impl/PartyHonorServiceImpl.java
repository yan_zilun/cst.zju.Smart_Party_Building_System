package edu.zjucst.spb.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import edu.zjucst.spb.dao.mapper.PartyHonorMapper;
import edu.zjucst.spb.domain.entity.BranchActivity;
import edu.zjucst.spb.domain.entity.PartyHonor;
import edu.zjucst.spb.service.PartyHonorService;
import edu.zjucst.spb.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 18:23
 */
@Service
public class PartyHonorServiceImpl implements PartyHonorService {
    @Autowired
    PartyHonorMapper partyHonorMapper;
    @Override
    public JSONObject listPartyHonor() {
        Long userId = SessionUtil.getUserId();
        JSONObject resp = new JSONObject();
        if(userId == null){
            resp.put("error_message", "用户不存在或未登录");
            return resp;
        }
        QueryWrapper<PartyHonor> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        List<PartyHonor> partyHonors = partyHonorMapper.selectList(queryWrapper);
        resp.put("honors", partyHonors);
        resp.put("error_message", "success");
        return resp;
    }
}
