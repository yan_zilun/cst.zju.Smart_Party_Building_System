package edu.zjucst.spb.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.domain.entity.Stage;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-28
 */
public interface StageService {
    int insertStage(Stage stage);

    Stage getStageByUserId(String userId);

    Stage getStageByProcessId(String processId);

    IPage<Stage> selectStageList(Page<Stage> page, Integer developId, String userName, String userNumber,
                                 Integer dateType, Date startTime, Date endTime);

    void updateStageByUserId(Stage stage, String userId);
//
//    List<Stage> getStageByDateRange(String dateCol, Date startTime, Date endTime) throws ParseException;
//
//    List<Stage> getStageByDateStrRange(String dateCol, String startStr, String endStr) throws ParseException;
}


//
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import edu.zjucst.spb.domain.entity.Stage;
//
//import java.util.Date;
//import java.util.List;
//
//public interface StageService {
//    /**
//     *根据发展阶段，用户名称，用户学工号信息筛选阶段列表
//     * @param developmentPhase 发展阶段
//     * @param userName 用户名称
//     * @param userNumber 用户学工号
//     * @return 阶段信息列表
//     */
//    IPage<Stage> selectStageList(Page<Stage> page, String developmentPhase, String userName, String userNumber,
//                                 Integer queryByDate, Date startTime, Date endTime);
//
//    /**
//     *根据stage id 更新该stage信息
//     * @param stage 阶段信息
//     * @return 更新记录条数
//     */
//    int updateStage(Stage stage);
//
//    /**
//     *插入单个stage
//     * @param stage 阶段信息
//     * @return 插入记录条数
//     */
//    int insert(Stage stage);
//
//    /**
//     *根据用户学工号选择该用户阶段信息
//     * @param userNumber 学工号
//     * @return 阶段信息
//     */
//    Stage selectStageByNumber(String userNumber);
//
//    /**
//     *编辑阶段列表
//     * @param stageList 阶段列表
//     * @return 插入或者更新记录条数
//     */
//    int editStageList(List<Stage> stageList);
//
//    /**
//     * 更新阶段列表 开放给支部书记、团委、党委
//     * @param stageList 阶段列表
//     * @return 更新记录条数
//     */
//    int updateStageList(List<Stage> stageList);
//
//    /**
//     * 插入阶段列表 团委、党委
//     * @param stageList 阶段列表
//     * @return 插入记录条数
//     */
//    int insertStageList(List<Stage> stageList);
//
//    /**
//     *删除阶段列表
//     * @param userNumber 需要删除阶段信息的用户学工号列表
//     * @return 删除记录条数
//     */
//    int deleteStageList(List<String> userNumber);
//}
