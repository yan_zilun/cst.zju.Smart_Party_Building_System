package edu.zjucst.spb.service.stage;

import edu.zjucst.spb.domain.entity.stage.ApplicantStage;

public interface ApplicantStageService extends BaseStageService<ApplicantStage> {

}
