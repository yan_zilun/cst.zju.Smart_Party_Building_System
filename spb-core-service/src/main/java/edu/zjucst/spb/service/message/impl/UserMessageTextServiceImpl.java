package edu.zjucst.spb.service.message.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.zjucst.spb.dao.message.UserMessageTextDao;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import edu.zjucst.spb.domain.entity.message.UserMessageText;
import edu.zjucst.spb.dao.mapper.UserMessageTextMapper;
import edu.zjucst.spb.domain.param.message.GetUserMessageParam;
import edu.zjucst.spb.service.message.UserMessageTextService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * (UserMessageText)表服务实现类
 *
 * @author Mrli
 * @since 2023-04-05 14:10:48
 */
@Service("userMessageTextService")
public class UserMessageTextServiceImpl extends ServiceImpl<UserMessageTextDao, UserMessageText> implements UserMessageTextService {

    @Resource
    private UserMessageTextMapper userMessageTextMapper;

    public List<UserMessageText> getUserMessageTextList(List<Integer> ids){
        QueryWrapper<UserMessageText> queryWrapperText = new QueryWrapper<UserMessageText>();

        queryWrapperText.in("id", ids);
//        queryWrapperTest.eq("id", userMessages.get(0).getMessageId());

        List<UserMessageText> userMessageTexts = userMessageTextMapper.selectList(queryWrapperText);

        return userMessageTexts;
    }

}

