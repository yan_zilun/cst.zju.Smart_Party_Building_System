package edu.zjucst.spb.service.message;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.zjucst.spb.domain.entity.message.SystemMessage;

/**
 * (SystemMessage)表服务接口
 *
 * @author Mrli
 * @since 2023-04-05 13:34:54
 */
public interface SystemMessageService extends IService<SystemMessage> {

}

