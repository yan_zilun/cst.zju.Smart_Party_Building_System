package edu.zjucst.spb.service;

import edu.zjucst.spb.domain.entity.Branch;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BranchService {
    int addBranch(Branch branch);

    int deleteBranch(Long branchId);

    int deleteBranches(List<Long> idList);

    String updateBranch(Branch branch);

    int updateBranches(List<Branch> branchList);

    List<Branch> findBranch(Branch branch);

    Branch findBranchById(Long Id);

    List<Branch> findBranchByName(String name);

//    查询全部支部信息
    List<Branch> findAllBranch();
}
