package edu.zjucst.spb.service.stage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

public interface BaseStageService<T> {

    int insertStage(T stage);

    T getStageByUserId(String userId);

    void updateStageByUserId(T stage, String userId);

    void updateStage(T stage);

    IPage<T> selectStageList(Page<T> page, QueryWrapper<T> wrapper);
}
