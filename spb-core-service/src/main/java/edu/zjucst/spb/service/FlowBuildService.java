package edu.zjucst.spb.service;

import edu.zjucst.spb.domain.entity.flow.FlowGroup;
import edu.zjucst.spb.domain.entity.flow.FlowUser;
import edu.zjucst.spb.domain.vo.flow.FlowGroupMemberQueryVO;
import edu.zjucst.spb.domain.vo.flow.FlowGroupQueryVO;
import org.flowable.engine.repository.Deployment;
import org.flowable.form.api.FormDeployment;

import java.util.List;

public interface FlowBuildService {

    Integer addUser(FlowUser flowUser);

    /**
     * 暂时先用UUID作为id
     * @param groupName 组名
     * @return 返回生成的组Id
     */
    Integer createGroup(String groupName);

    String queryGroupIdByGroupName(String groupName);

    FlowGroupMemberQueryVO queryGroupMemberByGroupName(Integer pageNumber, Integer pageSize, String groupName);

    void deleteUserByUserId(String userId);

    void deleteGroupByGroupId(String groupId);


    void deleteGroupByGroupName(String groupName);

    void createMembership(String userId, String groupId);

    Deployment createDeployment(String filePath, String flowName);

    FormDeployment createForm(String filePath, String formName, String deployId);

    FlowGroupQueryVO queryAllGroup(Integer pageNumber, Integer pageSize);

    Integer createUserMemberShip(String userId, String groupName);

    Integer deleteUserMemberShip(String userId, String groupName);
}
