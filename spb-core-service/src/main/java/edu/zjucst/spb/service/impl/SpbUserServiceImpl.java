package edu.zjucst.spb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.dao.mapper.ActivityMapper;
import edu.zjucst.spb.dao.spbuser.SpbUserDao;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Activity;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.enums.OptStatusEnum;
import edu.zjucst.spb.enums.ResponseStatusEnum;
import edu.zjucst.spb.service.SpbUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.zjucst.spb.util.SessionUtil;

import static edu.zjucst.spb.constant.RoleConstant.*;
import static edu.zjucst.spb.enums.ResponseStatusEnum.AUTHENTICATION_ERROR;

import java.lang.NullPointerException;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class SpbUserServiceImpl implements SpbUserService {

    @Autowired
    private SpbUserDao spbUserDao;


    @Autowired
    private ActivityMapper activityMapper;

    @Override
    @Transactional
    public int addUser(SpbUser spbUser) {
        return spbUserDao.insert(spbUser);
    }

    @Override
    @Transactional
    public ResponseResult<Object> deleteUser(Long userId) {
        Long selfId = SessionUtil.getUserId();
        if (selfId == null){
            return ResponseResult.error(AUTHENTICATION_ERROR.getCode(), AUTHENTICATION_ERROR.getDescription());
        }
        String role_name = spbUserDao.selectById(selfId).getRoleName();
        if (ADMINISTRATOR.equals(role_name)) {
            try{
                spbUserDao.deleteById(userId);
            } catch (Exception e){
                if (e instanceof NullPointerException) {
                    return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "删除的用户不存在");
                } else{
                    return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "未知错误");
                }
            }
        }else {
            return ResponseResult.error(ResponseStatusEnum.AUTHORIZATION_FORBIDDEN.getCode(), ResponseStatusEnum.AUTHORIZATION_FORBIDDEN.getDescription());
        }
        return ResponseResult.success();
    }


    @Override
    @Transactional
    public ResponseResult<Object> deleteUsers(List<Long> spbUserList) {
        Long selfId = SessionUtil.getUserId();
        if (selfId == null){
            return ResponseResult.error(AUTHENTICATION_ERROR.getCode(), AUTHENTICATION_ERROR.getDescription());
        }
        String role_name = spbUserDao.selectById(selfId).getRoleName();
        if (ADMINISTRATOR.equals(role_name)) {
            try {
                for (Long userId : spbUserList) {
                    spbUserDao.deleteById(userId);
                }
            }catch (Exception e){
                if (e instanceof NullPointerException) {
                    return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "删除的用户不存在");
                } else{
                    return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "未知错误");
                }
            }
        } else{
            return ResponseResult.error(ResponseStatusEnum.AUTHORIZATION_FORBIDDEN.getCode(), ResponseStatusEnum.AUTHORIZATION_FORBIDDEN.getDescription());
        }
        return ResponseResult.success();
    }

    @Override
    @Transactional
    public ResponseResult<Object> updateUser(SpbUser spbUser) {
        Long selfId = SessionUtil.getUserId();
        if (selfId == null){
            return ResponseResult.error(AUTHENTICATION_ERROR.getCode(), AUTHENTICATION_ERROR.getDescription());
        }
        String role_name = spbUserDao.selectById(selfId).getRoleName();

        if ("学生".equals(role_name)||ADMINISTRATOR.equals(role_name) || SECRETARY.equals(role_name)){
            Long userId = spbUser.getId();
            QueryWrapper<SpbUser> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id", userId);
            List<SpbUser> spbUsers = spbUserDao.selectList(queryWrapper);
            if (!spbUsers.isEmpty()){
                try{
                    SpbUser orinal_spbUser = spbUsers.get(0);
                    if(spbUser.getUserNumber() == null || spbUser.getUserNumber().isEmpty()){
                        spbUser.setUserNumber(orinal_spbUser.getUserNumber());
                    }
                    if(spbUser.getRoleName() == null || spbUser.getRoleName().isEmpty()){
                        spbUser.setRoleName(orinal_spbUser.getRoleName());
                    }
                    if(spbUser.getUsername() == null || spbUser.getUsername().isEmpty()){
                        spbUser.setUsername(orinal_spbUser.getUsername());
                    }
                    if(spbUser.getAddress() == null || spbUser.getAddress().isEmpty()){
                        spbUser.setAddress(orinal_spbUser.getAddress());
                    }
                    if(spbUser.getBirthday() == null){
                        spbUser.setBirthday(orinal_spbUser.getBirthday());
                    }
                    if(spbUser.getDevelopmentPhase() == null || spbUser.getDevelopmentPhase().isEmpty()){
                        spbUser.setDevelopmentPhase(orinal_spbUser.getDevelopmentPhase());
                    }
                    if(spbUser.getEmail() == null || spbUser.getEmail().isEmpty()){
                        spbUser.setEmail(orinal_spbUser.getEmail());
                    }
                    if(spbUser.getSex() == null){
                        spbUser.setSex(orinal_spbUser.getSex());
                    }
                    if(spbUser.getRegisterTime() == null){
                        spbUser.setRegisterTime(orinal_spbUser.getRegisterTime());
                    }
                    if(spbUser.getReadingStatus() == null || spbUser.getReadingStatus().isEmpty()){
                        spbUser.setReadingStatus(orinal_spbUser.getReadingStatus());
                    }
                    if(spbUser.getIdentityId() == null || spbUser.getIdentityId().isEmpty()){
                        spbUser.setIdentityId(orinal_spbUser.getIdentityId());
                    }
                    if(spbUser.getIfApply() == null){
                        spbUser.setIfApply(orinal_spbUser.getIfApply());
                    }
                    if(spbUser.getLeagueNum() == null || spbUser.getLeagueNum().isEmpty()){
                        spbUser.setLeagueNum(orinal_spbUser.getLeagueNum());
                    }
                    if(spbUser.getNationality() == null || spbUser.getNationality().isEmpty()){
                        spbUser.setNationality(orinal_spbUser.getNationality());
                    }
                    if(spbUser.getPartyBranch() == null || spbUser.getPartyBranch().isEmpty()){
                        spbUser.setPartyBranch(orinal_spbUser.getPartyBranch());
                    }
                    if(spbUser.getPhone() == null || spbUser.getPhone().isEmpty()){
                        spbUser.setPhone(orinal_spbUser.getPhone());
                    }
                    if(spbUser.getQualification() == null || spbUser.getQualification().isEmpty()){
                        spbUser.setQualification(orinal_spbUser.getQualification());
                    }
                    if(spbUser.getPwd() == null || spbUser.getPwd().isEmpty()){
                        spbUser.setPwd(orinal_spbUser.getPwd());
                    }
                    if(spbUser.getUserNumber().isEmpty()){
                        return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "更新的学/工号不能为空");
                    }
                    if(spbUser.getPwd().isEmpty()){
                        return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "更新的密码不能为空");
                    }
                    spbUser.setId(orinal_spbUser.getId());
                    spbUserDao.updateById(spbUser);
                }catch (Exception e){
                    return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "更新用户失败");
                }
            }else{
                return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "更新的用户id不存在");
            }
        } else{
            return ResponseResult.error(ResponseStatusEnum.AUTHORIZATION_FORBIDDEN.getCode(), ResponseStatusEnum.AUTHORIZATION_FORBIDDEN.getDescription());
        }
        return ResponseResult.success();
    }

    @Override
    @Transactional
    public ResponseResult<Object> updateUsers(List<SpbUser> spbUserList) {
        Long selfId = SessionUtil.getUserId();
        if (selfId == null){
            return ResponseResult.error(AUTHENTICATION_ERROR.getCode(), AUTHENTICATION_ERROR.getDescription());
        }
        String role_name = spbUserDao.selectById(selfId).getRoleName();
        if (ADMINISTRATOR.equals(role_name) || SECRETARY.equals(role_name)) {
            List<Long> userIds = new ArrayList<>();
            for (SpbUser spbUser : spbUserList) {
                userIds.add(spbUser.getId());
            }
            List<SpbUser> oringalUsers = spbUserDao.selectBatchIds(userIds);
            if(oringalUsers.size() != userIds.size()){
                return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "存在更新的用户id不存在");
            }

            for (SpbUser spbUser : spbUserList) {
                Long userId = spbUser.getId();
                QueryWrapper<SpbUser> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("id", userId);
                List<SpbUser> spbUsers = spbUserDao.selectList(queryWrapper);
                try{
                    SpbUser orinal_spbUser = spbUsers.get(0);
                    if(spbUser.getUserNumber() == null || spbUser.getUserNumber().isEmpty()){
                        spbUser.setUserNumber(orinal_spbUser.getUserNumber());
                    }
                    if(spbUser.getRoleName() == null || spbUser.getRoleName().isEmpty()){
                        spbUser.setRoleName(orinal_spbUser.getRoleName());
                    }
                    if(spbUser.getUsername() == null || spbUser.getUsername().isEmpty()){
                        spbUser.setUsername(orinal_spbUser.getUsername());
                    }
                    if(spbUser.getAddress() == null || spbUser.getAddress().isEmpty()){
                        spbUser.setAddress(orinal_spbUser.getAddress());
                    }
                    if(spbUser.getBirthday() == null){
                        spbUser.setBirthday(orinal_spbUser.getBirthday());
                    }
                    if(spbUser.getDevelopmentPhase() == null || spbUser.getDevelopmentPhase().isEmpty()){
                        spbUser.setDevelopmentPhase(orinal_spbUser.getDevelopmentPhase());
                    }
                    if(spbUser.getEmail() == null || spbUser.getEmail().isEmpty()){
                        spbUser.setEmail(orinal_spbUser.getEmail());
                    }
                    if(spbUser.getSex() == null){
                        spbUser.setSex(orinal_spbUser.getSex());
                    }
                    if(spbUser.getRegisterTime() == null){
                        spbUser.setRegisterTime(orinal_spbUser.getRegisterTime());
                    }
                    if(spbUser.getReadingStatus() == null || spbUser.getReadingStatus().isEmpty()){
                        spbUser.setReadingStatus(orinal_spbUser.getReadingStatus());
                    }
                    if(spbUser.getIdentityId() == null || spbUser.getIdentityId().isEmpty()){
                        spbUser.setIdentityId(orinal_spbUser.getIdentityId());
                    }
                    if(spbUser.getIfApply() == null){
                        spbUser.setIfApply(orinal_spbUser.getIfApply());
                    }
                    if(spbUser.getLeagueNum() == null || spbUser.getLeagueNum().isEmpty()){
                        spbUser.setLeagueNum(orinal_spbUser.getLeagueNum());
                    }
                    if(spbUser.getNationality() == null || spbUser.getNationality().isEmpty()){
                        spbUser.setNationality(orinal_spbUser.getNationality());
                    }
                    if(spbUser.getPartyBranch() == null || spbUser.getPartyBranch().isEmpty()){
                        spbUser.setPartyBranch(orinal_spbUser.getPartyBranch());
                    }
                    if(spbUser.getPhone() == null || spbUser.getPhone().isEmpty()){
                        spbUser.setPhone(orinal_spbUser.getPhone());
                    }
                    if(spbUser.getQualification() == null || spbUser.getQualification().isEmpty()){
                        spbUser.setQualification(orinal_spbUser.getQualification());
                    }
                    if(spbUser.getPwd() == null || spbUser.getPwd().isEmpty()){
                        spbUser.setPwd(orinal_spbUser.getPwd());
                    }
                    spbUser.setId(orinal_spbUser.getId());
                    spbUserDao.updateById(spbUser);
                }catch (Exception e){
                    return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "更新用户失败");
                }
            }
        }
        return ResponseResult.success();
    }

    @Override
    @Transactional
    public List<SpbUser> findUser(SpbUser spbUser) {
//        Long id = spbUser1.getId();
//        String username = spbUser1.getUsername();
//        String roleName = spbUser1.getRoleName();
//        Date registerTime = spbUser1.getRegisterTime();
//
//        SelectStatementProvider selectStatement = SqlBuilder.select(spbUserMapper.selectList)
//            .from(spbUser)
//            .where(SpbUserDynamicSqlSupport.id, isEqualToWhenPresent(id))
//            .and(SpbUserDynamicSqlSupport.username, isLikeWhenPresent(username))
//            .and(SpbUserDynamicSqlSupport.roleName, isEqualToWhenPresent(roleName))
//            .and(SpbUserDynamicSqlSupport.registerTime, isGreaterThanOrEqualToWhenPresent(registerTime))
//            .build()
//            .render(RenderingStrategies.MYBATIS3);
//
//        return spbUserMapper.selectMany(selectStatement);

//        return spbUserDao.selectList(new LambdaQueryWrapper<SpbUser>()
//            .eq(SpbUser::getId, spbUser.getId())
//            .eq(SpbUser::getUsername, spbUser.getUsername())
//            .eq(SpbUser::getRoleName, spbUser.getRoleName())
//            .gt(SpbUser::getRegisterTime, spbUser.getRegisterTime())
//        );
        return spbUserDao.selectList(new LambdaQueryWrapper<SpbUser>()
            .eq(!ObjectUtils.isEmpty(spbUser.getId()), SpbUser::getId, spbUser.getId())
            .eq(!ObjectUtils.isEmpty(spbUser.getUserNumber()), SpbUser::getUserNumber, spbUser.getUserNumber())
            .eq(!ObjectUtils.isEmpty(spbUser.getUsername()), SpbUser::getUsername, spbUser.getUsername())
            .eq(!ObjectUtils.isEmpty(spbUser.getRoleName()), SpbUser::getRoleName, spbUser.getRoleName())
            .gt(!ObjectUtils.isEmpty(spbUser.getRegisterTime()), SpbUser::getRegisterTime, spbUser.getRegisterTime())
        );
    }

    @Override
    public SpbUser findUserByNumber(String userNumber) {
        QueryWrapper<SpbUser> wrapper = new QueryWrapper<SpbUser>();
        if (userNumber != null) {
            wrapper.eq("user_number", userNumber);
        }
        return spbUserDao.selectOne(wrapper);
    }

    @Override
    public IPage<SpbUser> selectByPhase(Page<SpbUser> page, String partyBranch, String developmentPhase) {
        QueryWrapper<SpbUser> wrapper = new QueryWrapper<SpbUser>();
        if (partyBranch != null) {
            wrapper.eq("party_branch", partyBranch);
        }
        if (developmentPhase != null) {
            wrapper.eq("development_phase", developmentPhase);
        }
        return spbUserDao.selectPage(page, wrapper);
    }

    @Override
    public IPage<SpbUser> selectByState(Page<Activity> page, String partyBranch, String activityNumber, String state) {
        QueryWrapper<Activity> wrapper = new QueryWrapper<Activity>();
        if (partyBranch != null) {
            wrapper.eq("party_branch", partyBranch);
        }
        if (activityNumber != null) {
            wrapper.eq("activity_number", activityNumber);
        }
        if (state != null) {
            wrapper.eq("state", state);
        }
        Page<Activity> activityPage = activityMapper.selectPage(page, wrapper);
        List<SpbUser>  spbUsers     = new ArrayList<>();
        for (Activity activity : activityPage.getRecords()
        ) {
            SpbUser userByNumber = findUserByNumber(activity.getUserNumber());
            spbUsers.add(userByNumber);
        }
        Page<SpbUser> spbUserPage = new Page<>();
        spbUserPage.setRecords(spbUsers);
        spbUserPage.setPages(activityPage.getPages());
        spbUserPage.setCurrent(activityPage.getCurrent());
        spbUserPage.setSize(activityPage.getSize());
        return spbUserPage;
    }

    @Override
    public IPage<SpbUser> selectByRoleName(Page<SpbUser> page, String partyBranch, String roleName) {
        QueryWrapper<SpbUser> wrapper = new QueryWrapper<SpbUser>();
        if (partyBranch != null) {
            wrapper.eq("party_branch", partyBranch);
        }
        if (roleName != null) {
            wrapper.eq("role_name", roleName);
        }
        return spbUserDao.selectPage(page, wrapper);
    }

}
