package edu.zjucst.spb.service.message;

import com.baomidou.mybatisplus.extension.service.IService;
import edu.zjucst.spb.domain.entity.message.UserMessageText;
import edu.zjucst.spb.domain.param.message.GetUserMessageParam;

import java.util.List;

/**
 * (UserMessageText)UserMessageText表的服务接口
 *
 * @author Mrli
 * @since 2023-04-05 14:10:48
 */
public interface UserMessageTextService extends IService<UserMessageText> {

    /**
     * 查询信息内容列表
     */
//    List<UserMessageVO> getUserMessageList(GetUserMessageParam dto);
    List<UserMessageText> getUserMessageTextList(List<Integer> ids);

}

