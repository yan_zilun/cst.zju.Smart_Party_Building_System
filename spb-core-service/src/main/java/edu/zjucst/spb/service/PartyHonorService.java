package edu.zjucst.spb.service;

import com.alibaba.fastjson2.JSONObject;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 18:21
 */
public interface PartyHonorService {
    JSONObject listPartyHonor();
}
