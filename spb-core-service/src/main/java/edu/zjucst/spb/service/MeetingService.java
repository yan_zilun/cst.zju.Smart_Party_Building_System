package edu.zjucst.spb.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.domain.entity.Meeting;
import org.springframework.stereotype.Service;
//

/**
 * 组织生活service的接口
 * @author zjh
 */
@Service
public interface MeetingService {

    int addMeeting(Meeting meeting);

    /**
     * 根据查询条件查询信息
     *
     * @return
     */
    IPage<Meeting> selectMeeting(Page<Meeting> page, String organization, String topic, String claasification);
}
