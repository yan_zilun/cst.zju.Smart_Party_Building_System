package edu.zjucst.spb.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import edu.zjucst.spb.dao.mapper.BranchActivityMapper;
import edu.zjucst.spb.domain.entity.Branch;
import edu.zjucst.spb.domain.entity.BranchActivity;
import edu.zjucst.spb.service.BranchActivityService;
import edu.zjucst.spb.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 16:58
 */
@Service
public class BranchActivityServiceImpl implements BranchActivityService {
    @Autowired
    BranchActivityMapper branchActivityMapper;

    @Override
    public JSONObject listBranchActivity() {
        Long userId = SessionUtil.getUserId();
        JSONObject resp = new JSONObject();
        if(userId == null){
            resp.put("error_message", "用户不存在或未登录");
            return resp;
        }
        QueryWrapper<BranchActivity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userId);
        List<BranchActivity> branchActivities = branchActivityMapper.selectList(queryWrapper);
        resp.put("activities", branchActivities);
        resp.put("error_message", "success");
        return resp;
    }
}
