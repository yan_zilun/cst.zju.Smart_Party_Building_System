package edu.zjucst.spb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.dao.mapper.MeetingMapper;
import edu.zjucst.spb.dao.meeting.MeetingDao;
import edu.zjucst.spb.domain.entity.Meeting;
import edu.zjucst.spb.service.MeetingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

//
@Service
@Slf4j
public class MeetingServiceImpl implements MeetingService {

    @Autowired
    private MeetingDao meetingDao;

    @Autowired
    private MeetingMapper meetingMapper;

    @Override
    public int addMeeting(Meeting meeting) {
        return meetingDao.insert(meeting);
    }

    @Override
    public IPage<Meeting> selectMeeting(Page<Meeting> page, String organization, String topic, String claasification) {
        QueryWrapper<Meeting> wrapper = new QueryWrapper<Meeting>();
        if (organization != null) {
            wrapper.eq("organization", organization);
        }
        if (topic != null) {
            wrapper.like("topic", topic);
        }
        if (claasification != null) {
            wrapper.eq("claasification", claasification);
        }
        Page<Meeting> meetingPage = meetingMapper.selectPage(page, wrapper);
        List<Meeting> meetings = new ArrayList<>();
        for (Meeting meeting : meetingPage.getRecords()
        ) {
            meetings.add(meeting);
        }
        Page<Meeting> spbUserPage = new Page<>();
        spbUserPage.setRecords(meetings);
        spbUserPage.setPages(meetingPage.getPages());
        spbUserPage.setCurrent(meetingPage.getCurrent());
        spbUserPage.setSize(meetingPage.getSize());
        return spbUserPage;
    }
}
