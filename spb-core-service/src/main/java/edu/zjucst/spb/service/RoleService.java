package edu.zjucst.spb.service;

import edu.zjucst.spb.domain.entity.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {

    /**
     * 根据查询条件查询信息
     *
     * @return
     */
    List<Role> findRole(Role role);

    Role findRoleById(Long Id);
}
