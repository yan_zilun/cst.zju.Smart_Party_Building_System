package edu.zjucst.spb.service;

import edu.zjucst.spb.domain.entity.SelfStudy;
import com.baomidou.mybatisplus.extension.service.IService;
import edu.zjucst.spb.domain.param.selfstudy.GetSelfStudyParam;
import edu.zjucst.spb.domain.vo.MySelfStudyVO;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 自主活动相关服务类
 * @author 江中华
 * @since 2024-03-11
 */

public interface SelfStudyService extends IService<SelfStudy> {
    /**
     * 学生新增或修改自主活动
     *
     * @param dto 活动信息
     */
//    void addSelfActivity(AddSelfActivityParam dto);
    /**
     * 学生查询自主学习
     *
     * @param dto 活动信息
     */
    List<MySelfStudyVO> getMySelfStudyByPage(GetSelfStudyParam dto);
//    List<MySelfStudyVO> getMySelfStudyByDevelopmentPhaseID(GetSelfStudyParam dto);

    /**
     * 学生查询自主学习总学时
     *
     * @param userNumber 学工号
     */
    Integer getAllStudyHour(String userNumber);

    /**
     * 学生查询自主学习当前阶段学时
     *
     * @param userNumber 学工号
     */
    Integer getAllStudyHourNowDevelopmentPhase(String userNumber);

}
