package edu.zjucst.spb.service;

import edu.zjucst.spb.domain.param.activity.AddBranchActivityParam;
import edu.zjucst.spb.domain.param.activity.AddSelfActivityParam;
import edu.zjucst.spb.domain.param.activity.GetSelfActivityParam;
import edu.zjucst.spb.domain.entity.SelfActivity;
import com.baomidou.mybatisplus.extension.service.IService;
import edu.zjucst.spb.domain.vo.MySelfActivityVO;

import java.util.List;

/**
 * 自主活动相关服务类
 * @author qingo
 * @since 2023-04-18
 */
public interface SelfActivityService extends IService<SelfActivity> {

    /**
     * 学生新增或修改自主活动
     *
     * @param dto 活动信息
     */
    void addSelfActivity(AddSelfActivityParam dto);
    /**
     * 学生查询自主活动
     *
     * @param dto 活动信息
     */
    List<MySelfActivityVO> getMySelfActivityByPage(GetSelfActivityParam dto);
    /**
     * 团委新增支部活动
     *
     * @param dto 活动信息
     */
    void addBranchActivity(AddBranchActivityParam dto);

}
