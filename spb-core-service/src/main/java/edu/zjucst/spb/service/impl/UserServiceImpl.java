package edu.zjucst.spb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import edu.zjucst.spb.dao.user.UserDao;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.param.user.LoginParam;
import edu.zjucst.spb.domain.param.user.SignUpParam;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.vo.branch.MemberCountVO;
import edu.zjucst.spb.domain.vo.user.LoginVO;
import edu.zjucst.spb.enums.OptStatusEnum;
import edu.zjucst.spb.enums.UserBusinessStatusEnum;
import edu.zjucst.spb.exception.UserBusinessException;
import edu.zjucst.spb.pkg.redis.UserRedisDao;
import edu.zjucst.spb.pkg.shiro.token.SPBUsernamePasswordToken;
import edu.zjucst.spb.service.UserService;
import edu.zjucst.spb.util.SessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.flowable.engine.IdentityService;
import org.flowable.idm.api.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author qingo
 * @date 2022/12/22 16:50
 **/
@Slf4j
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, SpbUser> implements UserService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRedisDao userRedisDao;

    @Autowired
    private IdentityService identityService;

    @Override
    public MemberCountVO countPartyMember(String name) {
        QueryWrapper<SpbUser> queryWrapper=new QueryWrapper<>();
        Integer partyMember = Math.toIntExact(baseMapper.selectCount(queryWrapper.eq("party_branch", name).eq("development_phase", "党员")));
        Integer preparePartyMember = Math.toIntExact(baseMapper.selectCount(queryWrapper.eq("party_branch", name).eq("development_phase", "预备党员")));
        MemberCountVO memberCountVO = new MemberCountVO();
        memberCountVO.setPartyMember(partyMember);
        memberCountVO.setPreparePartyMember(preparePartyMember);
        return memberCountVO;
    }

    /**
     * 注册接口
     *
     * @author qingo
     * @date 2022/12/22 15:56
     **/
    @Transactional
    public ResponseResult<Object> signUp(SignUpParam dto) {
        // 1.1参数检验
        if (StringUtils.isEmpty(dto.getUserNumber()) || StringUtils.isEmpty(dto.getPwd())) {
            return ResponseResult.error(OptStatusEnum.PARAM_ERROR);
        }
        // 1.2 需要两次密码一致
        if (!dto.getPwdConfirmed().equals(dto.getPwd())) {
            throw new UserBusinessException(UserBusinessStatusEnum.PWD_NOT_SAME_ERR);
        }
        // 2. 创建对象
        SpbUser user = dto.transform();

        // 3.1 插入数据库 tb_user
        try {
            userDao.insert(user);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "账号已存在, 无法注册");
            }else if (e instanceof DataIntegrityViolationException) {
                return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "密码太长");
            }else{
                return ResponseResult.error(OptStatusEnum.OPT_ERROR.getCode(), "未知错误");
            }
        }

        // 3.2 同步操作 flowable的user表, UserEntityImpl不行
        User userIDM = identityService.newUser(String.valueOf(user.getId()));
        userIDM.setId(String.valueOf(user.getId()));
        userIDM.setDisplayName(user.getUsername());
        userIDM.setPassword(user.getPwd());
        identityService.saveUser(userIDM);
        return ResponseResult.success();
    }


    /**
     * 登录接口
     *
     * @author qingo
     * @date 2022/12/22 15:56
     **/
    public ResponseResult<LoginVO> login(LoginParam loginParam) {
        Subject subject = SecurityUtils.getSubject();

        try {
            // 首先进行验证码校验
            String codeKey = loginParam.getCodeKey();
            String verifyCode = loginParam.getVerifyCode();
            String correctCode = userRedisDao.get(codeKey);//获得redis中的验证码
            if(correctCode==null){//表明验证码过期
                return ResponseResult.error(UserBusinessStatusEnum.VERIFICATION_EXPIRED.getCode(), UserBusinessStatusEnum.VERIFICATION_EXPIRED.getDescription());
            }else if(!correctCode.equalsIgnoreCase(verifyCode)){//验证码错误
                return ResponseResult.error(UserBusinessStatusEnum.VERIFICATION_ERROR.getCode(), UserBusinessStatusEnum.VERIFICATION_ERROR.getDescription());
            }else {//验证码正确
                SPBUsernamePasswordToken token = new SPBUsernamePasswordToken(loginParam.getUserNumber(), loginParam.getPwd());
                subject.login(token);
            }
        } catch (UnknownAccountException e) {
            return ResponseResult.error(UserBusinessStatusEnum.USER_NOT_EXIST.getCode(), UserBusinessStatusEnum.USER_NOT_EXIST.getDescription());
        } catch (IncorrectCredentialsException e) {
            return ResponseResult.error(UserBusinessStatusEnum.PASSWORD_ERROR.getCode(), UserBusinessStatusEnum.PASSWORD_ERROR.getDescription());
        }
//        验证成功
        SpbUser spbUser = userDao.selectOne(new LambdaQueryWrapper<SpbUser>().eq(SpbUser::getUserNumber, loginParam.getUserNumber()));
        LoginVO loginVO = new LoginVO();//LoginVO包含userId、学号userNumber、用户名username以及角色名roleName
        loginVO.setUserId(spbUser.getId());
        loginVO.setUserNumber(spbUser.getUserNumber());
        loginVO.setRoleName(spbUser.getRoleName());
        loginVO.setUsername(spbUser.getUsername());
        System.out.println("test:");
        System.out.println(loginVO);
//        SessionUtil.setUserId(spbUser.getId());
        return ResponseResult.success(loginVO);
    }


    // TODO: 修改密码
}
