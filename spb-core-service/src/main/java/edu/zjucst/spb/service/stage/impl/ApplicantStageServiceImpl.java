package edu.zjucst.spb.service.stage.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.dao.mapper.stage.ApplicantStageMapper;
import edu.zjucst.spb.domain.entity.stage.ApplicantStage;
import edu.zjucst.spb.service.stage.ApplicantStageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ZlunYan
 * @description
 * @create 2024-5-25
 */

@Service
public class ApplicantStageServiceImpl implements ApplicantStageService {

    @Autowired
    private ApplicantStageMapper mapper;

    @Override
    public int insertStage(ApplicantStage stage) {
        return mapper.insert(stage);
    }

    @Override
    public ApplicantStage getStageByUserId(String userId) {
        QueryWrapper<ApplicantStage> wrapper = new QueryWrapper<>();
        wrapper.eq("user_number", userId);

        return mapper.selectOne(wrapper);
    }

    @Override
    public void updateStageByUserId(ApplicantStage stage, String userId) {
        QueryWrapper<ApplicantStage> wrapper = new QueryWrapper<>();
        wrapper.eq("user_number", userId);

        mapper.update(stage, wrapper);
    }

    @Override
    public void updateStage(ApplicantStage stage) {
        QueryWrapper<ApplicantStage> wrapper = new QueryWrapper<>();
        wrapper.eq("user_number", stage.getUserNumber());

        mapper.update(stage, wrapper);
    }

    @Override
    public IPage<ApplicantStage> selectStageList(Page<ApplicantStage> page, QueryWrapper<ApplicantStage> wrapper) {
        // TODO 加入权限判断
        return mapper.selectPage(page, wrapper);
    }
}
