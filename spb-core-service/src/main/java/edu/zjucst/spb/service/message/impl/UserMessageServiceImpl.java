package edu.zjucst.spb.service.message.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import edu.zjucst.spb.dao.mapper.UserMessageMapper;
import edu.zjucst.spb.dao.mapper.UserMessageTextMapper;
import edu.zjucst.spb.dao.message.UserMessageDao;
import edu.zjucst.spb.dao.message.UserMessageTextDao;
import edu.zjucst.spb.dao.user.UserDao;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.dto.MessageQueryDTO;
import edu.zjucst.spb.domain.dto.MessageQueryMapper;
import edu.zjucst.spb.domain.entity.SelfStudy;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import edu.zjucst.spb.domain.entity.message.UserMessageText;
import edu.zjucst.spb.domain.param.message.GetUserMessageParam;
import edu.zjucst.spb.domain.param.message.MessageQueryParam;
import edu.zjucst.spb.domain.vo.message.UserMessageVO;
import edu.zjucst.spb.domain.vo.message.UserNotifyMessageVO;
import edu.zjucst.spb.enums.UserBusinessStatusEnum;
import edu.zjucst.spb.service.message.UserMessageService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * (UserMessage)表服务实现类
 *
 * @author Mrli
 * @since 2023-04-05 14:17:21
 */
@Service("userMessageService")
public class UserMessageServiceImpl extends ServiceImpl<UserMessageDao, UserMessage> implements UserMessageService {

    @Resource
    private UserMessageTextDao userMessageTextDao;
    @Resource
    private UserMessageDao userMessageDao;

    @Resource
    private UserDao userDao;

    @Resource
    private UserMessageMapper userMessageMapper;
    @Resource
    private UserMessageTextMapper userMessageTextMapper;

    @Override
    public boolean sendPresetMessage(Long sendUserId, Long recvUserId, Long messageId, LocalDateTime dateTime) {
        return save(UserMessage.builder()
            .messageId(messageId)
            .sendUserId(sendUserId)
            .recvUserId(recvUserId)
            .needPush(true)
            .needPushTime(dateTime)
            .build()
        );
    }

    @Override
    public boolean sendPresetMessageRightNow(Long sendUserId, Long recvUserId, Long messageId) {
        return save(UserMessage.builder()
            .messageId(messageId)
            .sendUserId(sendUserId)
            .recvUserId(recvUserId)
            .needPush(false)
            .build()
        );
    }

    @Override
    @Transactional
    public ResponseResult sendMessage(Long sendUserId, Long recvUserId, String content) {
        // 1. 创建一条 userMessageText
        UserMessageText userMessageText = new UserMessageText(content);
        // 插入成功后 主键id 会自动回显填入 userMessageText
        userMessageTextDao.insert(userMessageText);
        SpbUser spbUser = userDao.selectById(sendUserId);
        if (spbUser == null) {
            return ResponseResult.error(UserBusinessStatusEnum.SEND_USER_NOT_EXIST.getCode(), UserBusinessStatusEnum.SEND_USER_NOT_EXIST.getDescription());
        }
        SpbUser spbUser1 = userDao.selectById(recvUserId);
        if (spbUser1 == null) {
            return ResponseResult.error(UserBusinessStatusEnum.RECEIVE_USER_NOT_EXIST.getCode(), UserBusinessStatusEnum.RECEIVE_USER_NOT_EXIST.getDescription());
        }
        // 2. TODO: 这边得校验 sendUserId, recvUserId 是否合法(存在)
//        boolean existSend = userDao.exists(new LambdaQueryWrapper<SpbUser>().eq(SpbUser::getId, sendUserId));
//        boolean existRecv = userDao.exists(new LambdaQueryWrapper<SpbUser>().eq(SpbUser::getId, recvUserId));
//
//        if (!existSend || !existRecv) {
//            return false;
//        }

        // 3. 创建一条 userMessage, 其中 messageId 为刚创建 userMessageText 的 id
        boolean save = save(UserMessage.builder()
            .messageId(userMessageText.getId())
            .sendUserId(sendUserId)
            .recvUserId(recvUserId)
            .needPush(false)
            .build()
        );
        return ResponseResult.success();
    }


    @Override
    @Transactional
    public ResponseResult sendMessageMessageToGroup(Long sendUserId, List<Long> userList, String content) {
        UserMessageText userMessageText = new UserMessageText(content);
        // 插入成功后 主键id 会自动回显填入 userMessageText
        userMessageTextDao.insert(userMessageText);
        SpbUser spbUser = userDao.selectById(sendUserId);
        if (spbUser == null) {
            return ResponseResult.error(UserBusinessStatusEnum.SEND_USER_NOT_EXIST.getCode(), UserBusinessStatusEnum.SEND_USER_NOT_EXIST.getDescription());
        }
        // 2. TODO: 这边得校验 sendUserId, recvUserId 是否合法(存在)

        // 3. 给用户批量插入
        ArrayList<UserMessage> userMessages = new ArrayList<>();
        List<Long> list = new ArrayList<>();
        for (Long userId : userList) {
            SpbUser spbUser1 = userDao.selectById(userId);
            if (spbUser1 == null) {
                list.add(userId);
            } else {
                userMessages.add(UserMessage.builder()
                    .messageId(userMessageText.getId())
                    .sendUserId(sendUserId)
                    .recvUserId(userId)
                    .needPush(false)
                    .build());
            }
        }
        saveBatch(userMessages);
        if (list != null && list.size() > 0) {
            return ResponseResult.error(UserBusinessStatusEnum.RECEIVE_USER_NOT_EXIST.getCode(), list + UserBusinessStatusEnum.RECEIVE_USER_NOT_EXIST.getDescription());
        } else {
            return ResponseResult.success();
        }
    }


    @Override
    public List<Long> updateBatchByIdList(Long sendUserId, List<Long> idList) {
        List<Long> list = new ArrayList<>();
        List<Long> update = new ArrayList<>();
        if (idList != null && idList.size() > 0) {
            for (Long l : idList) {
                UserMessage userMessage = userMessageDao.selectById(l);
                if (userMessage == null) {
                    list.add(l);
                } else  {
                    update.add(l);
                }
            }
        }
        userMessageDao.updateIsReadByIdBatch(update);
        return list;
    }

//
//    public PageInfo<UserNotifyMessageVO> queryMessageByPage(Long recvUserId, MessageQueryParam param) {
//        MessageQueryDTO dto = MessageQueryMapper.INSTANCE.param2dto(param);
//        dto.setRecvUserId(recvUserId);
//        PageHelper.startPage(param.getPageNumber(), param.getPageSize());
//        List<UserNotifyMessageVO> message = userMessageDao.queryMessage(dto);
//        return new PageInfo<UserNotifyMessageVO>(message);
//    }

    public List<UserMessage> getUserMessageList(GetUserMessageParam dto){
        QueryWrapper<UserMessage> queryWrapper = new QueryWrapper<UserMessage>();

        if (dto.getRecvUserId() != null){
            queryWrapper.eq("recv_user_id", dto.getRecvUserId());
        }
        if (dto.getIsRead() != null){
            queryWrapper.eq("is_read", dto.getIsRead());
        }
        List<UserMessage> userMessages = userMessageMapper.selectList(queryWrapper);

        return userMessages;
    }

    public List<UserMessageText> getUserMessageTextList(GetUserMessageParam dto){
        QueryWrapper<UserMessage> queryWrapper = new QueryWrapper<UserMessage>();
        QueryWrapper<UserMessageText> queryWrapperText = new QueryWrapper<UserMessageText>();

        if (dto.getRecvUserId() != null){
            queryWrapper.eq("recv_user_id", dto.getRecvUserId());
        }
        if (dto.getIsRead() != null){
            queryWrapper.eq("is_read", dto.getIsRead());
        }
        List<UserMessage> userMessages = userMessageMapper.selectList(queryWrapper);

        List<Long> ids = new ArrayList<>();
        for (UserMessage userMessage : userMessages) {
            ids.add(userMessage.getMessageId());
        }
        queryWrapperText.in("id", ids);
//        queryWrapperTest.eq("id", userMessages.get(0).getMessageId());

        List<UserMessageText> userMessageTexts = userMessageTextMapper.selectList(queryWrapperText);

        return userMessageTexts;
    }
    // 获取当前用户全部未读消息
    @Override
    public List<UserMessage> getAllNOReadMessage(Long recvUserId) {
        QueryWrapper<UserMessage> queryWrapper = new QueryWrapper<UserMessage>();
        queryWrapper.eq("recv_user_id", recvUserId);
        return userMessageMapper.selectList(queryWrapper);
    }
}

