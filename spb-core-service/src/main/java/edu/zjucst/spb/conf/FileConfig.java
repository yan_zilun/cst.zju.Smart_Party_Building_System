package edu.zjucst.spb.conf;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class FileConfig {
    @Value(value = "${spb.file.save-path}")
    private String savePath;

}
