package edu.zjucst.spb.controller.help;

import edu.zjucst.spb.domain.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author MrLi
 * @date 2022-12-03 21:06
 **/
@RestController
@RequestMapping("/health")
@Api(value = "心跳检测", tags = {"非业务接口"})
public class HealthController {
    @GetMapping
    @ResponseBody
    @ApiOperation("心跳响应")
    public ResponseResult<Object> get(){
        return ResponseResult.success();
    }
}
