package edu.zjucst.spb.controller.activity;

import com.alibaba.fastjson2.JSONObject;
import edu.zjucst.spb.service.BranchActivityService;
import edu.zjucst.spb.service.impl.BranchActivityServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 16:55
 */
@RestController
public class BranchActivityController {
    @Autowired
    BranchActivityService branchActivityService;
    @RequestMapping("/branchactivity/list/")
    public JSONObject listBranchActivity(){
        return branchActivityService.listBranchActivity();
    }
}
