//package edu.zjucst.spb.controller.flow;
//
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.flow.CompleteTaskParam;
//import edu.zjucst.spb.domain.param.flow.StartProcessInstanceParam;
//import edu.zjucst.spb.enums.FlowProcessEnum;
//import edu.zjucst.spb.service.FlowableService;
//import io.swagger.annotations.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//import static edu.zjucst.spb.enums.OptStatusEnum.PARAM_ERROR;
//
///**
// * @author yzl
// * @date 2023/11/28 13:21
// * 审批任务，查询任务与流程实例
// */
//@RestController
//@Api(value = "审批相关接口", tags = {"活动审批流程"})
//@RequestMapping("/flow/activity")
//public class AuditActivityController {
//
//    @Autowired
//    private FlowableService flowableService;
//
//    @ApiOperation(value = "启动流程实例")
//    @PostMapping("/startProcessInstance")
//    public ResponseResult<Object> startProcessInstance(@RequestBody StartProcessInstanceParam startProcessInstanceParam) {
//        String processName = startProcessInstanceParam.getProcessName();
//        String processKey = FlowProcessEnum.getProcessKeyByProcessName(processName);
//        if (processKey == "UNKNOWN"){ // 传入了未实现的流程定义名
//            System.out.println("错误。启动流程实例时传入了未知的flowable流程定义名称！");
//            return ResponseResult.error(PARAM_ERROR);
//        }
//        flowableService.startProcessInstance(processKey, startProcessInstanceParam.getUserId(), startProcessInstanceParam.getStartParam());
//        return ResponseResult.success();
//    }
//
//    @ApiOperation(value = "审批（即完成流程实例中用户任务）")
//    @PostMapping("/completeTask")
//    public ResponseResult<Object> completeTask(@RequestBody CompleteTaskParam completeTaskParam) {
//        flowableService.completeTask(completeTaskParam.getTaskId(), completeTaskParam.getTaskParam());
//        return ResponseResult.success();
//    }
//
//    @ApiOperation(value = "查找用户发起的所有流程实例IdList")
//    @GetMapping("/queryProcessInstanceByOriginator")
//    public ResponseResult<List<String>> queryProcessInstanceByOriginator(
//        @ApiParam(value = "学号", required = true) String userId
//    ) {
//        return ResponseResult.success(flowableService.queryProcessInstanceByOriginator(userId));
//    }
//
//    @ApiOperation(value = "根据用户id查找需要它审批的任务")
//    @GetMapping("/queryTaskByUserId")
//    public ResponseResult<List<String>> queryTaskByUserId(
//        @ApiParam(value = "学号/工号", required = true) String userId
//    ) {
//        return ResponseResult.success(flowableService.queryTaskByUserId(userId));
//    }
//
//    @ApiOperation(value = "根据用户组id查找需要它审批的任务")
//    @GetMapping("/queryTaskByGroupId")
//    public ResponseResult<List<String>> queryTaskByGroupId(String groupId) {
//        return ResponseResult.success(flowableService.queryTaskByGroupId(groupId));
//    }
//
//    @ApiOperation(value = "给任务增加一个审批用户组")
//    @PostMapping("/groups")
//    public ResponseResult<Object> addCandidateGroup(String taskId, String groupId) {
//        flowableService.addCandidateGroup(taskId, groupId);
//        return ResponseResult.success();
//    }
//
//    @ApiOperation(value = "给任务删除一个审批用户组")
//    @DeleteMapping("/groups")
//    public ResponseResult<Object> deleteCandidateGroup(String taskId, String groupId) {
//        flowableService.deleteCandidateGroup(taskId, groupId);
//        return ResponseResult.success();
//    }
//
//    @ApiOperation(value = "根据任务id查询该任务的所有审批用户组")
//    @GetMapping("/groups/byTaskId")
//    public ResponseResult<List<String>> queryTaskCandidateGroup(
//        @ApiParam(value = "任务ID") String taskId
//    ) {
//        return ResponseResult.success(flowableService.queryTaskCandidateGroup(taskId));
//    }
//    @ApiOperation(value = "根据流程实例id查找任务，返回任务id")
//    @GetMapping("/queryTaskByProcessInstanceId")
//    public ResponseResult<List<String>> queryTaskByProcessInstanceId(
//        @ApiParam(value = "流程实例ID") String processInstanceId
//    ) {
//        return ResponseResult.success(flowableService.queryTaskByProcessInstanceId(processInstanceId));
//    }
//
//}
