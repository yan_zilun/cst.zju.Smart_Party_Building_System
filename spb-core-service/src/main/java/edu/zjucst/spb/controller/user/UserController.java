package edu.zjucst.spb.controller.user;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.param.user.LoginParam;
import edu.zjucst.spb.domain.param.user.SignUpParam;
import edu.zjucst.spb.domain.vo.user.LoginVO;
import edu.zjucst.spb.enums.OptStatusEnum;
import edu.zjucst.spb.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 用户接口
 *
 * @author qingo
 * @date 2022/12/21 14:10
 **/
@RestController
@Slf4j
@Api(value = "用户相关接口", tags = {"用户操作相关接口"})
@ApiSupport(author = "qingo")
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation("新用户注册")
    @PostMapping("/signup")
    public ResponseResult<Object> signUp(@RequestBody @Validated SignUpParam dto) {
        return userService.signUp(dto);
    }


    @ApiOperation("用户登录")
    @PostMapping("/login")
    public ResponseResult<LoginVO> login(@RequestBody @Validated LoginParam loginParam) {
        return userService.login(loginParam);
    }


//    @ApiOperation("用户登录")
//    @GetMapping("/login")
//    public ResponseResult loginGet() {
//        return ResponseResult.error(OptStatusEnum.AUTHENTICATION_ERROR);
//    }

}
