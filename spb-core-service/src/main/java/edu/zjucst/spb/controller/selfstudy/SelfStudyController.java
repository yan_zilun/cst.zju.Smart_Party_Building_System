package edu.zjucst.spb.controller.selfstudy;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.SelfStudy;
import edu.zjucst.spb.service.SelfStudyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 权限接口
 *
 * @author jiangzhonghua
 * @date 2024/3/11
 **/
@RestController
@Slf4j
@Api(value = "自主学习相关接口", tags = {"自主学习相关接口"})
@ApiSupport(author = "jzh")
@RequestMapping("/selfstudy")
public class SelfStudyController {

    @Autowired
    private SelfStudyService selfStudyService;

    @ApiOperation("查询自我学习总学时")
    @GetMapping("/allstudyhour")
    ResponseResult<Integer> getAllStudyHour(@Validated String userNumber){
        return ResponseResult.success(selfStudyService.getAllStudyHour(userNumber));
    }

    @ApiOperation("当前阶段自我学习总学时")
    @GetMapping("/getallstudyhour/nowdevelopmentPhase")
    ResponseResult<Integer> getAllStudyHourNowDevelopmentPhase(@Validated String userNumber) {
        return ResponseResult.success(selfStudyService.getAllStudyHourNowDevelopmentPhase(userNumber));
    }

}
