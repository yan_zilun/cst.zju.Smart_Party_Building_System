package edu.zjucst.spb.controller.role;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Role;
import edu.zjucst.spb.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 权限接口
 *
 * @author wyx
 * @date 2023/3/21 14:10
 **/
@RestController
@Slf4j
@Api(value = "权限相关接口", tags = {"权限相关接口"})
@ApiSupport(author = "wyx")
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 支部书记/系统管理员查询权限
     **/
    @ApiOperation("查询权限")
    @GetMapping("/find/auth")
    ResponseResult<List<Role>> findRole(@Validated Role role) {
        return ResponseResult.success(roleService.findRole(role));
    }

}
