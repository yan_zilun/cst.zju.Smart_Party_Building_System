//package edu.zjucst.spb.controller.flow;
//
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.flow.CreateIDMUserParam;
//import edu.zjucst.spb.service.FlowableService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.websocket.server.PathParam;
//
///**
// * @author yzl
// * @date 2023/11/28 13:22
// */
//@RestController
//@Api(value = "flowable用户管理接口(不对外开放)", tags = {"Flowable管理"})
//@RequestMapping("/flow/users")
//public class FlowUserController {
//    @Autowired
//    private FlowableService flowableService;
//
//    /**
//     * 添加flowable中的用户
//     */
//    @ApiOperation(value = "添加flowable中的用户")
//    @PostMapping("/")
//    public ResponseResult<Object> addUser(@RequestBody CreateIDMUserParam createIDMUserParam){
//        flowableService.createIDMUser(createIDMUserParam.getId(), createIDMUserParam.getDisplayName());
//        return ResponseResult.success();
//    }
//
//    /**
//     * 删除flowable中的用户
//     */
//    @ApiOperation(value = "删除flowable中的用户")
//    @DeleteMapping("/{id}")
//    public ResponseResult<Object> deleteUser(@PathParam("id") String id){
//        flowableService.deleteIDMUser(id);
//        return ResponseResult.success();
//    }
//
//}
//
