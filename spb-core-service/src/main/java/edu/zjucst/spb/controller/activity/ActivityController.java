package edu.zjucst.spb.controller.activity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Activity;
import edu.zjucst.spb.domain.param.activity.ActivityParam;
import edu.zjucst.spb.domain.param.activity.GetActivityParam;
import edu.zjucst.spb.domain.vo.ActivityQueryVO;
import edu.zjucst.spb.domain.vo.UserVo;
import edu.zjucst.spb.enums.OptStatusEnum;
import edu.zjucst.spb.service.ActivityService;
import edu.zjucst.spb.service.SpbUserService;
import edu.zjucst.spb.util.DateUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static edu.zjucst.spb.constant.ActivityConstant.ACTIVITY_TYPE_TRAINING;

@RestController
@Api(value = "活动相关接口", tags = {"活动相关接口"})
@RequestMapping("/activities")
public class ActivityController {

    @Autowired
    private ActivityService activityService;

    @Autowired
    private SpbUserService spbUserService;

    @ApiOperation(value = "查询活动")
    @GetMapping("/")
    public ResponseResult<ActivityQueryVO> queryActivityList(GetActivityParam dto) {
        IPage<Activity> activityIPage = activityService.selectActivityList(new Page<>(dto.getPageNumber(), dto.getPageSize()), dto.getDevelopmentPhase(), dto.getDays(), dto.getActivityNumber());
        ActivityQueryVO activityQueryVO = new ActivityQueryVO();
        activityQueryVO.setActivities(activityIPage.getRecords());
        activityQueryVO.setCurrent(activityIPage.getCurrent());
        activityQueryVO.setPageSize(activityIPage.getSize());
        activityQueryVO.setPages(activityIPage.getPages());
        return ResponseResult.success(activityQueryVO);
    }

    @ApiOperation(value = "新增活动")
    @PostMapping("/")
    public ResponseResult<Object> addActivityList(@RequestBody ActivityParam activityParam) {
        try {
            List<Activity> activityList = getActivityList(activityParam);
            activityParam.setActivityNumber(activityService.selectActivityNumber());
            activityService.insert(activityList);
        } catch (ParseException e) {
            return ResponseResult.error(OptStatusEnum.PARAM_ERROR.getCode(), "日期参数错误");
        }

//        String state;
//        if (activityParam.getActivityType().equals(ACTIVITY_TYPE_TRAINING)) {
//            state = "已参加";
//        } else {
//            state = "已提交";
//        }
//        activityService.update(activityParam.getActivityNumber(), activityParam.getUserState(), state);
        return ResponseResult.success();
    }


    @ApiOperation(value = "更新活动")
    @PutMapping("/")
    public ResponseResult<Object> updateActivityList(@RequestBody ActivityParam activityParam) {
        if (activityService.hasExistedActivityNumber(activityParam.getActivityNumber()) == false) {
            return ResponseResult.error(OptStatusEnum.PARAM_ERROR.getCode(), "当前活动编号不存在");
        }
        try {
            List<Activity> activityList = getActivityList(activityParam);
            activityService.delete(activityParam.getActivityNumber());
            activityService.insert(activityList);
        } catch (ParseException e) {
            return ResponseResult.error(OptStatusEnum.PARAM_ERROR.getCode(), "日期参数错误");
        }

//        String state;
//        if (activityParam.getActivityType().equals(ACTIVITY_TYPE_TRAINING)) {
//            state = "已参加";
//        } else {
//            state = "已提交";
//        }
//        activityService.update(activityParam.getActivityNumber(), activityParam.getUserState(), state);
        return ResponseResult.success();
    }

    public List<Activity> getActivityList(ActivityParam activityParam) throws ParseException {
        List<Activity> activityList = new ArrayList<>();
        for (UserVo user : activityParam.getUserList()) {
            Activity activity = new Activity();
            activity.setActivityName(activityParam.getActivityName());
            activity.setActivityNumber(activityParam.getActivityNumber());
            activity.setActivitySponsor(activityParam.getActivitySponsor());
            activity.setActivityType(activityParam.getActivityType());
            activity.setActivityDate(DateUtil.transferToDate(activityParam.getActivityDate()));
            activity.setAdditionFile(activityParam.getAdditionFile());
            activity.setAppliedStudyHour(activityParam.getAppliedStudyHour());
            activity.setUserName(user.getUsername());
            activity.setDevelopmentPhase(spbUserService.findUserByNumber(user.getUserNumber()).getDevelopmentPhase());
            activity.setUserNumber(user.getUserNumber());
            activity.setRemark(activityParam.getRemark());
            if (activityParam.getActivityType().equals(ACTIVITY_TYPE_TRAINING)) {
                activity.setState("未参加");
            } else {
                activity.setState("未提交");
            }
            activityList.add(activity);
        }
        return activityList;
    }


}
