package edu.zjucst.spb.controller.flow;

import edu.zjucst.spb.domain.PageQry;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.flow.FlowGroup;
import edu.zjucst.spb.domain.entity.flow.FlowUser;
import edu.zjucst.spb.domain.param.flow.FlowGroupMemberQueryParam;
import edu.zjucst.spb.domain.vo.flow.FlowGroupQueryVO;
import edu.zjucst.spb.service.FlowBuildService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-29
 */

@RestController
@Api(value = "流程相关接口", tags = {"流程建设相关接口"})
@RequestMapping("/flow/build")
public class FlowBuildController {

    @Autowired
    private FlowBuildService flowBuildService;

    @ApiOperation("插入流程控制者(党委老师/党支部书记)")
    @PostMapping("/user")
    public ResponseResult<Integer> addFlowUser(@RequestBody @Validated FlowUser flowUser) {
        return ResponseResult.success(flowBuildService.addUser(flowUser));
    }

    @ApiOperation("插入流程用户组(党委/党支部)")
    @PostMapping("/group")
    public ResponseResult<Integer> addFlowGroup(@RequestBody @Validated @ApiParam("名称 例如：党委/第一党支部") String groupName) {
        return ResponseResult.success(flowBuildService.createGroup(groupName));
    }

    @ApiOperation("在用户组中新增控制者")
    @PostMapping("/group/membership")
    public ResponseResult<Integer> addUserMemberShip(@Validated @ApiParam("用户学工号 例如：10000001") String userId, @Validated @ApiParam("用户组名 例如：第一党支部") String groupName) {
//        System.out.println(userId);
//        System.out.println(groupName);
        return ResponseResult.success(flowBuildService.createUserMemberShip(userId, groupName));
    }

    @ApiOperation("删除流程控制者(党委老师/党支部书记)")
    @DeleteMapping("/user")
    public ResponseResult<Object> deleteFlowUser(@RequestBody @Validated @ApiParam("用户学工号 例如：10000001") String userId) {
        flowBuildService.deleteUserByUserId(userId);
        return ResponseResult.success();
    }

    @ApiOperation("删除流程用户组(党委/党支部)")
    @DeleteMapping("/group")
    public ResponseResult<Object> deleteFlowGroup(@RequestBody @Validated @ApiParam("用户组名 例如：第一党支部") String groupName) {
        flowBuildService.deleteGroupByGroupName(groupName);
        return ResponseResult.success();
    }

    @ApiOperation("在用户组删除控制者")
    @DeleteMapping("/group/membership")
    public ResponseResult<Integer> deleteUserMemberShip(@Validated @ApiParam("用户学工号 例如：10000001") String userId, @Validated @ApiParam("用户组名 例如：第一党支部") String groupName) {
//        System.out.println(userId);
//        System.out.println(groupName);
        return ResponseResult.success(flowBuildService.deleteUserMemberShip(userId, groupName));
    }

    @ApiOperation("查询流程用户组(党委/党支部)")
    @GetMapping("/group")
    public ResponseResult<Object> queryFlowGroup(@Validated FlowGroupMemberQueryParam param) {
//        System.out.println("groupName = " + groupName);
        return ResponseResult.success(flowBuildService.queryGroupMemberByGroupName(param.getPageNumber(), param.getPageSize(), param.getGroupName()));
    }

    @ApiOperation("查询所有流程用户组")
    @GetMapping("/listGroup")
    public ResponseResult<FlowGroupQueryVO> queryFlowGroup (PageQry qry) {
        return ResponseResult.success(flowBuildService.queryAllGroup(qry.getPageNumber(), qry.getPageSize()));
    }
}
