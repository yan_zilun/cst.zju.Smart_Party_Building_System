package edu.zjucst.spb.controller.stage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.domain.param.stage.StageQueryParam;
import edu.zjucst.spb.domain.vo.stage.NewStageQueryVO;
import edu.zjucst.spb.domain.vo.stage.StageInsertVO;
import edu.zjucst.spb.service.StageService;
import edu.zjucst.spb.util.converMapper.StageConvertMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

//@RestController
@Slf4j
//@Api(value = "发展阶段相关接口(弃用)", tags = {"发展阶段相关接口"})
//@RequestMapping("/stages")
public class StageController {

//    @Autowired
    private StageService stageService;

    @ApiOperation("[测试用]插入发展阶段(注册入党用户)")
    @PostMapping("/insert")
    public ResponseResult<Integer> addStage(@RequestBody @Validated StageInsertVO stage) {
//        System.out.println("insert");
//        System.out.println("stage.getUserName() = " + stage.getUserName());
//        System.out.println("stage.getUserNumber() = " + stage.getUserNumber());
//        System.out.println("stage.getDevelopmentPhaseId() = " + stage.getDevelopmentPhaseId());
//        System.out.println("stage.getBirthday() = " + stage.getBirthday());

        return ResponseResult.success(stageService.insertStage(StageConvertMapper.INSTANCE.stageInsertVOConvertstage(stage)));
    }

    @ApiOperation(value = "查询Stage列表")
    @GetMapping("/query")
    public ResponseResult<NewStageQueryVO> queryStageList(StageQueryParam stageQueryParam) {
        IPage<Stage> stageList = stageService.selectStageList(new Page<>(stageQueryParam.getPageNumber(),stageQueryParam.getPageSize()),
            stageQueryParam.getDevelopId(), stageQueryParam.getUserName(), stageQueryParam.getUserNumber(),
            stageQueryParam.getDateType(), stageQueryParam.getStartTime(), stageQueryParam.getEndTime());

        NewStageQueryVO stageQueryVO = new NewStageQueryVO();

        stageQueryVO.setStageVOList(StageConvertMapper.INSTANCE.stageConvertNewStageVO(stageList.getRecords()));
        stageQueryVO.setCurrent(stageList.getCurrent());
        stageQueryVO.setPages(stageList.getPages());
        stageQueryVO.setPageSize(stageList.getSize());

        return ResponseResult.success(stageQueryVO);
    }
}
//
//import com.baomidou.mybatisplus.core.metadata.IPage;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import edu.zjucst.spb.dao.mapper.StageMapper;
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.stage.*;
//import edu.zjucst.spb.domain.entity.Stage;
//import edu.zjucst.spb.domain.vo.stage.*;
//import edu.zjucst.spb.service.StageService;
//import edu.zjucst.spb.util.converMapper.StageConvertMapper;
//import edu.zjucst.spb.util.StageUtil;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import static edu.zjucst.spb.constant.StageConstant.*;
//
//@RestController
//@Api(value = "发展阶段相关接口", tags = {"发展阶段相关接口"})
//@RequestMapping("/stages")
//public class StageController {
//
//    @Autowired
//    private StageService stageService;
//
//    @ApiOperation(value = "查询入党申请人阶段用户列表")
//    @GetMapping("/applicant")
//    public ResponseResult<ApplicantQueryVO> queryApplicantStageList(StageQueryParam stageQueryParam) {
//        IPage<Stage> stageList = stageService.selectStageList(new Page<>(stageQueryParam.getPageNumber(),stageQueryParam.getPageSize()),APPLICANT, stageQueryParam.getUserName(), stageQueryParam.getUserNumber(),
//            stageQueryParam.getQueryByDate(), stageQueryParam.getStartTime(), stageQueryParam.getEndTime());
//        ApplicantQueryVO applicantQueryVO = new ApplicantQueryVO();
//        applicantQueryVO.setApplicantStageVOList(StageConvertMapper.INSTANCE.stageConvertApplicantStageVO(stageList.getRecords()));
//        applicantQueryVO.setCurrent(stageList.getCurrent());
//        applicantQueryVO.setPages(stageList.getPages());
//        applicantQueryVO.setPageSize(stageList.getSize());
//        return ResponseResult.success(applicantQueryVO);
//    }
//    @ApiOperation(value = "查询入党积极分子阶段用户列表")
//    @GetMapping("/activist")
//    public ResponseResult<ActivistQueryVO> queryActivistStageList(StageQueryParam stageQueryParam) {
//        IPage<Stage> stageList = stageService.selectStageList(new Page<>(stageQueryParam.getPageNumber(),stageQueryParam.getPageSize()),ACTIVIST, stageQueryParam.getUserName(), stageQueryParam.getUserNumber(),
//            stageQueryParam.getQueryByDate(), stageQueryParam.getStartTime(), stageQueryParam.getEndTime());
//        ActivistQueryVO activistQueryVO = new ActivistQueryVO();
//        activistQueryVO.setActivistStageVOList(StageConvertMapper.INSTANCE.stageConvertActivistStageVO(stageList.getRecords()));
//        activistQueryVO.setCurrent(stageList.getCurrent());
//        activistQueryVO.setPages(stageList.getPages());
//        activistQueryVO.setPageSize(stageList.getSize());
//        return ResponseResult.success(activistQueryVO);
//    }
//    @ApiOperation(value = "查询发展对象阶段用户列表")
//    @GetMapping("/develop")
//    public ResponseResult<DevelopQueryVO> queryDevelopStageList(StageQueryParam stageQueryParam) {
//        IPage<Stage> stageList = stageService.selectStageList(new Page<>(stageQueryParam.getPageNumber(),stageQueryParam.getPageSize()),DEVELOP, stageQueryParam.getUserName(), stageQueryParam.getUserNumber(),
//            stageQueryParam.getQueryByDate(), stageQueryParam.getStartTime(), stageQueryParam.getEndTime());
//        DevelopQueryVO developQueryVO = new DevelopQueryVO();
//        developQueryVO.setDevelopStageVOList(StageConvertMapper.INSTANCE.stageConvertDevelopStageVO(stageList.getRecords()));
//        developQueryVO.setCurrent(stageList.getCurrent());
//        developQueryVO.setPages(stageList.getPages());
//        developQueryVO.setPageSize(stageList.getSize());
//        return ResponseResult.success(developQueryVO);
//    }
//    @ApiOperation(value = "查询预备党员阶段用户列表")
//    @GetMapping("/prepare")
//    public ResponseResult<PreparePMQueryVO> queryPrepareStageList(StageQueryParam stageQueryParam) {
//        IPage<Stage> stageList = stageService.selectStageList(new Page<>(stageQueryParam.getPageNumber(),stageQueryParam.getPageSize()),PREPARE, stageQueryParam.getUserName(), stageQueryParam.getUserNumber(),
//            stageQueryParam.getQueryByDate(), stageQueryParam.getStartTime(), stageQueryParam.getEndTime());
//        PreparePMQueryVO preparePMQueryVO = new PreparePMQueryVO();
//        preparePMQueryVO.setPrepareStageVOList(StageConvertMapper.INSTANCE.stageConvertPreparePMStageVO(stageList.getRecords()));
//        preparePMQueryVO.setCurrent(stageList.getCurrent());
//        preparePMQueryVO.setPages(stageList.getPages());
//        preparePMQueryVO.setPageSize(stageList.getSize());
//        return ResponseResult.success(preparePMQueryVO);
//    }
//
//    @ApiOperation(value = "获取当前发展状态")
//    @GetMapping("/state")
//    public ResponseResult<StageStateVO> getPersonalState(String userNumber) {
//        Stage stage = stageService.selectStageByNumber(userNumber);
//        return ResponseResult.success(StageConvertMapper.INSTANCE.stageConvertStageStateVO(stage));
//    }
//
//    @ApiOperation(value = "编辑入党申请人阶段(已停用)")
//    @PostMapping("/applicant")
//    public ResponseResult<Object> editApplicantStage(ApplicantStageParam applicantStageParam) {
////        List<Stage> stageList = StageUtil.applicantStageParamtoStageList(applicantStageParam);
////        return ResponseResult.success(stageService.editStageList(stageList));
//        return null;
//    }
//
//    @ApiOperation(value = "编辑入党积极分子阶段(已停用)")
//    @PostMapping("/activist")
//    public ResponseResult<Object> editActivistStage(ActivistStageParam activistStageParam) {
////        List<Stage> stageList = StageUtil.activistStageParamtoStageList(activistStageParam);
////        return ResponseResult.success(stageService.editStageList(stageList));
//        return null;
//    }
//
//    @ApiOperation(value = "编辑发展对象阶段(已停用)")
//    @PostMapping("/develop")
//    public ResponseResult<Object> editDevelopStage(DevelopStageParam developStageParam) {
////        List<Stage> stageList = StageUtil.developStageParamtoStageList(developStageParam);
////        return ResponseResult.success(stageService.editStageList(stageList));
//        return null;
//    }
//
//    @ApiOperation(value = "编辑预备党员阶段(已停用)")
//    @PostMapping("/prepare")
//    public ResponseResult<Object> editPreparePMStage(PreparePMStageParam preparePMStageParam) {
////        List<Stage> stageList = StageUtil.preparePMStageParamtoStageList(preparePMStageParam);
////        return ResponseResult.success(stageService.editStageList(stageList));
//        return null;
//    }
//
//
//    @ApiOperation(value = "删除阶段信息")
//    @PostMapping("/delete")
//    public ResponseResult<Object> deleteStageList(@RequestBody Map<String,Object> userNumber) {
//        List<String> userNumber1 = (List<String>) userNumber.get("userNumber");
//        return ResponseResult.success(stageService.deleteStageList(userNumber1));
//    }
//
//
//}
