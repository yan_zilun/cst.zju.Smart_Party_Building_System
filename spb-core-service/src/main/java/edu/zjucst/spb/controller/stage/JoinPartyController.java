package edu.zjucst.spb.controller.stage;

import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.TbFile;
import edu.zjucst.spb.domain.entity.flow.joinParty.MyProcessTask;
import edu.zjucst.spb.domain.param.stage.joinParty.JoinPartyTaskListParam;
import edu.zjucst.spb.domain.param.stage.joinParty.TODOTaskQueryParam;
import edu.zjucst.spb.domain.vo.flow.joinParty.MyProcessTaskVO;
import edu.zjucst.spb.service.FileService;
import edu.zjucst.spb.service.joinParty.JoinPartyFlowService;
import edu.zjucst.spb.util.FileUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

//@RestController
@Slf4j
//@Api(value = "入党流程相关接口(弃用)", tags = {"入党流程相关接口"})
//@RequestMapping("/joinParty")
public class JoinPartyController {

//    @Autowired
    private JoinPartyFlowService joinPartyFlowService;

//    @Autowired
    // 假设模板文件相对路径（实际应替换为你自己的文件路径）
    private static final String TEMPLATE_FILE_PATH = "static/thinkReport.docx";

//    @Autowired
    private FileService fileService;

    @ApiOperation("申请入党")
    @PostMapping("/process")
    public ResponseResult<Object> startJoinPartyProcess(@ApiParam("学工号[测试用]") @Validated String userId, @ApiParam("支部名称[测试用]") @Validated String branchName) {
        joinPartyFlowService.startProcess(userId, branchName);
        return ResponseResult.success();
    }


    @ApiOperation("查询申请入党流程状态")
    @GetMapping("/process")
    public ResponseResult<MyProcessTask> queryMyProcess(@ApiParam("学工号[测试用]") @Validated String userId) {
//        joinPartyFlowService.queryMyCreateProcess(userId);
        return ResponseResult.success(joinPartyFlowService.queryMyCreateProcess(userId));
    }

    @ApiOperation("党委发送接收入党积极分子信号")
    @PostMapping("/acceptActivist")
    public ResponseResult<Object> acceptActivist(@ApiParam("学工号[测试用]") @Validated String userId) {
        joinPartyFlowService.acceptActivist(userId);
        return ResponseResult.success();
    }

    @ApiOperation("提交思想汇报")
    @PostMapping("/thoughtReport")
    public ResponseResult<Object> uploadThoughtReport(@ApiParam("学工号[测试用]") @Validated String userId, @ApiParam("文件id") @Validated Integer fileId) {
        joinPartyFlowService.startUploadThoughtReport(userId, fileId);
        return ResponseResult.success();
    }

    @ApiOperation("查询当前需要处理的申请入党流程任务")
    @GetMapping("/process/todo")
    public ResponseResult<MyProcessTaskVO> queryTODOProcessTask(TODOTaskQueryParam param) {
//        joinPartyFlowService.queryMyTODOProcess(branchId);
        return ResponseResult.success(joinPartyFlowService.queryMyTODOProcess(param.getPageNumber(), param.getPageSize(), param.getBranchName()));
    }

    @ApiOperation("根据任务id审批任务节点")
    @PostMapping("/process/audit")
    public ResponseResult<Object> completeTask(JoinPartyTaskListParam param) {
        // TODO 权限验证
//        joinPartyFlowService.simpleCompleteTaskByTaskId(taskId, accept);
        joinPartyFlowService.completeTaskByTaskId(param);
        return ResponseResult.success();
    }

    @GetMapping("/download/docx")
    @ApiOperation("下载思想汇报文档")
    public ResponseEntity<Resource> downloadTemplate() throws IOException {
        Resource resource = new ClassPathResource(TEMPLATE_FILE_PATH);
        if (!resource.exists()) {
            throw new FileNotFoundException("Template file not found");
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"thinkReport.docx\"");
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

        InputStream inputStream = resource.getInputStream();
        return ResponseEntity.ok()
            .headers(headers)
            .body(new InputStreamResource(inputStream));
    }
//    @GetMapping("/file/{taskId}")
//    @ApiOperation("获取当前任务上传的文件")
//    public ResponseEntity<byte[]> getFileByTaskId(@PathVariable("taskId") String taskId) throws IOException {
//        TbFile file = fileService.getFileById(joinPartyFlowService.queryFileIdByTaskId(taskId));
//        return FileUtil.getResponseEntity(file.getName(), file.getPath());
//    }
}
