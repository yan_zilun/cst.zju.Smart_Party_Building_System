package edu.zjucst.spb.controller.authority;

import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Branch;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.entity.Role;
import edu.zjucst.spb.service.BranchService;
import edu.zjucst.spb.service.SpbUserService;
import edu.zjucst.spb.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author wyx
 * @date 2023-05-22 19:52
 **/
@Api(value = "人员信息管理", tags = {"模型管理"})
@RestController
@RequestMapping("/api/auth/user")
public class UserAuthController {

    @Autowired
    private SpbUserService spbUserService;
    @Autowired
    private BranchService branchService;
    @Autowired
    private RoleService roleService;

    @ApiOperation("设置职位")
    @PostMapping("/assignBranchAndRole")
    public ResponseResult<Object> assignSecretary(Long userId, String branchName, String roleName) {
        // 1. 找到指定的branch
        // 2. 校验 userId 是否在branch 中
        // 3. 将 userId 设置为 branch 的书记
        // 3.1 维护 业务的库表
        // 3.2 维护 flowable group
        SpbUser user = new SpbUser();
        user.setId(userId);
        List<SpbUser> spbUserList = spbUserService.findUser(user);
        user = spbUserList.get(0);

        Branch branch = new Branch();
        branch.setBranchName(branchName);
        List<Branch> branchList = branchService.findBranch(branch);
        branch = branchList.get(0);

        Role role = new Role();
        role.setRoleName(roleName);
        List<Role> roleList = roleService.findRole(role);
        role = roleList.get(0);

        if (user.getPartyBranch() == null
            || !user.getPartyBranch().equals(branch.getBranchName())) {
            // 如果该学生不在任何党支部中，或者在其他党支部中
            user.setPartyBranch(branch.getBranchName());
            user.setRoleName(roleName);
            return spbUserService.updateUser(user);
        } else {
            // 如果已经在该党支部，但是权限名称不对
            if (user.getRoleName() == null || !user.getRoleName().equals(roleName)) {
                user.setRoleName(roleName);
                return spbUserService.updateUser(user);
            }
        }
        return ResponseResult.success();
    }

    @ApiOperation("设置所在支部")
    @PostMapping("/assignBranch")
    public ResponseResult<Object> adParamBranch(Long userId, String branchName) {
        // 1. 找到指定的branch
        // 2. 校验是否能够加入到该支部  really needed ?
        // 3. 更新 user 所在的 branchId
        SpbUser user = new SpbUser();
        user.setId(userId);
        List<SpbUser> spbUserList = spbUserService.findUser(user);
        user = spbUserList.get(0);

        Branch branch = new Branch();
        branch.setBranchName(branchName);
        List<Branch> branchList = branchService.findBranch(branch);
        branch = branchList.get(0);

        // 如果该学生不在任何党支部中，或者在其他党支部中
        if (user.getPartyBranch() == null
            || !user.getPartyBranch().equals(branch.getBranchName())) {
            user.setPartyBranch(branch.getBranchName());
            user.setRoleName("");
            return spbUserService.updateUser(user);
        } else {
            // 如果已经在该党支部
            return ResponseResult.success();
        }
    }
}
