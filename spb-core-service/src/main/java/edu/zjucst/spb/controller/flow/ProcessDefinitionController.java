//package edu.zjucst.spb.controller.flow;
//
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.flow.DeployProcessDefinitionParam;
//import edu.zjucst.spb.service.FlowableService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * @author yzl
// * @date 2023/11/28 13:19
// */
//@RestController
//@Api(value = "流程定义接口", tags = {"流程定义接口"})
//@RequestMapping("/flow/processDefinition")
//public class ProcessDefinitionController {
//    @Autowired
//    private FlowableService flowableService;
//
//    @ApiOperation(value = "部署流程定义(不对外开放)")
//    @PostMapping("/deploy")
//    public ResponseResult<Object> deployProcessDefinition(DeployProcessDefinitionParam deployProcessDefinitionParam){
//        flowableService.deployProcessDefinition(deployProcessDefinitionParam.getFile(), deployProcessDefinitionParam.getName());
//        return ResponseResult.success();
//    }
//
//    @ApiOperation(value = "删除流程定义(不对外开放)")
//    @DeleteMapping("/delete")
//    public ResponseResult<Object> deleteProcessDefinition(String id, boolean cascade){
//        flowableService.deleteProcessDefinition(id ,cascade);
//        return ResponseResult.success();
//    }
//
//}
