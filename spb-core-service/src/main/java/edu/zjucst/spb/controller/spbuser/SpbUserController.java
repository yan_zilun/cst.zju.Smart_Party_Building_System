package edu.zjucst.spb.controller.spbuser;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.PageQry;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.vo.PersonalInformationVO;
import edu.zjucst.spb.domain.vo.SelectUserPageVO;
import edu.zjucst.spb.service.SpbUserService;
import edu.zjucst.spb.util.converMapper.UserConvertMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用户接口
 *
 * @author wyx
 * @date 2023/3/21 14:10
 **/
@RestController
@Slf4j
@Api(value = "个人信息相关接口", tags = {"个人信息相关接口"})
@ApiSupport(author = "wyx")
@RequestMapping("/users")
public class SpbUserController {
    @Autowired
    private SpbUserService spbUserService;

    /**
     * 用户第一次登录自动注册新用户信息
     **/
    @ApiOperation("自动注册用户")
    @PostMapping("/")
    public ResponseResult<Integer> addUser(@RequestBody @Validated SpbUser spbUser) {
        return ResponseResult.success(spbUserService.addUser(spbUser));
    }

    /**
     * 系统管理员根据id删除用户
     **/
    @ApiOperation("删除用户")
    @DeleteMapping("/deleteUser")
    public ResponseResult<Object> deleteUser(@RequestBody @Validated SpbUser spbUser) {
        return spbUserService.deleteUser(spbUser.getId());
    }

    /**
     * 系统管理员根据id批量删除用户
     **/
    @ApiOperation("批量删除用户")
    @DeleteMapping("/")
    public ResponseResult<Object> deleteUsers(@RequestBody @Validated List<Long> spbUserList) {
        return spbUserService.deleteUsers(spbUserList);
    }

    /**
     * 学生用户修改个人信息，支部书记/系统管理员修改用户权限
     **/
    @ApiOperation("修改用户")
    @PostMapping("/updateUser")
    public ResponseResult<Object> updateUser(@RequestBody @Validated SpbUser spbUser) {
        return spbUserService.updateUser(spbUser);
    }

    /**
     * 支部书记/系统管理员批量修改用户权限
     **/
    @ApiOperation("批量修改用户")
    @PutMapping("/")
    public ResponseResult<Object> updateUsers(@RequestBody @Validated List<SpbUser> spbUserList) {
        return spbUserService.updateUsers(spbUserList);
    }

    /**
     * 学生用户查看自己的信息，支部书记/系统管理员根据查询条件查询用户信息
     **/
    @ApiOperation("查询用户")
    @GetMapping("/findUser")
    public ResponseResult<List<SpbUser>> findUser(Long id, String userNumber,String userName, String roleName, String registerTime) throws ParseException {
        SpbUser spbUser = new SpbUser();
        spbUser.setId(id);
        spbUser.setUserNumber(userNumber);
        spbUser.setUsername(userName);
        spbUser.setRoleName(roleName);

        if (registerTime != null) {
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = ft.parse(registerTime);
            spbUser.setRegisterTime(date);
        }

        return ResponseResult.success(spbUserService.findUser(spbUser));
    }

    @ApiOperation(value = "活动模块根据发展阶段选择参与人员/已提交人员")
    @GetMapping("/activities/phase")
    public ResponseResult<SelectUserPageVO> selectUserByPhase(PageQry pageQry, String partyBranch, String developmentPhase){
        IPage<SpbUser> iPage = spbUserService.selectByPhase(new Page<>(pageQry.getPageNumber(), pageQry.getPageSize()), partyBranch, developmentPhase);
        return ResponseResult.success(spbUserToSelectUserPageVO(iPage));
    }

    @ApiOperation(value = "活动模块根据提交状态/参与状态选择参与人员")
    @GetMapping("/activities/state")
    public ResponseResult<SelectUserPageVO> selectUserByState(PageQry pageQry,String partyBranch, String activityNumber,String state){
        final IPage<SpbUser> iPage;
        if(state==null){
            iPage = spbUserService.selectByPhase(new Page<>(pageQry.getPageNumber(), pageQry.getPageSize()), partyBranch, null);
        }else{
            iPage = spbUserService.selectByState(new Page<>(pageQry.getPageNumber(), pageQry.getPageSize()), partyBranch, activityNumber, state);
        } return ResponseResult.success(spbUserToSelectUserPageVO(iPage));
    }

    @ApiOperation(value = "发展阶段模块根据权限选择联系人/培养人")
    @GetMapping("/select/role")
    public ResponseResult<SelectUserPageVO> selectUserByRole(PageQry pageQry,String partyBranch, String roleName) {
        IPage<SpbUser> iPage = spbUserService.selectByRoleName(new Page<>(pageQry.getPageNumber(), pageQry.getPageSize()), partyBranch, roleName);
        return ResponseResult.success(spbUserToSelectUserPageVO(iPage));
    }

    @ApiOperation(value = "获取个人信息")
    @GetMapping("/info")
    public ResponseResult<PersonalInformationVO> getPersonalInformation(String userNumber) {
        SpbUser user = spbUserService.findUserByNumber(userNumber);
        PersonalInformationVO personalInformationVO = UserConvertMapper.INSTANCE.spbUserConvertPersonalInformationVO(user);
        return ResponseResult.success(personalInformationVO);
    }


    private SelectUserPageVO spbUserToSelectUserPageVO(IPage<SpbUser> iPage){
        SelectUserPageVO selectUserPageVO = new SelectUserPageVO();
        selectUserPageVO.setUserVOList(UserConvertMapper.INSTANCE.spbUserConvertSelectUserVO(iPage.getRecords()));
        selectUserPageVO.setCurrent(iPage.getCurrent());
        selectUserPageVO.setPages(iPage.getPages());
        selectUserPageVO.setPageSize(iPage.getSize());
        return selectUserPageVO;
    }

}
