//package edu.zjucst.spb.controller.message;
//
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.enums.OptStatusEnum;
//import edu.zjucst.spb.util.SseEmitterUtil;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
//
///**
// * @author MrLi
// * @date 2023-04-04 21:25
// * 接收 页面实时消息推送
// */
//@Api(value = "SSE实时消息", tags = {"站内信消息"})
//@RequestMapping("/sse")
//@Controller
//public class SSEController {
//
//    @GetMapping("/send")
//    @ApiOperation("SSE测试页面-发送")
//    public String get() {
//        return "sse_send";
//    }
//
//    @GetMapping("/recv")
//    @ApiOperation("SSE测试页面-接收")
//    public String recv() {
//        return "sse_recv";
//    }
//
//    /**
//     * 用于创建连接
//     */
//    @GetMapping("/connect/{userId}")
//    @ResponseBody
//    @ApiOperation("连接")
//    public SseEmitter connect(@PathVariable String userId) {
//        return SseEmitterUtil.connect(userId);
//    }
//
//    /**
//     * 推送给所有人
//     *
//     * @param message
//     * @return
//     */
//    @GetMapping("/push/{message}")
//    @ResponseBody
//    @ApiOperation("推送消息")
//    public ResponseResult<OptStatusEnum> push(@PathVariable(name = "message") String message) {
//        //获取连接人数
//        int userCount = SseEmitterUtil.getUserCount();
//        //如果无在线人数，返回
//        if (userCount < 1) {
//            return ResponseResult.error(OptStatusEnum.OPT_ERROR);
//        }
//        SseEmitterUtil.batchSendMessage(message);
//        return ResponseResult.success(OptStatusEnum.SUCCESS);
//    }
//
//    /**
//     * 发送给单个人
//     *
//     * @param message
//     * @param userid
//     * @return
//     */
//    @GetMapping("/push_one/{messsage}/{userid}")
//    @ResponseBody
//    @ApiOperation("发送给指定用户")
//    public ResponseResult<Object> pushOne(@PathVariable(name = "message") String message, @PathVariable(name = "userid") String userid) {
//        SseEmitterUtil.sendMessage(userid, message);
//        return ResponseResult.success("推送消息给" + userid);
//    }
//
//    /**
//     * 关闭连接
//     */
//    @GetMapping("/close/{userid}")
//    @ResponseBody
//    @ApiOperation("关闭指定用户的连接")
//    public ResponseResult<Object> close(@PathVariable("userid") String userid) {
//        SseEmitterUtil.removeUser(userid);
//        return ResponseResult.success("连接关闭");
//    }
//}
