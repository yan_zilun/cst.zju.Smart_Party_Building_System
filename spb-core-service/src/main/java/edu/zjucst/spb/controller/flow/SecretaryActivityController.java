//package edu.zjucst.spb.controller.flow;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * @author MrLi
// * @date 2022-12-30 17:01
// **/
//@RestController
//@Api(value = "书记提交活动相关接口", tags = {"活动审批流程"})
//@RequestMapping("/flow/activity/secretary")
//public class SecretaryActivityController {
//
//    @ApiOperation(value = "书记提交活动审批")
//    @PostMapping()
//    public void submit() {
//        // 1. 获得当前用户的ID
//
//        // 2. 根据传入的DTO得到级别
//
//        // 3. 开启审批流程
//
//    }
//
//
//    @ApiOperation(value = "获得自己提交的审批")
//    @GetMapping()
//    public void getSubmittedActivity() {
//        // 1. 获得当前用户的ID
//
//        // 2. 查询
//
//    }
//
//
//
//
//}
