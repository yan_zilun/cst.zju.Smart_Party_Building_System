package edu.zjucst.spb.controller.message;

/**
 * @author MrLi
 * @date 2023-04-04 22:29
 * 消息推送: from https://cloud.tencent.com/developer/article/1684449
 */
public class InfoController {
    /**
     * 查看系统推送消息
     */

    /**
     * 给指定用户推送消息
     */


    /**
     * 给指定用户组推送消息 ==> 数量比较多需要考虑全部发送成功 ==> MQ?
     */


    /**
     * 查看自己推送的消息
     */


    /**
     * 查看自己接收的消息
     */
}
