package edu.zjucst.spb.controller.activity;

import com.alibaba.fastjson2.JSONObject;
import edu.zjucst.spb.service.PartyHonorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zyy
 * @version 1.0
 * @date 2024/3/9 18:25
 */
@RestController
public class PartyHonorController {
    @Autowired
    PartyHonorService partyHonorService;
    @RequestMapping("/partyhonor/list/")
    public JSONObject listPartyHonor(){
        return partyHonorService.listPartyHonor();
    }
}
