//package edu.zjucst.spb.controller.message;
//
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.entity.message.UserMessageText;
//import edu.zjucst.spb.service.message.UserMessageTextService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.*;
//
//import javax.annotation.Resource;
//import java.io.Serializable;
//import java.util.List;
//
///**
// * (UserMessageText)表的Controller控制层
// *
// * @author Mrli
// * @since 2023-04-05 14:10:48
// */
//@RestController
//@RequestMapping("/userMessageText")
//@Api(value = "userMessageText", tags = {"站内信消息"})
//public class UserMessageTextController {
//    /**
//     * 服务对象
//     */
//    @Resource
//    private UserMessageTextService userMessageTextService;
//
//    /**
//     * 分页查询所有数据
//     *
//     * @param page            分页对象
//     * @param userMessageText 查询实体
//     * @return 所有数据
//     */
//    @GetMapping("/")
//    @ApiOperation("分页批量查询userMessageText")
//    public ResponseResult selectAll(Page<UserMessageText> page, UserMessageText userMessageText) {
//        return ResponseResult.success(userMessageTextService.page(page, new QueryWrapper<>(userMessageText)));
//    }
//
//    /**
//     * 通过主键查询单条数据
//     *
//     * @param id 主键
//     * @return 单条数据
//     */
//    @GetMapping("/{id}")
//    @ApiOperation("根据主键查询userMessageText")
//    public ResponseResult selectOne(@PathVariable Serializable id) {
//        return ResponseResult.success(userMessageTextService.getById(id));
//    }
//
//    /**
//     * 新增数据
//     *
//     * @param userMessageText 实体对象
//     * @return 新增结果
//     */
//    @PostMapping("/")
//    @ApiOperation("单条插入userMessageText")
//    public ResponseResult insert(@RequestBody UserMessageText userMessageText) {
//        return ResponseResult.success(userMessageTextService.save(userMessageText));
//    }
//
//    /**
//     * 修改数据
//     *
//     * @param userMessageText 实体对象
//     * @return 修改结果
//     */
//    @PutMapping("/")
//    @ApiOperation("修改userMessageText")
//    public ResponseResult update(@RequestBody UserMessageText userMessageText) {
//        return ResponseResult.success(userMessageTextService.updateById(userMessageText));
//    }
//
//    /**
//     * 删除数据
//     *
//     * @param idList 主键结合
//     * @return 删除结果
//     */
//    @DeleteMapping("/")
//    @ApiOperation("删除userMessageText")
//    public ResponseResult delete(@RequestParam("idList") List<Long> idList) {
//        return ResponseResult.success(userMessageTextService.removeByIds(idList));
//    }
//}
//
