package edu.zjucst.spb.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 为了临时展示首页内容
 * @author MrLi
 * @date 2022-12-04 10:57
 **/
@RestController
@RequestMapping("/")
@Api(value = "首页", tags = {"非业务接口"})
@ApiSupport(author = "mrli")
public class IndexController {
    @GetMapping
    @ResponseBody
    @ApiOperation("首页欢迎")
    public ResponseResult<Object> index() {
        return ResponseResult.success("浙江大学软件学院智慧党建系统,欢迎您~\n目前正处网站建设阶段, 请持续关注");
    }
}
