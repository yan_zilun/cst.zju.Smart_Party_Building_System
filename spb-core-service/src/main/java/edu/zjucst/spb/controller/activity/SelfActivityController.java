package edu.zjucst.spb.controller.activity;


import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.param.activity.AddBranchActivityParam;
import edu.zjucst.spb.domain.param.activity.AddSelfActivityParam;
import edu.zjucst.spb.domain.param.activity.GetSelfActivityParam;
import edu.zjucst.spb.domain.vo.MySelfActivityVO;
import edu.zjucst.spb.enums.OptStatusEnum;
import edu.zjucst.spb.service.SelfActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 自主活动相关接口
 * @author qingo
 * @since 2023-04-18
 */
@RestController
@Slf4j
@Api(value = "自主活动相关接口", tags = {"自主活动相关接口"})
@ApiSupport(author = "qingo")
@RequestMapping("/self-activity")
public class SelfActivityController {

    @Autowired
    private SelfActivityService selfActivityService;

    @ApiOperation("学生添加/更新自主活动")
    @PostMapping("/addSelfActivity")
    public ResponseResult addSelfActivity(@RequestBody @Validated AddSelfActivityParam dto) {
        if (dto.getActivityLevel() != 0 && dto.getActivityLevel() != 1) {
            return ResponseResult.error(OptStatusEnum.PARAM_ERROR.getCode(), "activityLevel参数错误");
        }
        selfActivityService.addSelfActivity(dto);
        return ResponseResult.success();
    }

    @ApiOperation("查询学生自己的所有自主活动")
    @GetMapping("/getMySelfActivity")
    public ResponseResult<List<MySelfActivityVO>> getMySelfActivityByPage(GetSelfActivityParam dto) {
        return ResponseResult.success(selfActivityService.getMySelfActivityByPage(dto));
    }

    @ApiOperation(value = "团委新增支部活动")
    @PostMapping("/addBranchActivity")
    public ResponseResult addBranchActivity(@RequestBody AddBranchActivityParam dto) {
        if (dto.getActivityLevel() != 0 && dto.getActivityLevel() != 1) {
            return ResponseResult.error(OptStatusEnum.PARAM_ERROR.getCode(), "activityLevel参数错误");
        }
        selfActivityService.addBranchActivity(dto);
        return ResponseResult.success();
    }

}
