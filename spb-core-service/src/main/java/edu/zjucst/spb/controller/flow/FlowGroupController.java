//package edu.zjucst.spb.controller.flow;
//
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.flow.CreateIDMGroupParam;
//import edu.zjucst.spb.service.FlowableService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.websocket.server.PathParam;
//
///**
// * @author yzl
// * @date 2023/11/28 13:18
// */
//@RestController
//@Api(value = "flowable用户组管理接口", tags = {"Flowable管理"})
//@RequestMapping("/flow/groups")
//public class FlowGroupController {
//    @Autowired
//    private FlowableService flowableService;
//
//    /**
//     * 添加flowable中的用户组
//     */
//    @ApiOperation(value = "添加flowable中的用户组")
//    @PostMapping("/")
//    public ResponseResult<Object> addGroup(@RequestBody CreateIDMGroupParam createIDMGroupParam){
//        flowableService.createIDMGroup(createIDMGroupParam.getName());
//        return ResponseResult.success();
//    }
//
//    /**
//     * 删除flowable中的用户组
//     */
//    @ApiOperation(value = "删除flowable中的用户组")
//    @DeleteMapping("/{id}")
//    public ResponseResult<Object> deleteGroup(@PathParam("id") String id){
//        flowableService.deleteIDMGroup(id);
//        return ResponseResult.success();
//    }
//
//
//}
