//package edu.zjucst.spb.controller.flow;
//
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.flow.CreateMembershipParam;
//import edu.zjucst.spb.domain.param.flow.DeleteMembershipParam;
//import edu.zjucst.spb.service.FlowableService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
///**
// * @author cy
// * @date 2023/4/26 14:57
// */
//@RestController
//@Api(value = "flowable用户关系管理接口", tags = {"Flowable管理"})
//@RequestMapping("/flow/memberships")
//public class FlowMembershipController {
//    @Autowired
//    private FlowableService flowableService;
//
//    /**
//     * 添加flowable中用户与用户组的关系
//     */
//    @ApiOperation(value = "添加flowable中用户与用户组的关系")
//    @PostMapping("/")
//    public ResponseResult<Object> addMembership(@RequestBody CreateMembershipParam createMembershipParam){
//        flowableService.relateIDMUser2Group(createMembershipParam.getUserId(), createMembershipParam.getGroupId());
//        return ResponseResult.success();
//    }
//
//    /**
//     * 删除flowable中用户与用户组的关系
//     */
//    @ApiOperation(value = "删除flowable中用户与用户组的关系")
//    @DeleteMapping("/")
//    public ResponseResult<Object> deleteMembership(@RequestBody DeleteMembershipParam deleteMembershipParam){
//        flowableService.unRelateIDMUser2Group(deleteMembershipParam.getUserId(), deleteMembershipParam.getGroupId());
//        return ResponseResult.success();
//    }
//
//
//}
