package edu.zjucst.spb.controller.message;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.message.SystemMessage;
import edu.zjucst.spb.service.message.SystemMessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;


/**
 * (SystemMessage)表控制层
 *
 * @author Mrli
 * @since 2023-04-05 13:34:53
 */
@RestController
@RequestMapping("/systemMessages")
@Slf4j
@Api(value = "系统消息推送", tags = {"系统公告相关接口"})
public class SystemMessageController {
    /**
     * 服务对象
     */
    @Resource
    private SystemMessageService systemMessageService;

    /**
     * 分页查询所有数据
     *
     * @param page          分页对象
     * @param systemMessage 查询实体
     * @return 所有数据
     */
    @GetMapping("/")
    @ApiOperation("获取所有系统信息")
    public ResponseResult<IPage<SystemMessage>> selectAll(Page<SystemMessage> page, SystemMessage systemMessage) {
        return ResponseResult.success(systemMessageService.page(page, new QueryWrapper<>(systemMessage)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/{id}")
    @ApiOperation("获取单条系统信息")
    public ResponseResult<SystemMessage> selectOne(@PathVariable Serializable id) {
        return ResponseResult.success(systemMessageService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param systemMessage 实体对象
     * @return 新增结果
     */
    @PostMapping("/")
    @ApiOperation("插入单条系统信息")
    public ResponseResult<Boolean> insert(@RequestBody SystemMessage systemMessage) {
        // 系统管理员ID默认为0
        Long userId = 0L;
        systemMessage.setSendUserId(userId);
        // 创建一条系统消息
        return ResponseResult.success(systemMessageService.save(systemMessage));
    }

    /**
     * 修改数据
     *
     * @param systemMessage 实体对象
     * @return 修改结果
     */
    @PutMapping("/")
    @ApiOperation("修改单条系统信息")
    public ResponseResult<Boolean> update(@RequestBody SystemMessage systemMessage) {
        return ResponseResult.success(systemMessageService.updateById(systemMessage));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/")
    @ApiOperation("删除单条系统信息")
    public ResponseResult<Boolean> delete(@RequestParam("idList") List<Long> idList) {
        return ResponseResult.success(systemMessageService.removeByIds(idList));
    }
}
