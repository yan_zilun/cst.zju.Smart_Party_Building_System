package edu.zjucst.spb.controller.stage.v2;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.stage.ApplicantStage;
import edu.zjucst.spb.domain.param.stage.v2.ApplicantStageQueryParam;
import edu.zjucst.spb.domain.param.stage.v2.ApplicantStageUpdateParam;
import edu.zjucst.spb.domain.vo.stage.v2.ApplicantStageVO;
import edu.zjucst.spb.domain.vo.stage.v2.StageListVO;
import edu.zjucst.spb.service.stage.ApplicantStageService;
import edu.zjucst.spb.util.converMapper.StageConvertMapperV2;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ZlunYan
 * @description
 * @create 2024-5-25
 */

@RestController
@Slf4j
@Api(value = "入党申请人发展阶段相关接口", tags = {"发展阶段相关接口"})
@RequestMapping("/stages/applicant")
public class ApplicantStageController {

    @Autowired
    private ApplicantStageService service;

    @ApiOperation("插入发展阶段")
    @PostMapping()
    public ResponseResult<Integer> addStage(String userNumber) {
        ApplicantStage stage = new ApplicantStage();
        stage.setUserNumber(userNumber);
        return ResponseResult.success(service.insertStage(stage));
    }

    @ApiOperation("查询stage")
    @GetMapping("/{userNumber}")
    public ResponseResult<ApplicantStageVO> queryStage(@PathVariable("userNumber") String userNumber) {
        return ResponseResult.success(StageConvertMapperV2.INSTANCE
            .stageConvertApplicantStageVO(service.getStageByUserId(userNumber)));
    }

    @ApiOperation(value = "查询Stage列表")
    @GetMapping()
    public ResponseResult<StageListVO<ApplicantStageVO>> queryStageList(ApplicantStageQueryParam param) {
        QueryWrapper<ApplicantStage> wrapper = new QueryWrapper<>();

        IPage<ApplicantStage> stageList = service.selectStageList(new Page<>(param.getPageNumber(),param.getPageSize()),
            wrapper);

        StageListVO<ApplicantStageVO> stageQueryVO = new StageListVO<>();
        stageQueryVO.setStageVOList(StageConvertMapperV2.INSTANCE.stageConvertApplicantStageVO(stageList.getRecords()));
        stageQueryVO.setCurrent(stageList.getCurrent());
        stageQueryVO.setPages(stageList.getPages());
        stageQueryVO.setPageSize(stageList.getSize());

        return ResponseResult.success(stageQueryVO);
    }

    @ApiOperation(value = "更新字段")
    @PostMapping("/update")
    public ResponseResult<Object> update(ApplicantStageUpdateParam param) {
        ApplicantStage stage = StageConvertMapperV2.INSTANCE.updateApplicantParamConvertStage(param);
        if (stage.getTalker() != null && stage.getTalker().equals("")) stage.setTalker(null);

        service.updateStage(stage);
        return ResponseResult.success();
    }
}
