package edu.zjucst.spb.controller.message;


import com.github.pagehelper.PageInfo;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.message.UserMessage;
import edu.zjucst.spb.domain.entity.message.UserMessageText;
import edu.zjucst.spb.domain.param.message.GetUserMessageParam;
import edu.zjucst.spb.domain.param.message.MessageQueryParam;
import edu.zjucst.spb.domain.vo.message.UserNotifyMessageVO;
import edu.zjucst.spb.enums.UserBusinessStatusEnum;
import edu.zjucst.spb.exception.NotLoginException;
import edu.zjucst.spb.service.message.UserMessageService;
import edu.zjucst.spb.util.SessionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.collections.BagUtils;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * (UserMessage)表的Controller控制层
 *
 * @author Mrli
 * @since 2023-04-05 14:17:20
 */
@RestController
@RequestMapping("/userMessages")
@Api(value = "站内信推送", tags = {"站内信消息"})
@RequiresAuthentication
public class UserMessageController {
    /**
     * 服务对象
     */
    @Resource
    private UserMessageService userMessageService;

    /**
     * 分页查询所有数据
     *
     * @return 所有数据
     */
    @GetMapping("/allmessage")
    @ApiOperation("分页批量查询自己的消息")
    public ResponseResult<List<UserMessage>> selectAll(@ApiParam(value = "分页查询参数") GetUserMessageParam param) throws NotLoginException {
        // 判断是否登录了, 否则抛出异常==>改成shiro控制
//        Optional.ofNullable(SessionUtil.getUserId()).orElseThrow(NotLoginException::new);
//        MessageQueryDTO dto = MessageQueryMapper.INSTANCE.param2dto(param);
//        dto.setRecvUserId(SessionUtil.getUserId());
        return ResponseResult.success(userMessageService.getUserMessageList(param));
    }

    @GetMapping("all/notread/message")
    @ApiOperation("获取当前用户所有未读消息")
    public ResponseResult<List<UserMessage>> selectAllNOReadMessage(@ApiParam(value="当前用户id") Long recvUserId){
        return ResponseResult.success(userMessageService.getAllNOReadMessage(recvUserId));
    }
    /**
     * 分页查询所有消息text
     *
     * @return 所有数据
     */
    @GetMapping("/allmessagetext")
    @ApiOperation("查询当前用户id的全部消息内容")
    public ResponseResult<List<UserMessageText>> selectAllText(@ApiParam(value = "分页查询参数") GetUserMessageParam param) throws NotLoginException {
        // 判断是否登录了, 否则抛出异常==>改成shiro控制
//        Optional.ofNullable(SessionUtil.getUserId()).orElseThrow(NotLoginException::new);
//        MessageQueryDTO dto = MessageQueryMapper.INSTANCE.param2dto(param);
//        dto.setRecvUserId(SessionUtil.getUserId());
        return ResponseResult.success(userMessageService.getUserMessageTextList(param));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/{id}")
    @ApiOperation("根据主键查询userMessage")
    public ResponseResult<UserMessage> selectOne(@PathVariable Serializable id) {
        UserMessage byId = userMessageService.getById(id);
        if (byId == null) {
            return ResponseResult.error(UserBusinessStatusEnum.MESSAGE_NOT_EXIST.getCode(), UserBusinessStatusEnum.MESSAGE_NOT_EXIST.getDescription());
        } else {
            return ResponseResult.success(byId);
        }
    }

    /**
     * 新增数据
     *
     * @return 新增结果
     */
    @PostMapping("/s2s")
    @ApiOperation("单发指定用户")
    public ResponseResult<Boolean> send(
        @ApiParam(value = "接收消息的用户Id", required = true) @RequestParam Long recvUserId,
        @ApiParam(value = "消息的具体内容", required = true) @RequestParam String content
    ) {
        // TODO: 标题+附件

        // 获得当前登录的用户ID
        Optional.ofNullable(SessionUtil.getUserId()).orElseThrow(NotLoginException::new);
        // 创建一条消息
        return userMessageService.sendMessage(SessionUtil.getUserId(), recvUserId, content);
    }

    @PostMapping("/s2g")
    @ApiOperation("群发消息")
    public ResponseResult<Object> senParamGroup(
        @RequestBody List<Long> sendUserIdList,
        @RequestParam String content
    ) {
        // 创建一条消息
        return userMessageService.sendMessageMessageToGroup(SessionUtil.getUserId(), sendUserIdList, content);
    }


    /**
     * 批量更新已读
     *
     * @param idList 需要批量变更的id
     * @return 返回结果
     * @DeleteMapping("/") + @RequestParam("idList") List<Long> idList 可行
     * @PutMapping("/") + @RequestParam("idList") List<Long> idList 不可行
     * ==> 统一使用 @RequestBody： [1,2]
     */
    @PutMapping("/")
    @ApiOperation("批量更新已读")
    public ResponseResult<Object> updateBatchById(
        @ApiParam(value = "需要更新的message Id列表", required = true) @RequestBody List<Long> idList) {
        List<Long> list = userMessageService.updateBatchByIdList(SessionUtil.getUserId(), idList);
        if (list != null && list.size() > 0) {
            return ResponseResult.error(UserBusinessStatusEnum.MESSAGE_NOT_EXIST.getCode(), list + UserBusinessStatusEnum.MESSAGE_NOT_EXIST.getDescription());
        } else {
            return ResponseResult.success();
        }

    }


    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/")
    @ApiOperation("批量删除消息")
    public ResponseResult<Boolean> delete(
        @ApiParam(value = "需要更新的message Id列表", required = true) @RequestBody List<Long> idList) {
        List<Long> list = new ArrayList<>();
        List<Long> delList = new ArrayList<>();
        if (idList != null && idList.size() > 0) {
            for (Long l : idList) {
                UserMessage byId = userMessageService.getById(l);
                if (byId == null) {
                    list.add(l);
                } else {
                    delList.add(l);
                }
            }
        }
        userMessageService.removeByIds(delList);
        if (list != null && list.size() > 0)
        {
            return ResponseResult.error(UserBusinessStatusEnum.MESSAGE_NOT_EXIST.getCode(), list + UserBusinessStatusEnum.MESSAGE_NOT_EXIST.getDescription());
        } else {
            return ResponseResult.success();
        }
    }
}

