package edu.zjucst.spb.controller.meeting;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.PageQry;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Meeting;
import edu.zjucst.spb.domain.vo.MeetingQueryVO;
import edu.zjucst.spb.service.MeetingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//
@RestController
@Slf4j
@Api(value = "组织生活相关接口", tags = {"组织生活相关接口"})
@ApiSupport(author = "zjh")
@RequestMapping("/meeting")
public class MeetingController {

    @Autowired
    private MeetingService meetingService;

    // 添加会议
    @ApiOperation("添加会议")
    @PostMapping("/add")
    public ResponseResult<Integer> addUser(@RequestBody Meeting meeting) {
        return ResponseResult.success(meetingService.addMeeting(meeting));
    }

    // 查询会议
    @ApiOperation(value = "查询会议")
    @GetMapping("/query")
    public ResponseResult<MeetingQueryVO> selectUserByState(PageQry pageQry, String organization, String topic, String claasification){
        final IPage<Meeting> iPage;
        iPage = meetingService.selectMeeting(new Page<>(pageQry.getPageNumber(), pageQry.getPageSize()), organization, topic, claasification);
        MeetingQueryVO meetingQueryVO = new MeetingQueryVO();
        meetingQueryVO.setMeetings(iPage.getRecords());
        meetingQueryVO.setCurrent(iPage.getCurrent());
        meetingQueryVO.setPageSize(iPage.getSize());
        meetingQueryVO.setPages(iPage.getPages());
        return ResponseResult.success(meetingQueryVO);
    }
}
