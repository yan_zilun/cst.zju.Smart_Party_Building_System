package edu.zjucst.spb.controller.authority;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Branch;
import edu.zjucst.spb.domain.vo.branch.MemberCountVO;
import edu.zjucst.spb.service.BranchService;
import edu.zjucst.spb.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author lx
 * @date 2023-12-22 19:52
 **/
@RestController
@Slf4j
@Api(value = "支部信息管理", tags = {"支部管理"})
@ApiSupport(author = "lx")
@RequestMapping("/api/auth/branch")
public class BranchAuthController {

    @Autowired
    private BranchService branchService;

    @Autowired
    private UserService userService;

    @ApiOperation("创建新的支部")
    @PostMapping("/addBranch")
    public ResponseResult<Integer> addNewBranch(@RequestBody @Validated Branch branch) {
        // 1. 添加业务上的branch
        // 2. 添加flow上的group
        /*
        * 創建新的支部
        * 条件:
        * 1.不允许有重复的支部名
        * 2.若parentBranchId不为空 则必须存在与支部表中，即parentBranchId必须有效
        * */
        //如果branchId存在与当前数据库 则将id设为空
        if(branchService.findBranchById(branch.getId())!=null){
            //设置了组件自增策略 设置为空主键会自动增长
            branch.setId(null);//防止后台因为重复的id而报错
        }
        //直接根据支部名获取支部
        List<Branch> branchList = branchService.findBranchByName(branch.getBranchName());
        if(branchList.size()>0){
            //此时代表此支部名重复
            return ResponseResult.error("10010", "添加的党支部名称已经存在于数据库中！");
        }
        //查询parentBranchId对应的支部是否存在
        if(branch.getParentBranchId()!=null&&branchService.findBranchById(branch.getParentBranchId())==null){
            //此时上级党支部不存在 需要重新查询并添加
            return ResponseResult.error("10011", "上级党支部不存在，请查询添加！");
        }
        return ResponseResult.success(branchService.addBranch(branch));
    }

    @ApiOperation("删除支部")
    @DeleteMapping("/deleteBranch")
    public ResponseResult<Integer> deleteBranch(@RequestBody @Validated Long branchId) {
        return ResponseResult.success(branchService.deleteBranch(branchId));
    }

    @ApiOperation("批量删除支部")
    @DeleteMapping("/deleteBranches")
    public ResponseResult<Integer> deleteBranches(@RequestBody @Validated List<Long> idList) {
        return ResponseResult.success(branchService.deleteBranches(idList));
    }

    @ApiOperation("修改支部信息")
    @PutMapping("/updateBranch")
    public ResponseResult<Object> updateBranch(@RequestBody @Validated Branch branch) {
        return ResponseResult.success(branchService.updateBranch(branch));
    }

    @ApiOperation("批量修改支部信息")
    @PutMapping("/updateBranches")
    public ResponseResult<Object> updateBranches(@RequestBody @Validated List<Branch> branchList) {
        return ResponseResult.success(branchService.updateBranches(branchList));
    }

    @ApiOperation("查询支部信息")
    @GetMapping("/findBranches")
    public ResponseResult<List<Branch>> findBranches(Long branchId, String branchName) {
        Branch branch = new Branch();
        branch.setId(branchId);
        branch.setBranchName(branchName);
        return ResponseResult.success(branchService.findBranch(branch));
    }

    @ApiOperation("查询全部支部信息")
    @GetMapping("/findAllBranches")
    public ResponseResult<List<Branch>> findAllBranches() {
        return ResponseResult.success(branchService.findAllBranch());
    }

    // 统计当前支部的党员和预备党员人数
    @ApiOperation("统计当前支部的党员和预备党员人数")
    @GetMapping("/count/party")
    public ResponseResult<MemberCountVO> countPartyMember(String name){
        return ResponseResult.success(userService.countPartyMember(name));
    }
}
