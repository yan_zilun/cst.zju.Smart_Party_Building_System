package edu.zjucst.spb.controller.user;

import cn.hutool.core.util.IdUtil;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import com.wf.captcha.SpecCaptcha;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.vo.user.CaptchaVO;
import edu.zjucst.spb.enums.UserBusinessStatusEnum;
import edu.zjucst.spb.exception.BizException;
import edu.zjucst.spb.pkg.redis.UserRedisDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
@Validated
@Api(value = "图片验证码相关接口", tags = {"图片验证码相关接口"})
@ApiSupport(author = "qingo")
@RequestMapping("/captcha")
public class CaptchaController {
    public static final int MAX_REQUESTS_PER_MINUTE = 10;
    public static final int WINDOW_SIZE = 1; // 时间窗口大小（单位：分钟）
    public static final TimeUnit timeUnit = TimeUnit.MINUTES;
    public long expiration = 5;
    @Autowired
    private UserRedisDao userRedisDao;

    @ApiOperation("获取图片验证码")
    @GetMapping
    public ResponseResult<CaptchaVO> captcha(HttpServletRequest request, HttpServletResponse response) {
        /*
        * 1.首先，在Redis中存储用户请求验证码的计数器，以用户的唯一标识（例如用户ID或IP地址）作为键。
          2.每次用户请求验证码接口时，首先检查计数器的值。
          3.如果计数器的值小于设定的最大请求次数（例如10），则允许请求验证码，并将计数器加1。
          4.如果计数器的值达到最大请求次数，说明用户在一分钟内的请求次数已经超过限制，返回相应的错误信息。
          5.在每分钟结束时，可以清零计数器，以便下一分钟重新开始计数。
        * */
        String userId=request.getRequestedSessionId()+' '+request.getRemoteAddr();//对应用户在会话下的唯一标识
        Long count=userRedisDao.increment(userId, WINDOW_SIZE);
        if(count>MAX_REQUESTS_PER_MINUTE){
            return ResponseResult.error("1000","请求太频繁,请稍后重试");
        }
        SpecCaptcha specCaptcha = new SpecCaptcha(130, 48);
        String verCode = specCaptcha.text();
        // 存入redis并设置过期时间, 将key和base64返回给前端
        CaptchaVO captchaVO = new CaptchaVO();
        System.out.println(verCode);
        log.info("{}",verCode);
        captchaVO.setKey(userRedisDao.setCaptchaKey(IdUtil.randomUUID(), verCode.toLowerCase(), expiration, timeUnit));
        captchaVO.setImage(specCaptcha.toBase64());
        return ResponseResult.success(captchaVO);
    }

    @ApiOperation("校验图片验证码")
    @GetMapping("/verify")
    public ResponseResult<Object> verifyCaptcha(@RequestParam @NotBlank String verKey, @RequestParam @NotBlank String verCode) {
        try {
            // 获取redis中的验证码
            String redisCode = userRedisDao.get(verKey);
            log.info("{} {} {}",verKey,verCode,redisCode);
            if(redisCode==null){
                //验证码已经过期
                throw new BizException(UserBusinessStatusEnum.VERIFICATION_EXPIRED.getCode(), UserBusinessStatusEnum.VERIFICATION_EXPIRED.getDescription());
            }
            if (!verCode.trim().toLowerCase().equals(redisCode)) {
                //验证码错误
                throw new BizException(UserBusinessStatusEnum.VERIFICATION_ERROR.getCode(), UserBusinessStatusEnum.VERIFICATION_ERROR.getDescription());
            }
            //校验成功后需要删除校验码
            userRedisDao.remove(verKey);
            return ResponseResult.success();
        }catch (RedisConnectionFailureException e) {
            // 处理Redis连接失败的情况
            log.error("Redis连接失败: {}", e.getMessage());
            throw new BizException(UserBusinessStatusEnum.REDIS_CONNECTION_FAILURE.getCode(), UserBusinessStatusEnum.REDIS_CONNECTION_FAILURE.getDescription());
        }
    }
}
