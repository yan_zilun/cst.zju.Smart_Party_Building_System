package edu.zjucst.spb.stage;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.controller.stage.StageController;
import edu.zjucst.spb.domain.ResponseResult;
import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.domain.param.stage.StageQueryParam;
import edu.zjucst.spb.domain.vo.stage.NewStageQueryVO;
import edu.zjucst.spb.domain.vo.stage.NewStageVO;
import edu.zjucst.spb.domain.vo.stage.StageInsertVO;
import edu.zjucst.spb.service.StageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class StageUnitTest {

    @Autowired
    private StageController stageController;

    @Test
    void testInsertStage() throws ParseException {
        StageInsertVO stageInsertVO = new StageInsertVO();
        stageInsertVO.setUserNumber("22351236");
        stageInsertVO.setUserName("严梓伦");
        stageInsertVO.setDevelopmentPhaseId(0);

        String birthStr = "2001-01-03";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        stageInsertVO.setBirthday(sdf.parse(birthStr));

        stageController.addStage(stageInsertVO);
    }



    @Test
    void queryStage() {
        StageQueryParam param = new StageQueryParam();
        param.setUserNumber("22351236");

        ResponseResult<NewStageQueryVO> result = stageController.queryStageList(param);
        List<NewStageVO> stageVOList = result.getData().getStageVOList();
        for (NewStageVO stage:stageVOList) {
            System.out.println("stage.getUserNumber() = " + stage.getUserNumber());
            System.out.println("stage.getUserName() = " + stage.getUserName());
            System.out.println("stage.getDevelopmentPhase() = " + stage.getDevelopmentPhase());
            System.out.println("stage.getBirthday() = " + stage.getBirthday());
            System.out.println("stage.getDeliveryTime() = " + stage.getDeliveryTime());
        }
    }

    @Test
    void test() {

}

}
