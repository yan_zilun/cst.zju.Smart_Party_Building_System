package edu.zjucst.spb.stage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import edu.zjucst.spb.controller.stage.v2.ApplicantStageController;
import edu.zjucst.spb.domain.entity.stage.ApplicantStage;
import edu.zjucst.spb.domain.param.stage.v2.ApplicantStageUpdateParam;
import edu.zjucst.spb.domain.vo.stage.v2.ApplicantStageVO;
import edu.zjucst.spb.domain.vo.stage.v2.StageListVO;
import edu.zjucst.spb.service.stage.ApplicantStageService;
import edu.zjucst.spb.util.converMapper.StageConvertMapperV2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author ZlunYan
 * @description
 * @create 2024-5-25
 */

@SpringBootTest
public class StageTestV2 {

    @Autowired
    ApplicantStageService service;

    @Autowired
    ApplicantStageController controller;

    @Test
    void testControllerUpdate() throws ParseException {
        ApplicantStageUpdateParam param = new ApplicantStageUpdateParam();
        param.setUserNumber("22351236");
        param.setTalker("");

        String date = "2001-01-03";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        param.setDeliveryTime(sdf.parse(date));

        controller.update(param);
    }

    @Test
    void testInsert() {
        ApplicantStage stage = new ApplicantStage();
        stage.setUserNumber("22351236");

        service.insertStage(stage);
    }

    @Test
    void testQuery() {
        QueryWrapper<ApplicantStage> wrapper = new QueryWrapper<>();

        IPage<ApplicantStage> stageList = service.selectStageList(new Page<>(1,5),
            wrapper);

        StageListVO<ApplicantStageVO> stageQueryVO = new StageListVO<>();
        stageQueryVO.setStageVOList(StageConvertMapperV2.INSTANCE.stageConvertApplicantStageVO(stageList.getRecords()));
        stageQueryVO.setCurrent(stageList.getCurrent());
        stageQueryVO.setPages(stageList.getPages());
        stageQueryVO.setPageSize(stageList.getSize());

        System.out.println(stageQueryVO);
    }

    @Test
    void testUpdate() throws ParseException {
        ApplicantStage stage = new ApplicantStage();
        stage.setUserNumber("22351236");
        stage.setTalker("");  // 这个如果为null则不会被改变

        String date = "2001-01-03";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        stage.setDeliveryTime(sdf.parse(date));

        service.updateStage(stage);
    }
}
