package edu.zjucst.spb;

import edu.zjucst.spb.controller.activity.ActivityController;
import edu.zjucst.spb.controller.stage.StageController;
import edu.zjucst.spb.dao.mapper.ActivityMapper;
import edu.zjucst.spb.dao.mapper.StageMapper;
import edu.zjucst.spb.domain.DevelopStateMachine;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.domain.param.stage.StageQueryParam;
import edu.zjucst.spb.service.SpbUserService;
import edu.zjucst.spb.util.EmailManager;
import edu.zjucst.spb.util.converMapper.UserConvertMapper;
import org.flowable.engine.ProcessEngine;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class CoreServiceApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private StageMapper stageMapper;

    @Autowired
    private ActivityMapper activityMapper;

    @Autowired
    private ActivityController activityController;

    @Autowired
    private SpbUserService spbUserService;

    @Autowired
    private StageController stageController;

    @Autowired
    private ProcessEngine processEngine;

    @Test
    public void testFlowable() {
        System.out.println("processEngine= " + processEngine);
    }

    @Test
    public void testInsert() {
//        Stage stage = new Stage();
//        stage.setUserId(1);
//        stage.setUserName("chb");
//        stage.setDevelopmentPhase("入党积极分子");
//        stage.setTalker("贝");
//        stage.setJoinYear(new Date(123));
//        stage.setRecommendationInterval(3);
//        stage.setTrainingDuration(new Time(123));
//        stage.setRecommendable("是");
//        stage.setRecommendationNotification("已通知");
//        int res = stageMapper.insert(stage);
//        System.out.println(res);

    }

    @Test
    public void testInsert1() {
        List<SpbUser> userList = new ArrayList<>();
        SpbUser       user     = new SpbUser();
        user.setUsername("chb");
        user.setPhone("123");
        user.setUserNumber("22250001");
        userList.add(user);
//        activityController.updateActivityList("测试","20230414005","推优活动",
//            "softmax",new Date(),"text",2,userList,userList,"测试");
//        activityController.addActivityList("测试","培训活动",
//            "softmax",new Date(),"text",2,userList,userList,"测试");

//        ResponseResult responseResult = activityController.selectUserByPhase("积极分子");
//        List<SpbUser> data = (List<SpbUser>) responseResult.getData();


//        ResponseResult responseResult = activityController.queryActivityList("团员", null, null);
//        List<Activity> activity = (List<Activity>) responseResult.getData();
//        SpbUser user1 = spbUserService.findUserByNumber("22250001");
//        System.out.println(user1);
//        Stage stage = new Stage();
//        stage.setId(123);
//        stage.setUserNumber("321");
//        stage.setUserName("chbbb");
//        stage.setDevelopmentPhase("tuanyuan");
//        stage.setIsLeague("shi");
//        stage.setDeliveryTime(new Date());
//        stage.setTalkRegistrationTime(new Date());
//        stage.setTalker("sui");
//        stage.setQualificationInterval("6个月");
//        stage.setIsPromote("是");
//
//        stage.setPromoteTime(new Date());
//        stage.setActivistTime(new Date());
//        stage.setThoughtReport("shibin");
//        stage.setEducationalVisit("sd");
//        stage.setActivistPartyTraining("sd");
//        stage.setCultivateContacts("sd");
//        stage.setDevelopmentPublicTime(new Date());
//        stage.setTeacherTime(new Date());

//        ApplicantStageVO applicantStageVO = StageConvertMapper.INSTANCE.stageConvertApplicantStageVO(stage);

//        ActivistStageVO activistStageVO = StageConvertMapper.INSTANCE.stageConvertActivistStageVO(stage);

        SpbUser spbUser = new SpbUser();
        spbUser.setUserNumber("123");
        spbUser.setUsername("chb");
        spbUser.setPhone("123");

        UserConvertMapper.INSTANCE.spbUserConvertSelectUserVO(spbUser);


//        ResponseResult responseResult = activityController.queryActivityList("团员", null, null);
//        List<Activity> activity = (List<Activity>) responseResult.getData();

    }

    @Test
    public void testStateMachine() {
        DevelopStateMachine developStateMachine1 = DevelopStateMachine.ACTIVIST;

        DevelopStateMachine developStateMachine2 = DevelopStateMachine.PREPARE;
        Stage               stage                = new Stage();


        SpbUser spbUser = new SpbUser();
        spbUser.setUserNumber("123");
        spbUser.setUsername("chb");
        spbUser.setPhone("123");

        System.out.println(DevelopStateMachine.canStep(developStateMachine1, developStateMachine2));
    }

    @Test
    public void testApplicantStageController() {
//        ApplicantStageParam applicantStageParam = new ApplicantStageParam();
//        applicantStageParam.setIsLeague("是");
//        applicantStageParam.setDeliveryTime(new Date());
//        applicantStageParam.setDevelopmentPhase("入党申请人阶段");
//        applicantStageParam.setIsPromote("否");
//        applicantStageParam.setQualificationInterval("3个月");
//        applicantStageParam.setTalker("chb;chb;");
//        applicantStageParam.setTalkRegistrationTime(new Date());
//        UserVo user = new UserVo();
//        UserVo user1 = new UserVo();
//        user.setUsername("iso");
//        user.setUserNumber("22250001");
//        List<UserVo> spbUsers = new ArrayList<>();
//        spbUsers.add(user);
//        user1.setUsername("Test");
//        user1.setUserNumber("10000");
//        spbUsers.add(user1);
//        applicantStageParam.setUserList(spbUsers);
//        stageController.editApplicantStage(applicantStageParam);
//
//
//        StageQueryParam stageQueryParam = new StageQueryParam();
//        stageQueryParam.setUserName("iso");


        //stageQueryParam.set
    }


    @Test
    public void testActivistStageController() {
//        ActivistStageParam activistStageParam = new ActivistStageParam();
//        activistStageParam.setActivistTime(new Date());
//        activistStageParam.setCultivateContacts("chb;chb;");
//        activistStageParam.setDevelopmentPhase("入党积极分子阶段");
//        activistStageParam.setDevelopmentPublicTime(new Date());
//        activistStageParam.setEducationalVisit("1;2;");
//        activistStageParam.setPromoteTime(new Date());
//        activistStageParam.setTeacherTime(new Date());
//        activistStageParam.setThoughtReport("1;2;");
//        SpbUser user = new SpbUser();
//        SpbUser user1 = new SpbUser();
//        user.setUsername("iso");
//        user.setUserNumber("22250001");
//        List<SpbUser> spbUsers = new ArrayList<>();
//        spbUsers.add(user);
//        user1.setUsername("Test");
//        user1.setUserNumber("10000");
//        spbUsers.add(user1);
//        activistStageParam.setUserList(spbUsers);
//        stageController.editActivistStage(activistStageParam);


        StageQueryParam stageQueryParam = new StageQueryParam();
        stageQueryParam.setUserName("iso");
        //stageQueryParam.set
    }

    @Test
    public void testDevelopStageController() {
//        DevelopStageParam developStageParam = new DevelopStageParam();
//        developStageParam.setConfirmTime(new Date());
//        developStageParam.setDevelopmentPhase("发展对象阶段");
//        SpbUser user = new SpbUser();
//        SpbUser user1 = new SpbUser();
//        user.setUsername("iso");
//        user.setUserNumber("22250001");
//        List<SpbUser> spbUsers = new ArrayList<>();
//        spbUsers.add(user);
//        user1.setUsername("Test");
//        user1.setUserNumber("10000");
//        spbUsers.add(user1);
//        developStageParam.setUserList(spbUsers);
//        stageController.editDevelopStage(developStageParam);


        StageQueryParam stageQueryParam = new StageQueryParam();
        stageQueryParam.setUserName("iso");
        //stageQueryParam.set
    }


    @Autowired
    private EmailManager emailManager;
    @Test
    public void testEmail(){
        emailManager.sendSimpleMail(1L, "测试邮件", "测试123");
    }
}
