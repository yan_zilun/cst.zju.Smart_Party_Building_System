//package edu.zjucst.spb.flow;
//
//import edu.zjucst.spb.controller.flow.*;
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.flow.*;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@SpringBootTest
//public class FlowUnitTest {
//
//    @Autowired
//    private FlowUserController flowUserController;
//
//    @Autowired
//    private FlowGroupController flowGroupController;
//
//    @Autowired
//    private FlowMembershipController flowMembershipController;
//
//    @Autowired
//    private ProcessDefinitionController processDefinitionController;
//
//    @Autowired
//    private AuditActivityController auditActivityController;
//
//    @Test
//    public void testAddUser(){
//        String id = "test1_id";
//        String displayName = "test1_name";
//        CreateIDMUserParam createIDMUserParam = new CreateIDMUserParam(id, displayName);
//        flowUserController.addUser(createIDMUserParam);
//    }
//
//    @Test
//    public void testDeleteUser(){
//        String id = "test1_id";
//        String displayName = "test1_name";
//        flowUserController.deleteUser(id);
//    }
//
//    @Test
//    public void testAddGroup(){
////        String id = "group2_id";
////        String name = "group2_name";
////        CreateIDMGroupParam createIDMGroupParam = new CreateIDMGroupParam(id, name);
////        flowGroupController.addGroup(createIDMGroupParam);
//    }
//
//    @Test
//    public void testDeleteGroup(){
//        String id = "group1_id";
//        String name = "group1_name";
//        flowGroupController.deleteGroup(id);
//    }
//
//    @Test
//    public void testAddMembership(){
//        String userid = "test1_id";
//        String groupid = "group1_id";
//        CreateMembershipParam createMembershipParam = new CreateMembershipParam(userid, groupid);
//        flowMembershipController.addMembership(createMembershipParam);
//    }
//
//    @Test
//    public void testDeleteMembership(){
//        String userid = "test1_id";
//        String groupid = "group1_id";
//        DeleteMembershipParam deleteMembershipParam = new DeleteMembershipParam(userid, groupid);
//        flowMembershipController.deleteMembership(deleteMembershipParam);
//    }
//
//    @Test
//    public void testDeployProcessDefinition(){
//        String file = "processes/Autonomous_activities2.bpmn20.xml";
//        String name = "Autonomous_activities";
//        DeployProcessDefinitionParam deployProcessDefinitionParam = new DeployProcessDefinitionParam(file, name);
//        processDefinitionController.deployProcessDefinition(deployProcessDefinitionParam);
//    }
//
//    @Test
//    public void testDeleteProcessDefinition(){
//        String file = "processes/Autonomous_activities.bpmn20.xml";
//        String id = "157c4f21-e896-11ed-a394-02004c4f4f50";
//        processDefinitionController.deleteProcessDefinition(id, false);
//    }
//
//    @Test
//    public void testStartProcessInstance(){
//        String processName = "autonomous activity";
//        String userId = "test1_id";
//        Map<String, Object> startParam = new HashMap<>();
//        startParam.put("variable1", "value1");
//        startParam.put("group1", "group1_id");
//        StartProcessInstanceParam startProcessInstanceParam = new StartProcessInstanceParam(processName, userId, startParam);
//        auditActivityController.startProcessInstance(startProcessInstanceParam);
//    }
//
//    @Test
//    public void testcompleteTask(){
//        String taskId = "0459dd7a-e9a4-11ed-845f-02004c4f4f50";
//        Map<String, Object> taskParam = new HashMap<>();
//        CompleteTaskParam completeTaskParam = new CompleteTaskParam(taskId, taskParam);
//        taskParam.put("approved", false);
//        auditActivityController.completeTask(completeTaskParam);
//    }
//
//    @Test
//    public void testqueryProcessInstanceByOriginator(){
//        String userId = "test1_id";
//        ResponseResult<List<String>>  res = auditActivityController.queryProcessInstanceByOriginator(userId);
//        System.out.println("res = " + res);
//    }
//
//    @Test
//    public void testqueryTaskByUserId(){
//        String userId = "22151221";
//        ResponseResult<List<String>>  res = auditActivityController.queryTaskByUserId(userId);
//        System.out.println("res = " + res);
//    }
//
//    @Test
//    public void testqueryTaskByGroupId(){
//        String groupId = "group1_id";
//        ResponseResult<List<String>>  res = auditActivityController.queryTaskByGroupId(groupId);
//        System.out.println("res = " + res);
//    }
//
//    @Test
//    public void testaddCandidateGroup(){
//        String taskId = "3288b1ca-e89a-11ed-9717-02004c4f4f50";
//        String groupId = "group1_id";
//        auditActivityController.addCandidateGroup(taskId, groupId);
//    }
//
//    @Test
//    public void testdeleteCandidateGroup(){
//        String taskId = "3288b1ca-e89a-11ed-9717-02004c4f4f50";
//        String groupId = "123";
//        auditActivityController.deleteCandidateGroup(taskId, groupId);
//
//    }
//
//    @Test
//    public void testqueryTaskCandidateGroup(){
//        String taskId = "3288b1ca-e89a-11ed-9717-02004c4f4f50";
//        ResponseResult<List<String>>  res = auditActivityController.queryTaskCandidateGroup(taskId);
//        System.out.println("res = " + res);
//    }
//
//}
