package edu.zjucst.spb.flow;

import edu.zjucst.spb.exception.BizException;
import edu.zjucst.spb.service.FlowBuildService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.Deployment;
import org.flowable.form.api.FormDeployment;
import org.flowable.form.api.FormInfo;
import org.flowable.form.api.FormModel;
import org.flowable.form.api.FormRepositoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-29
 */
@SpringBootTest
public class FlowBuildTest {

    @Autowired
    FlowBuildService flowBuildService;

    @Test
    void testGroupCreate() {
        flowBuildService.createGroup("第一党支部");
    }

    @Test
    void testGroupQuery() {
        String id = flowBuildService.queryGroupIdByGroupName("第一党支部");
        System.out.println("id = " + id);
    }

    @Test
    void testGroupDelete() {
//        flowBuildService.deleteGroupByGroupName("第一党支部");
        flowBuildService.deleteGroupByGroupId("2342342");
    }

    @Test
    void testMembership() {
        try {
            flowBuildService.createMembership("22351236", "1234123");
        } catch (BizException e) {
            System.out.println("e.getRetCode() = " + e.getRetCode());
            System.out.println("e.getRetMessage() = " + e.getRetMessage());
        }

    }

    @Test
    void deploy() {
        Deployment test = flowBuildService.createDeployment("flows/joinParty5.bpmn20.xml", "joinPartyV1");
        System.out.println(test.getId());
        System.out.println(test.getName());
        System.out.println(test.getKey());
    }

    @Test
    void deployForm() {
        String deployId = "88bf45db-180e-11ef-a1c0-e454e82b2ca6";

        List<String> list = Arrays.asList(
            "activistPartyTrainingTime",
            "activistTime",
            "confirmTime",
            "cultivateContacts",
            "deliveryTime",
            "developmentPublicTime",
            "educationalVisitTime",
            "recordTime",
            "talkRegistrationTimeAndTalker"
        );

        list.forEach(formName -> {
            String path = "flows/forms/" + formName + ".form";
            FormDeployment form = flowBuildService.createForm(path, formName, deployId);
            System.out.println(formName + " completed.");
//            System.out.println(path);
        });

//        FormDeployment deliveryTime = flowBuildService.createForm("flows/forms/deliveryTime.form", "deliveryTime", deployId);
//        System.out.println(deliveryTime.getId());
//        System.out.println(deliveryTime.getName());
    }

    @Autowired
    private FormRepositoryService formRepositoryService;

    @Autowired
    private RepositoryService repositoryService;

    @Test
    void deleteForm() {
        FormInfo deliveryTime = formRepositoryService.getFormModelByKey("deliveryTime.form");
        System.out.println("deliveryTime.getId() = " + deliveryTime.getId());
        System.out.println("deliveryTime.getName() = " + deliveryTime.getName());

        formRepositoryService.deleteDeployment("dc6c063b-9b06-11ee-ae87-e454e82b2ca6");

//        repositoryService.deleteDeployment("79996622-9b02-11ee-a24b-e454e82b2ca6");
    }
}
