//package edu.zjucst.spb.flow;
//
//import edu.zjucst.spb.controller.flow.*;
//import edu.zjucst.spb.domain.ResponseResult;
//import edu.zjucst.spb.domain.param.flow.*;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.HashMap;
//import java.util.List;
//
//@SpringBootTest
//public class FlowIntegrationTest {
//    @Autowired
//    private FlowUserController flowUserController;
//
//    @Autowired
//    private FlowGroupController flowGroupController;
//
//    @Autowired
//    private FlowMembershipController flowMembershipController;
//
//    @Autowired
//    private ProcessDefinitionController processDefinitionController;
//
//    @Autowired
//    private AuditActivityController auditActivityController;
//
//    @Test
//    public void test1_1(){
////        //创建5个用户、3个用户组、若干用户组与用户的关系用于测试
////        //用户组A包含用户A，用户组B包含用户A、B、C，用户组C包含用户A、B、C、D、E
////        flowUserController.addUser(new CreateIDMUserParam("user_Aid", "user_Aname"));
////        flowUserController.addUser(new CreateIDMUserParam("user_Bid", "user_Bname"));
////        flowUserController.addUser(new CreateIDMUserParam("user_Cid", "user_Cname"));
////        flowUserController.addUser(new CreateIDMUserParam("user_Did", "user_Dname"));
////        flowUserController.addUser(new CreateIDMUserParam("user_Eid", "user_Ename"));
////        flowGroupController.addGroup(new CreateIDMGroupParam("group_Aid", "group_Aname"));
////        flowGroupController.addGroup(new CreateIDMGroupParam("group_Bid", "group_Bname"));
////        flowGroupController.addGroup(new CreateIDMGroupParam("group_Cid", "group_Cname"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Aid", "group_Aid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Aid", "group_Bid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Bid", "group_Bid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Cid", "group_Bid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Aid", "group_Cid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Bid", "group_Cid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Cid", "group_Cid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Did", "group_Cid"));
////        flowMembershipController.addMembership(new CreateMembershipParam("user_Eid", "group_Cid"));
////
////        //启动流程定义A的三个流程实例。启动流程实例A，由用户A发起，审批组为用户组A；启动流程实例B，由用户A发起，审批组为用户组B；启动流程实例C，由用户E发起，审批组为用户组C
////        auditActivityController.startProcessInstance(new StartProcessInstanceParam("autonomous activity", "user_Aid",
////            new HashMap<String, Object>(){
////                {
////                    put("group1", "group_Aid");
////                }
////            }));
////        auditActivityController.startProcessInstance(new StartProcessInstanceParam("autonomous activity", "user_Aid",
////            new HashMap<String, Object>(){
////                {
////                    put("group1", "group_Bid");
////                }
////            }));
////        auditActivityController.startProcessInstance(new StartProcessInstanceParam("autonomous activity", "user_Eid",
////            new HashMap<String, Object>(){
////                {
////                    put("group1", "group_Cid");
////                }
////            }));
//    } //测试通过
//
//    @Test
//    public void test1_2(){
//        //查找用户A发起的流程实例
//        ResponseResult<List<String>> res1 = auditActivityController.queryProcessInstanceByOriginator("user_Aid");
//        System.out.println("res1 = " + res1); //测试通过
//        //查找需要用户B审批的任务
//        ResponseResult<List<String>>  res2 = auditActivityController.queryTaskByUserId("user_Bid");
//        System.out.println("res2 = " + res2); //测试通过
//        //查找需要用户组B审批的任务
//        ResponseResult<List<String>>  res3 = auditActivityController.queryTaskByGroupId("group_Bid");
//        System.out.println("res3 = " + res3); //测试通过
//        //对审批组为用户组A的任务添加审批用户组B，查找这些任务的审批用户组，再删除刚刚添加的审批用户组B，再查找这些任务的审批用户组
//        List<String> taskIds = auditActivityController.queryTaskByGroupId("group_Aid").getData();
//        assert taskIds.size() == 1: "taskIds.size() != 1";
//        for(String taskId : taskIds){
//            auditActivityController.addCandidateGroup(taskId, "group_Bid");
//            ResponseResult<List<String>>  res4 = auditActivityController.queryTaskCandidateGroup(taskId);
//            System.out.println("res4 = " + res4); //测试通过
//            auditActivityController.deleteCandidateGroup(taskId, "group_Bid");
//            ResponseResult<List<String>>  res5 = auditActivityController.queryTaskCandidateGroup(taskId);
//            System.out.println("res5 = " + res5); //测试通过
//        }
//        //查找用户E发起的流程实例中的任务id
//        List<String> processInstanceIds = auditActivityController.queryProcessInstanceByOriginator("user_Eid").getData();
//        assert processInstanceIds.size() == 1: "rocessInstanceIds.size() != 1";
//        for (String processInstanceId : processInstanceIds){
//            ResponseResult<List<String>> res6 = auditActivityController.queryTaskByProcessInstanceId(processInstanceId);
//            System.out.println("res6 = " + res6); //测试通过
//        }
//    }
//
//    @Test
//    public void test1_3(){
//        //查询需要用户组B审批的任务，完成这些任务，再次查询需要用户组B审批的任务
//        ResponseResult<List<String>>  res1 = auditActivityController.queryTaskByGroupId("group_Bid");
//        System.out.println("res1 = " + res1); //测试通过
//        List<String> tasks = res1.getData();
//        for(String task: tasks){
//            auditActivityController.completeTask(new CompleteTaskParam(task, new HashMap<String, Object>(){
//                {
//                    put("approved", false);
//                }
//            }));
//        }
//        ResponseResult<List<String>>  res2 = auditActivityController.queryTaskByGroupId("group_Bid");
//        System.out.println("res2 = " + res2); //测试通过
//    }
//
//    @Test
//    public void clrTest1(){
//        //清除测试1中对数据库增加的内容，包括用户关系、用户、用户组、任务
//        //完成所有任务
//        ResponseResult<List<String>>  res1 = auditActivityController.queryTaskByUserId("user_Aid");
//        List<String> tasks = res1.getData();
//        for(String task: tasks){
//            auditActivityController.completeTask(new CompleteTaskParam(task, new HashMap<String, Object>(){
//                {
//                    put("approved", false);
//                }
//            }));
//        }
//        //删除用户关系
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Aid", "group_Aid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Aid", "group_Bid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Bid", "group_Bid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Cid", "group_Bid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Aid", "group_Cid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Bid", "group_Cid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Cid", "group_Cid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Did", "group_Cid"));
//        flowMembershipController.deleteMembership(new DeleteMembershipParam("user_Eid", "group_Cid"));
//        //删除用户组、用户
//        flowGroupController.deleteGroup("group_Aid");
//        flowGroupController.deleteGroup("group_Bid");
//        flowGroupController.deleteGroup("group_Cid");
//        flowUserController.deleteUser("user_Aid");
//        flowUserController.deleteUser("user_Bid");
//        flowUserController.deleteUser("user_Cid");
//        flowUserController.deleteUser("user_Did");
//        flowUserController.deleteUser("user_Eid");
//    }
//}
