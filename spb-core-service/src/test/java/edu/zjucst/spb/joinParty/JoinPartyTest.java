package edu.zjucst.spb.joinParty;

import edu.zjucst.spb.domain.entity.Stage;
import edu.zjucst.spb.domain.entity.flow.joinParty.MyProcessTask;
import edu.zjucst.spb.domain.param.stage.joinParty.JoinPartyTaskListParam;
import edu.zjucst.spb.domain.vo.stage.StageInsertVO;
import edu.zjucst.spb.exception.BizException;
import edu.zjucst.spb.service.StageService;
import edu.zjucst.spb.service.joinParty.JoinPartyFlowService;
import edu.zjucst.spb.util.converMapper.StageConvertMapper;
import org.flowable.engine.TaskService;
import org.flowable.task.api.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

/**
 * @author ZlunYan
 * @description
 * @create 2023-11-30
 */

@SpringBootTest
public class JoinPartyTest {

    @Autowired
    private JoinPartyFlowService joinPartyFlowService;

    @Autowired
    private StageService stageService;

    @Test
    void testCreateStage() throws ParseException {
        StageInsertVO vo = new StageInsertVO();
        vo.setUserName("严梓伦");
        vo.setUserNumber("22351236");
        vo.setDevelopmentPhaseId(0);

        String birthStr = "2001-01-03";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        vo.setBirthday(sdf.parse(birthStr));

        stageService.insertStage(StageConvertMapper.INSTANCE.stageInsertVOConvertstage(vo));
    }

    @Test
    void testStartProcess() {
        joinPartyFlowService.startProcess("22351236", "第一党支部");
    }

    @Test
    void testQueryMyProcess() {
        MyProcessTask task = joinPartyFlowService.queryMyCreateProcess("22351236");
        System.out.println("task.getId() = " + task.getId());
        System.out.println("task.getName() = " + task.getName());
        System.out.println("task.getDescription() = " + task.getDescription());
    }

    @Test
    void testMyTODOTask() {
        try {
            List<MyProcessTask> tasks = joinPartyFlowService.queryMyTODOProcess(1, 10, "第一党支部").getTaskList();

            for (MyProcessTask task : tasks) {
                System.out.println("task.getId() = " + task.getId());
                System.out.println("task.getName() = " + task.getName());
                System.out.println("task.getDescription() = " + task.getDescription());
            }
        } catch (BizException e) {
            System.out.println("e.getRetMessage() = " + e.getRetMessage());
        }
    }

//    @Test
//    void testCompleteTask() {
//        String taskId = "39f80c0e-8f65-11ee-950f-e454e82b2ca6";
//        joinPartyFlowService.simpleCompleteTaskByTaskId(taskId, true);
//    }

    @Test
    void testDeleteTask() {
        String processId = "999a2e0e-a704-11ee-a33a-e454e82b2ca6";
        joinPartyFlowService.simpleDeleteProcess(processId, "error");
    }

    @Test
    void testQueryCompleteTask() throws ParseException {
        String taskId = "5b3591d8-a700-11ee-a826-e454e82b2ca6";
        JoinPartyTaskListParam param = new JoinPartyTaskListParam();
        param.setTaskId(taskId);
        param.setAccept(true);

        String birthStr = "2023-12-30";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        param.setTime(sdf.parse(birthStr));
        joinPartyFlowService.completeTaskByTaskId(param);
    }


    @Autowired
    private TaskService taskService;
    @Test
    void testQueryByDefinitionId() {
        // 53ccae88-9b17-11ee-99fd-e454e82b2ca6
        Task task = taskService.createTaskQuery().taskId("53ccae88-9b17-11ee-99fd-e454e82b2ca6").singleResult();
        joinPartyFlowService.simpleDeleteProcess(task.getProcessInstanceId(), "error");
    }

    @Test
    void testReflex() throws NoSuchFieldException, IllegalAccessException {
        Stage stage = new Stage();
        stage.setUserName("123");

        Field userName = Stage.class.getDeclaredField("userName");
        userName.setAccessible(true);
        userName.set(stage, "严梓伦");

        System.out.println(stage.getUserName());
    }

    @Test
    void testListJoin() {
        StringBuilder stringBuilder = new StringBuilder("123");
        System.out.println(stringBuilder.toString());
    }
}
