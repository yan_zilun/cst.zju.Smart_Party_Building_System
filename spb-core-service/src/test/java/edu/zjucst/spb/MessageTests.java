package edu.zjucst.spb;

import edu.zjucst.spb.domain.DevelopStateMachine;
import edu.zjucst.spb.domain.entity.SpbUser;
import edu.zjucst.spb.service.message.UserMessageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author MrLi
 * @date 2023-06-07 15:53
 **/

@SpringBootTest
public class MessageTests {


    @Autowired
    private UserMessageService userMessageService;

    @Test
    public void testStageChange() {
        SpbUser user = new SpbUser();
        user.setId(1L);
        user.setUsername("chb");
        user.setPhone("123");
        user.setUserNumber("22250001");
        user.setDevelopmentPhase("发展对象");

        SpbUser updateUser = DevelopStateMachine.ACTIVIST.update(user);
        System.out.println(updateUser);
    }

}
