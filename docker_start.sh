###### 一键部署脚本 #####
# 日志路径
LOG_FOLDER="${HOME}/logs"
UPDATE_FOLDER="${HOME}/update"
create_folder(){
  local folder_path=$1
  if [ ! -d "$folder_path" ]; then
    mkdir "$folder_path"
    echo "日志文件夹 $folder_path 创建成功"
  fi
}
# 创建文件夹
create_folder ${LOG_FOLDER}
create_folder ${UPDATE_FOLDER}


# 暂停并删除容器
docker stop spb
docker rm spb
# 删除旧的镜像
docker rmi spb:latest
docker rm spb
# 创建新镜像
docker build -t spb:latest .
# 暴露 宿主机的8080 => 容器应用的8080端口上
docker run -tid -v ${HOME}/update/:/update -v ${HOME}/logs/:/app/logs -p 8080:8080 --name spb spb:latest
# 查看启动状态
#docker run -ti -p 8080:8080 --name spb spb:latest
