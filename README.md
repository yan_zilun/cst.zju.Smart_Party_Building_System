# smartPartyBusiness

> 💡：ZJU-CST Party Business System——软件学院智慧党建系统

资料见:
- [Teambition](https://www.teambition.com/project/6386bbd14a983a2aed3e8c6d/app/5eba5fc96a92214d420a321c)
- 钉钉

## 技术栈
- SpringBoot
- SpringMvc
- MybatisPlus
- Shiro
- Flowable
- [mpaStruct](https://blog.csdn.net/qq_44732146/article/details/119968376)


## 项目结构

> 目前为单体应用架构设计, 本身没有分模块的必要, 但为了梳理分类更清晰, 还是将代码分为两个模块:
> 1. `spb-common`: 通用的功能模块
> 2. `spb-core-service`: 业务领域相关的核心代码
>

目前的文件树结构:

```
├───spb-common
│   └───src
│       ├───main
│       │   ├───java
│       │   │   └───edu
│       │   │       └───zjucst
│       │   │           └───spb
│       │   │               ├───domain
│       │   │               ├───enums
│       │   │               ├───exception
│       │   │               └───util
│       │   └───resources
│       └───test
│           └───java
└───spb-core-service
    ├───.mvn
    │   └───wrapper
    ├───src
    │   ├───main
    │   │   ├───java
    │   │   │   └───edu
    │   │   │       └───zjucst
    │   │   │           └───spb
    │   │   │               ├───conf
    │   │   │               ├───controller
    │   │   │               ├───domain
    │   │   │               │   ├───dto
    │   │   │               │   ├───entity
    │   │   │               │   └───vo
    │   │   │               ├───mapper
    │   │   │               ├───service
    │   │   │               │   └───impl
    │   │   │               └───util
    │   │   └───resources
    │   │       ├───mapper
    │   │       ├───sql
    │   │       ├───static
    │   │       └───templates
    │   └───test
    │       └───java
    │           └───edu
    │               └───zjucst
    │                   └───spb
```

后续会为**后台管理系统**新开一个模块

## [ChangeLog]()
- 2023年4月25日: 完成站内信框架; 部分完成自主活动; 部分完成flowable接口; 确定用户信息字段; 更新开发规范
- 2022年12月4日: 完成dockerfile部署; 后续添加CICD
- 2022年12月3日: 完成项目基本web框架搭建


## 规范

### 文件路径规范
- mapper文件位于`src/main/resources/mapper`
- bpmn流程文件位于`src/main/resources/processes`
- sql的库表文件位于`src/main/resources/sql`


### 开发注意手册

1. ResponseResult 使用

- controller中的结果通过响应封装类ResponseResult返回, service尽量不返回ResponseResult, dao层不返回ResponseResult。
  - 注：如果service会有不同的处理结果，如`UPLOAD_FORBIDDEN`、`UPLOAD_ERROR`，那么可以service.method可以返回枚举类型, 然后在controller中根据枚举类型的不同返回相应的ResponseResult
- ResponseResult需要增加泛型, 可以降低与前端接口对接的成本, 否则响应参数中会缺失参数类型, 只是返回object(泛型擦除)。
    - 有泛型的swagger文档
      ![](docs/readme_pic/responseResult没有泛型.png)
    - 有泛型的swagger文档
      ![](docs/readme_pic/responseResult有泛型.png)

2. mybatisPlus使用

- ①然后sql拼接的时候mybatis提供了链式操作，直接链式写就行。
  ![](docs/readme_pic/mybatisPlus链式操作.png)
- ②至于使用lambdaQuery+方法引用 还是 QueryWrapper这个自行决定即可

- 结构规范
    - xxxService `extends IService<Activity>` 从而获得`saveBatch`的一些批量操作的能力
      ```java
      public interface ActivityService extends IService<Activity> {
      }
      ```
    - xxxServiceImpl 除了是实现 xxxService接口以外还需要继承 ` extends ServiceImpl<ActivityMapper, Activity>` 从而确定返回值泛型类型
      ```java
      @Service
      public class ActivityServiceImpl extends ServiceImpl<ActivityMapper, Activity>  implements ActivityService {
          @Autowired
          private ActivityMapper activityMapper;
      }
      ```
    - xxxDao `extends BaseMapper<Activity>` 从而获得一些通用的mapper能力
      ```java
      public interface ActivityDao extends BaseMapper<Activity> {
      }
      ```
3. 事务使用
- spring提供了对数据库事务管理的封装，主要通过动态代理实现的。每条语句都会以事务的形式执行，如果需要在service中对一些列操作进行事务管理可以增加**声明式事务**或者**编程式事务**，
  其中Spring官方推荐声明式事务(虽然容易事务失效, 但是真的很方便), 在函数上增加`@Transactional`即可。

    注：由于使用了动态代理, 因此没必要用的地方请不要乱用, 增加服务器性能损耗——select的地方基本都不需要加的；在需要保证一些列insert或者update语句的原子性的地方才加上

4. controller参数控制
- controller里如果请求参数过长, 请写成pojo(最好是都写成POJO, 因为可以通过swagger增强), 命名后缀带Param(比如`ActivityCreateParam`)，后期也便于维护
![](docs/readme_pic/controller参数不宜过长.png)

5. RESTful规范
- 虽然不少项目也只用`@get`和`@post`也够了，只不过还是全按规范来把，分为`get/post/put/delete`。其中`put/delete`的请求参数请使用`@RequestBody`，
`@RequestParam`对`@put`的支持好像有点问题，而`delete`保持统一也使用`@RequestBody`吧
- URL也关注下写法，e.g.`/名词s` ，统一下后跟前端对接的时候方便点
![](docs/readme_pic/restful_url.png)

6. 数据库
- 表名统一增加`tb_`前缀
- 固定默认字段: `create_time`和`update_time`
  ```sql
    `create_time`  DATETIME NOT NULL COMMENT '创建时间' DEFAULT CURRENT_TIMESTAMP,
    `update_time`  DATETIME NOT NULL COMMENT '更新时间' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  ```
- 时间类型统一不使用`TIMESTAMP`而是`DATE`or`DATETIME`类型
- DAO中的接口参数增加`@Param(xxx)`, 就可以在mapper.xml中直接使用该名字拿取对应的参数, 其会调用xxx的getter、setter方法.
  ```xml
    <select id="pageQueryMessage" resultMap="UserNotifyMessageVOResultMap">
      select u.username, um.is_read, um.create_time, um.update_time, umt.content
      from tb_user_message um, tb_user_message_text umt, tb_spb_user u
      <where>
        um.message_id = umt.id
        and um.recv_user_id = #{recvUserId}
        and um.send_user_id = u.id
        <if test="messageIsReadPageQueryDTO.isRead != null">
          and `is_read` = #{messageIsReadPageQueryDTO.isRead}
        </if>
      </where>
      order by `update_time`
      <if test="messageIsReadPageQueryDTO.offset != null and messageIsReadPageQueryDTO.offset != null">
        limit #{messageIsReadPageQueryDTO.offset}, #{messageIsReadPageQueryDTO.pageTotal}
      </if>
    </select>

  ```
  more： @Param是MyBatis所提供的 (`org.apache.ibatis.annotations.Param`)，作为Dao层的注解，作用是用于传递参数，从而可以与SQL中的的字段名相对应，一般在2=<参数数<=5时使用最佳。



# 2023工作进展

## 功能点自测

🟢：已通过验证

🔴：未通过验证

🟠：部分通过验证

🟡：正在验证

| 功能模块     | 子功能点           | 验证状态 | 验证人 |
| ------------ | ------------------ | -------- |-----|
| 个人信息     | 模型（支部）管理   |      🟢  | 鲁兴  |
|              | 权限管理   |   🟠   | 鲁兴  |
|              | 登录注册功能模块   |      🟢    | 鲁兴  |
| UTILS        | 文件管理           | 🟢        | 严梓伦 |
|              | 图片验证码相关接口 | 🟢        | 严梓伦 |
|              | 站内信消息         | 🟡        | 严梓伦 |
|              | 非业务接口         | 🟢        | 严梓伦 |
| 活动管理模块 | 活动审批流程       |    🟠      | 王钶  |
|              | 活动相关接口      |   🟠     | 陈瑞豪 |
|              | 自主活动相关接口   |   🟠     | 陈瑞豪 |

