# @方法一: dockerfile中打包jar包
# ======step1===========
# 在镜像中打包jar
#FROM maven:3.6.2 as bd
#WORKDIR /code
#COPY ./ /code/build
#RUN cd /code/build && mvn package -Dmaven.test.skip=true && mv /code/build/spb-core-service/target/core-service-0.1.jar /code && rm -rf /code/build
#
## ======step2===========
#FROM openjdk:8-jdk-alpine
## 指定维护者的名字
#MAINTAINER zjucst 1063052964@qq.com
#
#WORKDIR /app/
## 将当前目录下的jar包复制到docker容器的/目录下
#COPY --from=bd /code/core-service-0.1.jar /app/app.jar
## 声明服务运行在8080端口
#EXPOSE 8080
## 提供上传数据挂载点
#VOLUME /update
## 指定docker容器启动时运行jar包
#ENTRYPOINT ["java", "-jar","app.jar", "--spring.profiles.active=prod"]



# @方法二: 打包好jar包后使用, 实践证明这种更便捷些
FROM openjdk:8-jdk-alpine
# 指定维护者的名字
MAINTAINER zjucst 1063052964@qq.com

WORKDIR /app/
# 直接把jar包跟dockerfile放在一起, 然后将当前目录下的jar包复制到docker容器的/目录下
COPY core-service-0.1.jar /app/app.jar
# 复制target路径下的jar包
#COPY spb-core-service/target/core-service-0.1.jar /app/app.jar
# 声明服务运行在8080端口
EXPOSE 8080
# 提供上传数据挂载点
VOLUME /update /app/logs
# 指定docker容器启动时运行jar包
ENTRYPOINT ["java", "-jar","app.jar", "--spring.profiles.active=prod"]
